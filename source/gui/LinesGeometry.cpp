#include "LinesGeometry.h"

#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qbufferdatagenerator.h>
#include <Qt3DRender/qpointsize.h>
#include <Qt3DRender/QAttribute>
#include <limits>

using namespace Qt3DRender;
using namespace Qt3DCore;
using namespace std;

Lines::Lines(QNode* parent) : QGeometryRenderer(parent) {
  LinesGeometry* geometry = new LinesGeometry(this);
  setPrimitiveType(PrimitiveType::Lines);
  QGeometryRenderer::setGeometry(geometry);
}

Lines::~Lines() {}

void Lines::setVertices(vector<Vertex> vertices) {
  static_cast<LinesGeometry*>(geometry())->setVertices(vertices);
}

// jclay: creates a plane of specified size in XZ plane
// returns a QByteArray in the following format:
// vec3 position <x.size, 0, z.size>
// vec2 texture coordinate
// vec3 normal <0, 1, 0>
// vec4 tangent
QByteArray createLinesVertexData(vector<Vertex> vertices) {
  const int nVerts = vertices.size();
  const quint32 stride = (3 + 3) * sizeof(float);
  QByteArray bufferBytes;
  bufferBytes.resize(stride * nVerts);
  float* fptr = reinterpret_cast<float*>(bufferBytes.data());

  for (auto v : vertices) {
    *fptr++ = static_cast<float>(v.position.x);
    *fptr++ = static_cast<float>(v.position.y);
    *fptr++ = static_cast<float>(v.position.z);

    *fptr++ = v.color.r;
    *fptr++ = v.color.g;
    *fptr++ = v.color.b;
  }

  return bufferBytes;
}

QByteArray createLinesIndexData(int num_vertices) {
  // Create the index data. 2 triangles per rectangular face
  QByteArray indexBytes;
  indexBytes.resize(num_vertices * sizeof(quint32));
  quint32* indexPtr = reinterpret_cast<quint32*>(indexBytes.data());

  for (int i = 0; i < num_vertices; i++) {
    *indexPtr++ = i;
  }

  return indexBytes;
}

class LinesVertexBufferFunctor : public QBufferDataGenerator {
 public:
  explicit LinesVertexBufferFunctor(vector<Vertex> vertices) : vertices(vertices) {}

  ~LinesVertexBufferFunctor() {}

  QByteArray operator()() Q_DECL_FINAL { return createLinesVertexData(vertices); }

  bool operator==(const QBufferDataGenerator& other) const Q_DECL_FINAL {
    const LinesVertexBufferFunctor* otherFunctor =
        functor_cast<LinesVertexBufferFunctor>(&other);
    // if (otherFunctor != nullptr) return (otherFunctor->vertices == vertices);
    return false;
  }

  QT3D_FUNCTOR(LinesVertexBufferFunctor)

 private:
  vector<Vertex> vertices;
};

class LinesIndexBufferFunctor : public QBufferDataGenerator {
 public:
  explicit LinesIndexBufferFunctor(int _num_vertices) : num_vertices(_num_vertices) {}

  ~LinesIndexBufferFunctor() {}

  QByteArray operator()() Q_DECL_FINAL { return createLinesIndexData(num_vertices); }

  bool operator==(const QBufferDataGenerator& other) const Q_DECL_FINAL {
    const LinesIndexBufferFunctor* otherFunctor =
        functor_cast<LinesIndexBufferFunctor>(&other);
    if (otherFunctor != nullptr)
      return (otherFunctor->num_vertices == num_vertices);
    return false;
  }

  QT3D_FUNCTOR(LinesIndexBufferFunctor)

 private:
  int num_vertices;
};

LinesGeometry::LinesGeometry(LinesGeometry::QNode* parent) : QGeometry(parent) {
  this->init();
}

LinesGeometry::~LinesGeometry() {}

void LinesGeometry::setVertices(vector<Vertex> vertices) {
  this->vertices = vertices;
  updateVertices();
  updateIndices();
}

void LinesGeometry::updateVertices() {
  const int nVerts = vertices.size();
  positionAttribute->setCount(nVerts);
  vertexBuffer->setDataGenerator(
      QSharedPointer<LinesVertexBufferFunctor>::create(vertices));
}

void LinesGeometry::updateIndices() {
  const int indices = vertices.size();
  indexAttribute->setCount(indices);
  indexBuffer->setDataGenerator(QSharedPointer<LinesIndexBufferFunctor>::create(indices));
}

void LinesGeometry::init() {
  positionAttribute = new QAttribute();
  colorAttribute = new QAttribute();
  indexAttribute = new QAttribute();
  vertexBuffer = new Qt3DRender::QBuffer();
  indexBuffer = new Qt3DRender::QBuffer();

  const int nVerts = vertices.size();
  const int stride = (3 + 3) * sizeof(float);

  positionAttribute->setName(QAttribute::defaultPositionAttributeName());
  positionAttribute->setVertexBaseType(QAttribute::Float);
  positionAttribute->setVertexSize(3);
  positionAttribute->setAttributeType(QAttribute::VertexAttribute);
  positionAttribute->setBuffer(vertexBuffer);
  positionAttribute->setByteStride(stride);
  positionAttribute->setCount(nVerts);

  colorAttribute->setName(QAttribute::defaultColorAttributeName());
  colorAttribute->setVertexBaseType(QAttribute::Float);
  colorAttribute->setVertexSize(3);
  colorAttribute->setAttributeType(QAttribute::VertexAttribute);
  colorAttribute->setBuffer(vertexBuffer);
  colorAttribute->setByteOffset(3 * sizeof(float));
  colorAttribute->setByteStride(stride);
  colorAttribute->setCount(nVerts);

  indexAttribute->setAttributeType(QAttribute::IndexAttribute);
  indexAttribute->setVertexBaseType(QAttribute::UnsignedInt);
  indexAttribute->setBuffer(indexBuffer);

  // Each primitive has 3 vertives
  indexAttribute->setCount(nVerts);

  vertexBuffer->setDataGenerator(
      QSharedPointer<LinesVertexBufferFunctor>::create(vertices));
  indexBuffer->setDataGenerator(
      QSharedPointer<LinesIndexBufferFunctor>::create(vertices.size()));

  addAttribute(positionAttribute);
  addAttribute(colorAttribute);
  addAttribute(indexAttribute);
}
