// TODO implement a parser strictly for these actions.
#include <boost/timer/timer.hpp>
#include "ArgParser.h"
#include "CPUSpringMass.h"
#include "CSVWriter.h"
#include "DML/Config.h"
#include "Logger.h"
#include "Simulator.h"
#include "Telemetry.h"

using namespace std;
using namespace boost::timer;

enum class TestScenario { beam_test, energy_test, performance_test };

int run_test(TestScenario ts) {
  string dml_fp;
  DML::Config config;

  // run without QT
  auto timeout_simulation_ns = config.GetRule()->GetStop().simulation_ns;
  auto timeout_wall_clock_ns = config.GetRule()->GetStop().wall_clock_ns;

  const auto start = chrono::high_resolution_clock::now();

  const auto timecond = [=]() -> bool {
    return timeout_wall_clock_ns > 0 ? chrono::duration_cast<chrono::nanoseconds>(
                                           chrono::high_resolution_clock::now() - start)
                                               .count() < timeout_wall_clock_ns
                                     : true;
  };

  auto timestepcond = [=]() -> bool {
    return (timeout_simulation_ns > 0
                ? timeout_simulation_ns >
                      Simulator::instance().spring_mass->GetSimulationTime()
                : true);
  };

  auto time = Simulator::instance().spring_mass->GetSimulationTime();

  switch (ts) {
    case TestScenario::beam_test: {
      // This functionality should be implemented in an "sim-update-happend" callback
      auto physics_thread = new thread([&]() {
        Simulator::instance().spring_mass->enableUpdating();

        while (timecond() && timestepcond()) {
          time = Simulator::instance().spring_mass->GetSimulationTime();

          // Update progress every 10ms
          if (time > 0 && (time % int(100 * 1e6) == 0)) {
            cout << time / 1e6 << " / " << timeout_simulation_ns / 1e6 << endl;
          }

          Simulator::instance().spring_mass->Step();

          if (Simulator::instance().released) {
            Telemetry::PositionTrace::instance().update();
          }
        }

        // Since we're manually controlling the steps, this doesn't really matter
        // but keeping it here for semantics.
        Simulator::instance().spring_mass->disableUpdating();

        Telemetry::PositionTrace::instance().write();
        return 0;
      });
      // Is this needed?
      // physics_thread->join();
      return 0;
      break;
    }
    case TestScenario::energy_test: {
      // TODO Implement...
      break;
    }
    case TestScenario::performance_test: {
      const int step_count = 1000;

      auto physics_thread = new thread([&]() {
        Simulator::instance().spring_mass->enableUpdating();

        cpu_timer timer;

        {
          auto_cpu_timer a_timer;
          for (int i = 0; i < step_count; i++) {
            Simulator::instance().spring_mass->Step();
          }
        }

        timer.stop();

        auto elapsed_secs = timer.elapsed().wall * 1e-9;
        auto avg_time_per_step = (elapsed_secs / step_count);
        auto masses_sec = Simulator::instance().masses->size() / avg_time_per_step;
        auto springs_sec = Simulator::instance().springs->size() / avg_time_per_step;
        auto csv_path = Simulator::instance().output_dir;

        if (ArgParser::useGPU.Get()) {
          csv_path = csv_path / "telemetry" / "gpu_performance.csv";
        } else {
          csv_path = csv_path / "telemetry" / "cpu_performance.csv";
        }

        CSVWriter csv(",");
        csv.newRow() << "bars"
                     << "masses"
                     << "bars_per_second"
                     << "masses_per_second";
        csv.newRow() << Simulator::instance().springs->size()
                     << Simulator::instance().masses->size() << springs_sec << masses_sec;
        csv.writeToFile(csv_path.string());

        cout << "Elapsed seconds: " << elapsed_secs << endl;
        cout << "Masses per second: " << masses_sec << endl;
        cout << "Springs per second: " << springs_sec << endl;

        Simulator::instance().spring_mass->disableUpdating();

        return 0;
      });
      physics_thread->join();
      return 0;
      break;
    }
    default:
      cout << "Test type not recognized";
      break;
  }
}

int main(int argc, char** argv) {
  ArgParser::parse(argc, argv);

  // All of these tests are meant to be run headless
  Simulator::instance().hide_window = true;

  Simulator::instance().use_gpu = ArgParser::useGPU.Get();
  if (ArgParser::useGPU.Get())
    cout << "GPU acceleration enabled." << endl;

  auto dml_fp = ArgParser::dmlFilepath.Get();
  auto config = DML::Parser::LoadDML(dml_fp);
  Simulator::instance().ConfigureFromDML(config);

  try {
    if (ArgParser::testPerformance.Get()) {
      cout << "Initializing performance tests " << endl;
      run_test(TestScenario::performance_test);
    } else if (ArgParser::testEnergy.Get()) {
      cout << "Initializing energy tests " << endl;
      run_test(TestScenario::energy_test);
    } else if (ArgParser::testBeam.Get()) {
      cout << "Initializing cantilerver tests " << endl;
      run_test(TestScenario::beam_test);
    } else {
      // TODO better error handling
      cerr << "No test type was specified" << endl;
    }
  } catch (exception& e) {
    cerr << e.what() << endl;
    return 0;
  }
}