#pragma once

#include "Globals.h"
#include "Mass.h"
#include "Spring.h"

#include <memory>
#include <string>
#include <vector>

class SpringMass;

enum class IntegrationMethods { Euler, Verlet, InitVerlet, RK4 };

class IntegrationMethod {
 public:
  IntegrationMethod() = default;
  IntegrationMethod(double damping, double time_step)
      : damping(damping), time_step(time_step), time_step_sq(time_step * time_step) {}
  virtual void step(){};
  virtual void initialize(){};
  virtual void integre(){};
  virtual void initialize(const std::shared_ptr<Mass>& mass){};
  virtual void integrate(const std::shared_ptr<Mass>& mass){};
  virtual void set_damping(double damping){};

  virtual void compute_forces(IntegrationMethods im){};

  void set_system(SpringMass* system) { this->system = system; };

  SpringMass* system;

  double damping;
  double time_step;
  // time_step^2, for Verlet
  double time_step_sq;
};

class SpringMass {
 public:
  SpringMass() = default;
  virtual void Step(){};
  virtual void Initialize(std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses,
                          std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs,
                          std::string integration_method,
                          double _time_step,
                          double _damping,
                          bool _is_rendering){};
  virtual void ComputeMassForces(const std::shared_ptr<Mass>& mass){};
  virtual void release(){};

  virtual void SetTracedMasses(std::vector<std::shared_ptr<Mass>> volume){};
  void disableUpdating();
  void enableUpdating();
  bool isDone();
  void PerformUpdate();
  virtual void PostUpdate(){};
  unsigned long long GetSimulationTime();
  double GetSimulationTimeSeconds();
  double GetSimulationTimeMS();
  unsigned long long simulation_time;
  bool failed = false;

  std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses;
  std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs;

  std::unique_ptr<IntegrationMethod> integration_method;

  double time_step;
  unsigned long long time_step_ns;

 protected:
  unsigned long long measure_freq_from;
  bool is_updating = false;
  double damping;
  bool is_done = false;
  bool is_rendering;
  bool released = false;
  unsigned long long release_at_ns = 500 * 1e6;
};
