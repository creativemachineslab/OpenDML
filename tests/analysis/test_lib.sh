#!/usr/bin/env bash

# This file contains functions used by test scripts.
# Not meant to be used from shell.

set -e
set -u

setup_dml_test_script() {
  # must run inside a script.  NOT from the shell
  tests_dir="$(dirname "$(readlink -f "$0")")"
  repo_root_dir="$(dirname "$(dirname $tests_dir)")"
  dirs="${@:-$tests_dir/data_*}"
  if [ "${HIDE_WINDOW:-1}" = "1" ] ; then
    hide_window="--hide-window"
  else
    hide_window=""
  fi
}

gen_simulation_tests() {
  rm -rf $dirs
  cd "$repo_root_dir"
  scripts/build 1>&2 \
    && python $tests_dir/generate_dml.py 1>&2 \
    && find ${dirs} -name "*.dml" \
    | xargs -I% sh -c 'echo `dirname %`/`basename % .dml`' \
    | xargs -I% echo scripts/run_sim \
    -f %.dml \
    --fp-trace-position %.trace.position.csv  \
    --fp-trace-total-energy %.trace.energy.csv \
    --apply-forces \
    ${hide_window}
}

analyze_data() {
  cd "$tests_dir"
  python $tests_dir/fft_anal.py --plot-ffts --plot-energy --plot-results

  for csvtype in "position" "energy" ; do
    local identical="$(\
      set -e; set -u
      diff $tests_dir/data_identical_output/*.trace.${csvtype}.csv\
        2>&1 | wc -l)"
    [ "$identical" = "0" ] && echo -n "PASS: " || echo -n "FAIL: "
    echo "Identical $csvtype csvs"
  done
}

# gen_simulation_tests $@ | sh
# analyze_data
