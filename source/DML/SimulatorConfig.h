#pragma once
#include <cstddef>

class SimulatorConfig {
 public:
  SimulatorConfig();
  ~SimulatorConfig();
  bool setWindowHeight(size_t h);
  bool setWindowWidth(size_t w);
  bool setWindowSize(size_t w, size_t h);

 private:
  size_t windowHeight;
  size_t windowWidth;
};

SimulatorConfig simConfig;
