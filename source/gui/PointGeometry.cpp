#include "PointGeometry.h"

#include <Qt3DRender/qattribute.h>
#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qbufferdatagenerator.h>
#include <Qt3DRender/qpointsize.h>
#include <limits>

using namespace Qt3DRender;
using namespace Qt3DCore;
using namespace std;

Point::Point(QNode* parent) : QGeometryRenderer(parent) {
  PointGeometry* geometry = new PointGeometry(this);
  setPrimitiveType(PrimitiveType::Points);
  QGeometryRenderer::setGeometry(geometry);
}

Point::~Point() {}

void Point::setVertices(vector<Vertex> vertices) {
  static_cast<PointGeometry*>(geometry())->setVertices(vertices);
}

QByteArray createPointVertexData(vector<Vertex> vertices) {
  const int nVerts = vertices.size();
  const quint32 stride = (3 + 3) * sizeof(float);
  // jclay: buffer
  QByteArray bufferBytes;
  // jclay: set buffer size ot number of vertices
  // for cube, this is width * height
  bufferBytes.resize(stride * nVerts);
  // jclay: we now take this empty buffer and tell
  // c++ to interpret our container as an array of floats
  float* fptr = reinterpret_cast<float*>(bufferBytes.data());

  for (auto v : vertices) {
    *fptr++ = static_cast<float>(v.position.x);
    *fptr++ = static_cast<float>(v.position.y);
    *fptr++ = static_cast<float>(v.position.z);

    *fptr++ = v.color.r;
    *fptr++ = v.color.g;
    *fptr++ = v.color.b;
  }

  return bufferBytes;
}

QByteArray createPointIndexData(int num_vertices) {
  // Create the index data. 2 triangles per rectangular face
  QByteArray indexBytes;
  indexBytes.resize(num_vertices * sizeof(quint32));
  quint32* indexPtr = reinterpret_cast<quint32*>(indexBytes.data());

  for (int i = 0; i < num_vertices; i++) {
    *indexPtr++ = i;
  }

  return indexBytes;
}

class PointVertexBufferFunctor : public QBufferDataGenerator {
 public:
  explicit PointVertexBufferFunctor(vector<Vertex> vertices) : vertices(vertices) {}

  ~PointVertexBufferFunctor() {}

  QByteArray operator()() Q_DECL_FINAL { return createPointVertexData(vertices); }

  bool operator==(const QBufferDataGenerator& other) const Q_DECL_FINAL {
    const PointVertexBufferFunctor* otherFunctor =
        functor_cast<PointVertexBufferFunctor>(&other);
    // if (otherFunctor != nullptr) return (otherFunctor->vertices.size() ==
    // vertices.size());
    return false;
  }

  QT3D_FUNCTOR(PointVertexBufferFunctor)

 private:
  vector<Vertex> vertices;
};

class PointIndexBufferFunctor : public QBufferDataGenerator {
 public:
  explicit PointIndexBufferFunctor(int _num_vertices) : num_vertices(_num_vertices) {}

  ~PointIndexBufferFunctor() {}

  QByteArray operator()() Q_DECL_FINAL { return createPointIndexData(num_vertices); }

  bool operator==(const QBufferDataGenerator& other) const Q_DECL_FINAL {
    const PointIndexBufferFunctor* otherFunctor =
        functor_cast<PointIndexBufferFunctor>(&other);
    if (otherFunctor != nullptr)
      return (otherFunctor->num_vertices == num_vertices);
    return false;
  }

  QT3D_FUNCTOR(PointIndexBufferFunctor)

 private:
  int num_vertices;
};

PointGeometry::PointGeometry(PointGeometry::QNode* parent) : QGeometry(parent) {
  this->init();
}

PointGeometry::~PointGeometry() {}

void PointGeometry::setVertices(vector<Vertex> vertices) {
  this->vertices = vertices;
  updateVertices();
  updateIndices();
}

void PointGeometry::updateVertices() {
  positionAttribute->setCount(vertices.size());
  vertexBuffer->setDataGenerator(
      QSharedPointer<PointVertexBufferFunctor>::create(vertices));
}

void PointGeometry::updateIndices() {
  indexAttribute->setCount(vertices.size());
  indexBuffer->setDataGenerator(
      QSharedPointer<PointIndexBufferFunctor>::create(vertices.size()));
}

void PointGeometry::init() {
  positionAttribute = new QAttribute();
  colorAttribute = new QAttribute();
  indexAttribute = new QAttribute();
  vertexBuffer = new Qt3DRender::QBuffer();
  indexBuffer = new Qt3DRender::QBuffer();

  const int nVerts = vertices.size();
  const int stride =
      (3 + 3) * sizeof(float);  // since the vertices are "side by side" in the buffer

  positionAttribute->setName(QAttribute::defaultPositionAttributeName());
  positionAttribute->setVertexBaseType(QAttribute::Float);
  positionAttribute->setVertexSize(3);
  positionAttribute->setAttributeType(QAttribute::VertexAttribute);
  positionAttribute->setBuffer(vertexBuffer);
  positionAttribute->setByteStride(stride);
  positionAttribute->setCount(nVerts);

  colorAttribute->setName(QAttribute::defaultColorAttributeName());
  colorAttribute->setVertexBaseType(QAttribute::Float);
  colorAttribute->setVertexSize(3);
  colorAttribute->setAttributeType(QAttribute::VertexAttribute);
  colorAttribute->setBuffer(vertexBuffer);
  colorAttribute->setByteOffset(3 * sizeof(float));
  colorAttribute->setByteStride(stride);
  colorAttribute->setCount(nVerts);

  indexAttribute->setAttributeType(QAttribute::IndexAttribute);
  indexAttribute->setVertexBaseType(QAttribute::UnsignedInt);
  indexAttribute->setBuffer(indexBuffer);

  // Each primitive has 3 vertives
  indexAttribute->setCount(nVerts);

  vertexBuffer->setDataGenerator(
      QSharedPointer<PointVertexBufferFunctor>::create(vertices));
  indexBuffer->setDataGenerator(
      QSharedPointer<PointIndexBufferFunctor>::create(vertices.size()));

  addAttribute(positionAttribute);
  addAttribute(colorAttribute);
  addAttribute(indexAttribute);
}
