function main {
  local cwd="$(dirname "$(readlink -f "$0")")"
  cd $cwd

  python ../fft_anal.py
  pandoc -o report.pdf frequency_analysis.md
}

main
