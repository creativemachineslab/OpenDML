Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.16, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.161, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.161, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.161, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.16_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.16
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.1605
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.1605
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.1605
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.16_1
All masses were unique
Calculated number of masses: 6765
Max Spring Connections: 20
Min Spring Connections: 11
Average Spring Connections: 19.9172
Max Spring Length: 0.016561
Min Spring Length: 0.00327144
Max Spring Stiffness: 2.73208e+07
Min Spring Stiffness: 5.39692e+06
Average Spring Stiffness: 1.40415e+07
6765 masses
initial centroid0.080258
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
3533
3537
3546
3547
3549
3558
3576
3579
3587
3617
3620
3633
3672
3676
3678
3679
3680
3681
3696
3706
3713
3734
3746
3751
3792
3793
3832
3873
3875
3881
3886
3900
3939
3946
3958
4000
4007
4037
4044
4053
4055
4076
4093
4136
4143
4149
4163
4199
4239
4245
4255
4272
4275
4280
4313
4400
4403
4423
4424
4472
4538
4610
4681
4770
4788
4876
4923
4996
5128
5149
5185
5344
5404
5423
5496
5554
5696
5706
5778
5841
5899
5954
6085
6099
6129
6197
6269
6322
6357
6388
6410
6431
6442
6469
6482
6548
6558
6561
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.080258
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 1099
Avg Time Per Step: 0.001099
Masses per Second: 6.15557e+06
Springs per Second: 6.13009e+07
