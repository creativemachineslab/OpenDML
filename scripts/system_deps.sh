# use this script by sourcing it and then manually calling defined functions
# . ./system_deps.sh

export_shell_variables() {
export LD_LIBRARY_PATH=$WORK/bin/lib:$WORK/bin/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$WORK/bin/lib:$WORK/bin/lib64:$LIBRARY_PATH
export INSTALL_PREFIX=$WORK/bin
export PATH=$INSTALL_PREFIX/bin:$PATH
export CMAKE_PREFIX_PATH=$INSTALL_PREFIX:$CMAKE_PREFIX_PATH
export NUM_CORES=4
}

install_cmake() {
(
set -e
set -u
wget https://cmake.org/files/v3.10/cmake-3.10.2.tar.gz
tar xvzf cmake-3.10.2.tar.gz
cd cmake-3.10.2
./configure --prefix=$INSTALL_PREFIX
make -j$NUM_CORES
make install
)
}

install_pugixml() {
 _install_github_via_cmake https://github.com/zeux/pugixml.git
}

install_gmp() {
 _wget https://gmplib.org/download/gmp/gmp-6.1.2.tar.xz
 _install_github_via_configure gmp-6.1.2
}

install_mpfr() {
 _wget http://www.mpfr.org/mpfr-current/mpfr-4.0.1.tar.xz
 _install_github_via_configure mpfr-4.0.1
}

install_boost() {(
_wget https://dl.bintray.com/boostorg/release/1.66.0/source/boost_1_66_0.tar.gz
cd boost_1_66_0
./bootstrap.sh --prefix=$INSTALL_PREFIX
./b2 install
)}

install_cgal() {
 _install_github_via_cmake https://github.com/CGAL/cgal.git
}

install_test_python() {
_wget https://www.python.org/ftp/python/3.6.4/Python-3.6.4.tar.xz
_install_github_via_configure Python-3.6.4
ln -s $INSTALL_PREFIX/bin/python3 $INSTALL_PREFIX/bin/python
}

install_glm() {
_install_github_via_cmake https://github.com/g-truc/glm.git
}

install_test_python_libs() {
pip3 install lxml
pip3 install numpy
pip3 install pandas
pip3 install matplotlib
pip3 install scipy
pip3 install seaborn
}

_wget() {(
  set -e
  set -u
  local url="$1"
  local dirname="`basename $1`"
  if [ ! -e "$dirname" ] ; then
    wget $url
    case "${url##*.}" in
     "xz") tar xJf $dirname;;
     "gz") tar xzf $dirname;;
     *) echo "Do not know how to extract $dirname"
    esac
  fi
)}

_install_github_via_configure() {(
set -e
set -u
local github_url="$1"
local dirname="`basename $1`"
if [ ! -d "$dirname" ] ; then
 git clone $github_url
fi
cd $dirname
./configure --prefix=$INSTALL_PREFIX
make -j$NUM_CORES
make install
)}


_install_github_via_cmake() {(
set -e
set -u
local github_url="$1"
local dirname="`basename $1`"
local dirname="${dirname%.git}"
if [ ! -d "$dirname" ] ; then
 git clone $github_url
 mkdir $dirname/build
fi
cd $dirname/build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$INSTALL_PREFIX -DCMAKE_BUILD_TYPE=Release ..
make -j$NUM_CORES
make install
)}

