#include "Volume.h"
#include "Simulator.h"

Volume::Volume() {}

Volume::~Volume() {}

Volume::Volume(DML::Volume& dml_volume) {
  this->id = dml_volume.GetID();
  this->scale = dml_volume.Scale();
  this->color = dml_volume.Color();
  if (dml_volume.Primitive() == "stl") {
    this->type = VolumeType::STL;
    this->stl_path = dml_volume.STLPath();
  } else {
    this->type = VolumeType::Cube;
  }
  this->translate = dml_volume.Translate();
  this->dml_volume = std::make_shared<DML::Volume>(dml_volume);
  Build();
}

void Volume::Build() {
  if (type == VolumeType::STL) {
    Geometry::ReadSTL(polyhedron, stl_path);
    auto bbox = Geometry::PolyhedronBBOX(polyhedron);
    if (dml_volume->MoveTo() == "origin") {
      Geometry::Translate(
          polyhedron, dvec3(-1.0 * bbox.xmin(), -1.0 * bbox.ymin(), -1.0 * bbox.zmin()));
    } else if (dml_volume->MoveTo() == "centroid") {
      Geometry::Translate(polyhedron,
                          dvec3(-1.0 * bbox.xmin() - (bbox.xmax() - bbox.xmin()) / 2,
                                -1.0 * bbox.ymin() - (bbox.ymax() - bbox.ymin()) / 2,
                                -1.0 * bbox.zmin() - (bbox.zmax() - bbox.zmin()) / 2));
    }
    Geometry::Scale(polyhedron, scale.x);
  } else {
    Geometry::MakeCube(polyhedron, scale.x, scale.y, scale.z);
  }

  Geometry::Translate(polyhedron, translate);

  bbox = Geometry::PolyhedronBBOX(polyhedron);
  this->bbox = bbox;
  Geometry::printBBOX(bbox, this->id);

  aabb_tree = Geometry::InitializeAABB_Tree(polyhedron);
}

// Take a mass pointer, and add it to the masses inside this volume
void Volume::AddMass(shared_ptr<Mass> mass) {
  mass->fixed |= fixed;
  mass->appliedAcceleration = appliedAcceleration;
  masses_inside.push_back(mass);
}

template <class T, class U>
void Volume::AddForce(shared_ptr<DML::LoadcaseParams::Params> prop) {
  auto params = std::dynamic_pointer_cast<T>(prop);
  forces.push_back(std::make_shared<U>(U(*params)));
}

void Volume::AddForce(DML::LoadcaseParams::Types type,
                      shared_ptr<DML::LoadcaseParams::Params> params) {
  using DML::LoadcaseParams::Types;
  switch (type) {
    case Types::Constant:
      AddForce<DML::LoadcaseParams::Constant, Forces::Constant>(params);
      break;
    case Types::Shock:
      AddForce<DML::LoadcaseParams::Shock, Forces::Shock>(params);
      break;
    case Types::Pulse:
      AddForce<DML::LoadcaseParams::Pulse, Forces::Pulse>(params);
      break;
    case Types::Fix:
      fixed = true;
      break;
    case Types::Accel: {
      // Hack
      auto tmp = std::dynamic_pointer_cast<DML::LoadcaseParams::Accel>(params);
      appliedAcceleration += tmp->magnitude;
      Simulator::instance().gravity = appliedAcceleration;
      break;
    }
    default:
      throw std::runtime_error("code bug: Unrecognized force type");
  }
}
