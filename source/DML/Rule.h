#pragma once
#include <unordered_map>
#include "RuleDamping.h"
#include "RuleDevelopment.h"
#include "RuleIntegration.h"
#include "RuleLoad.h"
#include "RuleMechanics.h"
#include "RuleOutput.h"
#include "RuleProperty.h"
#include "RuleStop.h"
#include "RuleTelemetry.h"
#include "RuleTrace.h"

namespace DML {
class Rule {
 public:
  Rule();
  ~Rule();
  RuleOutput GetOutput();
  RuleLoad GetLoad();
  RuleMechanics GetMechanics();
  RuleTrace GetTrace();
  RuleStop GetStop();
  RuleDamping GetDamping();

  RuleTelemetry GetTelemetry() const { return telemetry; }

  RuleIntegration GetIntegration() const { return integration; }

  std::vector<Loadcase> GetLoadcases();
  std::vector<Volume> GetVolumes();

  void SetOutput(RuleOutput rule);
  void SetDamping(RuleDamping rule);
  void SetLoad(RuleLoad load);
  void SetMechanics(RuleMechanics mechanics);
  void SetDevelopment(RuleDevelopment development);
  void SetTrace(RuleTrace trace);
  void AppendLoadcase(Loadcase);
  void AppendVolume(Volume);

  void SetTelemetry(const RuleTelemetry& rt) { telemetry = rt; }

  void SetIntegration(const RuleIntegration& ri) { integration = ri; }

  void SetStop(RuleStop stop);
  bool gpu_acceleration = false;

 private:
  std::vector<Volume> volumes;
  std::vector<Loadcase> loadcases;
  RuleOutput output;
  RuleLoad load;
  RuleMechanics mechanics;
  RuleDamping damping;
  RuleIntegration integration;
  RuleDevelopment development;
  RuleTrace trace;
  RuleStop stop;
  RuleTelemetry telemetry;
};

}  // namespace DML
