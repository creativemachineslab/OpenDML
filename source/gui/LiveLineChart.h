#pragma once

#include <qchartview.h>
#include <QList>
#include <QPointF>
#include <QtCharts/QChart>
#include <boost/filesystem.hpp>
#include <vector>
#include "Analysis.h"

class LiveLineChart : public QObject {
  Q_OBJECT
 public:
  LiveLineChart(std::function<EnergyData(void)> data_source);
  ~LiveLineChart() = default;

  // we use an id and a chart pair
  std::vector<std::pair<std::string, QtCharts::QChart*>> charts;
  std::vector<QtCharts::QChartView*> chart_views;
  boost::filesystem::path chart_image_directory;

 public slots:
  void update();
  void capture();

 private:
  std::function<EnergyData(void)> data_source;
  int update_count = 0;
  int max_elems_to_show = 600;

  QList<QPointF> epe;
  QList<QPointF> gpe;
  QList<QPointF> ke;
  QList<QPointF> total;
  QList<QPointF> trace;
};
