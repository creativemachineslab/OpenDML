#ifdef USE_INTEL_PSTL
#include <pstl/algorithm>
#include <pstl/execution>
#include <pstl/numeric>

#define std_par std
#else
// Use the standard library implementation
#include <algorithm>
#include <execution>
#include <numeric>

#define std_par std
#endif

#include <cmath>
#include "Analysis.h"

namespace Analysis {
using namespace std;
using namespace std_par;
extern boost::circular_buffer<EnergyData> energy_data(kNUM_ENERGY_TO_STORE);
extern boost::circular_buffer<dvec3> trace_data(kNUM_TRACE_TO_STORE);

// double StandardDeviation(std::vector<double>);
// double Variance(std::vector<double>);

auto variance = [](auto samples) {
  const auto n = static_cast<double>(samples.size());

  if (n < 2)
    return 0.0;
  double mean = reduce(execution::par, samples.begin(), samples.end(), 0.0) / n;
  auto res = reduce(execution::par,
                    samples.begin(),
                    samples.end(),
                    0.0,
                    [mean](const double var, const double x_i) {
                      return var + (x_i - mean) * (x_i - mean);
                    }) /
             n;

  return res;
};

auto standard_deviation = [](auto samples) { return sqrt(variance(samples)); };

EnergyData GetCurrentyEnergy() {
  // total energy = gravitational_potential+kinetic_energy+spring_energy

  // TODO cleanup...
  const auto masses = Simulator::instance().masses;
  const auto springs = Simulator::instance().springs;

  auto energy = EnergyData();

  const auto calc_gpe = [masses]() {
    double result = 0.0;
    for (const auto mass : *masses) {
      result +=
          abs(Simulator::instance().gravity.y) * (mass->position.y + 10) * mass->mass;
    }
    return result;
  };

  const auto calc_ke = [masses]() {
    double result = 0.0;
    for (const auto mass : *masses) {
      result += 0.5 * mass->mass * glm::dot(mass->currentVelocity, mass->currentVelocity);
    }
    return result;
  };

  const auto calc_epe = [springs]() {
    double result = 0.0;
    for (const auto s : *springs) {
      result += 0.5 * s->stiffness * (s->diffLength) * (s->diffLength);
    }
    return result;
  };

  const auto time = Simulator::instance().spring_mass->GetSimulationTimeMS();
  const auto gpe = calc_gpe();
  const auto ke = calc_ke();
  const auto epe = calc_epe();

  energy.time = time;
  energy.elastic_potential_energy = epe;
  energy.gravitational_potential_energy = gpe;
  energy.kinetic_energy = ke;
  energy.total_energy = epe + gpe + ke;

  energy_data.push_back(energy);

  return energy;
}

// How many samples to use, starting with the most recent
EnergyData GetStdDevEnergy(int sample_count) {
  const auto get_field_data = [sample_count](const auto& data,
                                             double EnergyData::*field) {
    vector<double> dbl_vec;
    for (auto t = data.end() - sample_count; t != data.end(); ++t) {
      dbl_vec.push_back((*t).*field);
    }
    return dbl_vec;
  };

  // const auto get_double_gpe = [](auto data) {
  //  vector<double> dbl_vec;
  //  for (auto t : data) {
  //    dbl_vec.push_back(t.gravitational_potential_energy);
  //  }
  //  return dbl_vec;
  //};

  // const auto get_double_ke = [](auto data) {
  //  vector<double> dbl_vec;
  //  for (auto t : data) {
  //    dbl_vec.push_back(t.kinetic_energy);
  //  }
  //  return dbl_vec;
  //};

  // const auto get_double_total = [](auto data) {
  //  vector<double> dbl_vec;
  //  for (auto t : data) {
  //    dbl_vec.push_back(t.total_energy);
  //  }
  //  return dbl_vec;
  //};

  auto ed = EnergyData();

  // ed.gravitational_potential_energy =
  //    StandardDeviation(get_double_gpe(energy_data));
  // ed.elastic_potential_energy =
  // StandardDeviation(get_double_epe(energy_data)); ed.kinetic_energy =
  // StandardDeviation(get_double_ke(energy_data)); ed.total_energy =
  // StandardDeviation(get_double_total(energy_data));

  vector<double EnergyData::*> elems = {&EnergyData::gravitational_potential_energy,
                                        &EnergyData::elastic_potential_energy,
                                        &EnergyData::kinetic_energy,
                                        &EnergyData::total_energy};
  for (const auto& e : elems) {
    ed.*e = standard_deviation(get_field_data(energy_data, e));
  }

  return ed;
}

double GetStdDevTrace(int sample_count) {
  const auto get_field_data = [sample_count](const auto& data,
                                             double glm::dvec3::*field) {
    vector<double> dbl_vec;
    for (auto t = data.end() - sample_count; t != data.end(); ++t) {
      dbl_vec.push_back((*t).*field);
    }
    return dbl_vec;
  };

  return standard_deviation(get_field_data(trace_data, &glm::dvec3::y));
}

std::shared_ptr<Mass> mass_to_trace;
bool ttrace_set = false;
glm::dvec3 GetVolumeTrace() {
  // TODO ... hacky hardcode and very brittle
  // this requires the user to name their volume trace.
  auto volume = Simulator::instance().volumes["trace"];
  if (volume->masses_inside.size() == 0) {
    cerr << "Error: No masses to trace" << endl;
    exit(1);
  }

  // if (!ttrace_set) {
  //   // double min_
  //   for (const auto &m : volume->masses_inside) {
  //     // if (m->originalPosition.y == 0) {
  //       ttrace_set                      = true;
  //       mass_to_trace = m;
  //       m->customColor                 = glm::vec3(0, 0, 1);
  //       break;
  //     // }
  // }

  // }
  auto centroid = dvec3(0, 0, 0);
  for (const auto& m : volume->masses_inside) {
    centroid += m->position;
    m->customColor = glm::vec3(0, 0, 1);
  }

  auto result = centroid / double(volume->masses_inside.size());
  // trace_data.push_back(mass_to_trace->position);

  return result;
  // return mass_to_trace->position;
}
}  // namespace Analysis
