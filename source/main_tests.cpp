#include "gui/MainWindow.h"

#include <chrono>

using namespace std;

int main(int argc, char** argv) {
  // app.setQuitOnLastWindowClosed(false);
  int return_code = 0;
  do {
    QApplication app(argc, argv);
    return_code = CreateAndRunQTApp(argc, argv);
  } while (return_code == EXIT_CODE_REBOOT);
}
