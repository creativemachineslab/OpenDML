#pragma once
#include <iostream>

namespace DML {
class RuleOutput {
 public:
  RuleOutput();
  ~RuleOutput();
  std::string destination;
  int windowHeight;
  int windowWidth;
};
}  // namespace DML
