#include "Spring.h"
#include <iostream>

using namespace std;

glm::dvec3 defaultSpringColor = dvec3(0.0f, 0.0f, 0.0f);      // black
glm::dvec3 compressedSpringColor = dvec3(0.90f, 0.2f, 0.2f);  // red
glm::dvec3 extendedSpringColor = dvec3(0.1f, 0.8f, 0.1f);     // green

Spring::Spring(unsigned int mass1_idx,
               unsigned int mass2_idx,
               double restLength,
               double stiffness) {
  this->mass1_idx = mass1_idx;
  this->mass2_idx = mass2_idx;
  this->restLength = restLength;
  this->stiffness = stiffness;
  this->SetColorByState(Spring::States::DEFAULT);
}

Spring::Spring(shared_ptr<Mass> mass1,
               shared_ptr<Mass> mass2,
               double restLength,
               double stiffness) {
  this->mass1 = mass1;
  this->mass2 = mass2;
  this->restLength = restLength;
  this->stiffness = stiffness;
  this->SetColorByState(Spring::States::DEFAULT);
}

Spring::~Spring() {}

// Set color by state
// TODO this needs to be cleaned up
// not really using this anymore
void Spring::SetColorByState(States state) {
  switch (state) {
    case States::DEFAULT:
      color = defaultSpringColor;
      break;
    case States::COMPRESSED:
      color = compressedSpringColor;
      break;
    case States::EXTENDED:
      color = extendedSpringColor;
      break;
  }
}

void Spring::SetColor(glm::dvec3 color) {
  this->color = color;
}

double Spring::GetRestLength() {
  return restLength;
}
