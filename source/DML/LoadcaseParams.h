#pragma once
#include "glm/vec3.hpp"

namespace DML {
namespace LoadcaseParams {
enum class Types { Constant, Accel, Fix, Shock, Pulse };

struct Params {
  virtual ~Params() = default;
};

struct Constant : Params {
  glm::dvec3 magnitude;
};

struct Accel : Params {
  glm::dvec3 magnitude;
};

struct Fix : Params {};

struct Shock : Params {
  unsigned long long ns;
  glm::dvec3 magnitude;
};

struct Pulse : Params {
  unsigned long long on_ns;
  unsigned long long off_ns;
  glm::dvec3 on_magnitude;
  glm::dvec3 off_magnitude;
  bool repeat;
};
}  // namespace LoadcaseParams
}  // namespace DML
