# Google Cloud Performance Testing

Spin up a Google Cloud instance with 1 Tesla V100 (I use Ubuntu 18.04). 

I built the project on a local Ubuntu 18.04 VM, you could build on the machine but will need to install dev tools and deps.

Generate some DML files for testing locally:

```
# from project root:
cd tests/analysis
python ./generate_dml.py --use_gpu --name linux_perf_v100 --performance
```

Transfer opendml folder to the machine (I use rsync but SFTP works also)

SSH into the machine:

```
sudo apt-get update && sudo apt-get -y upgrade

# Add Nvidia driver repo
sudo apt-key adv --fetch-keys  http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo bash -c 'echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list'
sudo apt update
# Install Nvidia driver
sudo apt install nvidia-driver-415

# Install script deps
sudo apt install python3-pip
pip3 install lxml
```


You can verify the NVidia device id to pass to the script below by using:

```
nvidia-smi
```

Run the tests:

```
gcloud_instance$> python3 scripts/run_tests_perf.py linux_perf_v100 --device_id=0
```

After the tests are complete, transfer back and you can analyze performance 
using the provided Python scripts.

