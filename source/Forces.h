#pragma once

#include <glm/glm.hpp>
#include <map>
#include <memory>
#include <vector>
#include "DML/LoadcaseParams.h"

using glm::dvec3;
using std::shared_ptr;
using std::vector;

namespace Forces {
// TODO:
// - simulator needs to assign these force iterators to masses
//   - for each timestep, go through volume's forces.  if any force changed,
//     update external force on its volume's masses.
//     update is mass.externalForce - (force.cur_val - force.prev_val)

struct ForceIterator {
  virtual dvec3 Next(unsigned long long time_ns) = 0;
};

struct Constant : ForceIterator {
  Constant(DML::LoadcaseParams::Constant cfg) : Constant(cfg.magnitude){};
  Constant(dvec3 val);
  dvec3 Next(unsigned long long time_ns) override;

 private:
  dvec3 val;
};

struct Pulse : ForceIterator {
  Pulse(DML::LoadcaseParams::Pulse cfg)
      : Pulse(cfg.on_magnitude, cfg.off_magnitude, cfg.on_ns, cfg.off_ns, cfg.repeat){};
  Pulse(dvec3 on_val,
        dvec3 off_val,
        unsigned long long on_ns,
        unsigned long long off_ns,
        bool repeat);
  dvec3 Next(unsigned long long time_ns) override;

 private:
  dvec3 on_val;
  dvec3 off_val;
  unsigned long long on_ns;
  unsigned long long period;
  bool repeat;
};

struct Shock : Pulse {
  Shock(DML::LoadcaseParams::Shock cfg) : Shock(cfg.magnitude, cfg.ns){};

  Shock(dvec3 magnitude, unsigned long long ns)
      : Pulse(magnitude, dvec3(0, 0, 0), ns, 0, false) {}
};

}  // namespace Forces
