Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.02, 0.1
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.101
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.101
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.101
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_z\RK4\0.1_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.1
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.1005
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.1005
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.1005
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_z\RK4\0.1_1
All masses were unique
Calculated number of masses: 4305
Max Spring Connections: 20
Min Spring Connections: 11
Average Spring Connections: 19.9108
Max Spring Length: 0.0176307
Min Spring Length: 0.00344784
Max Spring Stiffness: 2.5923e+07
Min Spring Stiffness: 5.06948e+06
Average Spring Stiffness: 1.4015e+07
4305 masses
initial centroid0.0101569
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
2868
2890
2915
2919
2920
2927
2928
2929
2934
2946
2948
2952
2955
2968
2974
2978
2980
2984
2992
2994
2995
2997
3006
3011
3022
3027
3033
3034
3037
3071
3079
3088
3089
3090
3107
3114
3132
3140
3152
3168
3176
3185
3188
3214
3255
3278
3295
3300
3313
3322
3350
3388
3389
3407
3425
3450
3507
3512
3519
3581
3587
3603
3611
3655
3656
3680
3719
3734
3757
3811
3813
3840
3842
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.0101569
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 591.655
Avg Time Per Step: 0.000591655
Masses per Second: 7.2762e+06
Springs per Second: 7.24375e+07
