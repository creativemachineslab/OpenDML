import numpy as np  
import os
import glob
import re
from plotly import tools
import plotly.io as pio

from plotly import __version__
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
import plotly.graph_objs as go

if not os.path.exists('images'):
    os.mkdir('images')
    os.mkdir('images/svgs')
    os.mkdir('images/pdfs')
    os.mkdir('images/pngs')

cachedir = 'cache'
from joblib import Memory
memory = Memory(cachedir, verbose=0)

DEBUG=False

INTEGRATION_METHODS = ["verlet", "euler", "rk4"]

# from help(plotly.graph_objs.scatter.Line.dash)
LINE_DASH_STYLES = ['solid', 'dot', 'longdashdot', 'dashdot', 'longdash', 'dash']

def normalize(vector):
   return np.array(vector)/np.linalg.norm(np.array(vector))

def find_ray_intersection(ray_origin, ray_direction, point1, point2):
    ray_origin = np.array(ray_origin, dtype=np.float64)
    ray_direction = np.array(normalize(ray_direction), dtype=np.float64)
    point1 = np.array([point1['time'], point1['pos_y']], dtype=np.float64)
    point2 = np.array([point2['time'], point2['pos_y']], dtype=np.float64)
    
    v1 = ray_origin - point1
    v2 = point2 - point1
    v3 = np.array([-ray_direction[1], ray_direction[0]], dtype=np.float64)

    t1 = np.cross(v2, v1) / np.dot(v2, v3)
    t2 = np.dot(v1, v3) / np.dot(v2, v3)

    if t1 >= 0.0 and t2 >= 0.0 and t2 <= 1.0:
        return ray_origin + t1 * ray_direction
    else:
        return None

def positive_crossings(ray, ray_direction, data):
    """
        Return the number of times a given numpy array in
        (x,y) intersects with a given ray. 
        
        This only counts the crosses when the slope of the
        line crossing the ray is positive.
        
        >>> t = np.arange(0.0, 100, 0.01)
        >>> s = np.sin(0.2*np.pi*t)
        >>> data = np.column_stack((t, s))
        >>> ray = (0,0)
        >>> direction = normalize((1,0))
        >>> crossings = positive_crossings(ray, direction, data)
        array([[ 0.,  0.],
               [10.,  0.],
               [20.,  0.],
               [30.,  0.],
               [40.,  0.],
               [50.,  0.],
               [60.,  0.],
               [70.,  0.],
               [80.,  0.],
               [90.,  0.]])
    """
    crossings = []

    if len(data['pos_y']) < 2:
        return np.array(crossings)

    for i, _x in enumerate(data[:-1]):
        # only count the crosses on the "up" side
        if data['pos_y'][i] < data['pos_y'][i+1]:
            if (data['pos_y'][i] == ray[1]):
                continue
           
            test_intersect = find_ray_intersection(ray,ray_direction, data[i], data[i+1])
            if test_intersect is not None:
                crossings.append(test_intersect)

    return np.array(crossings)

@memory.cache
def frequency_from_cross_count(cross_y_value, data):
    ray = (0, cross_y_value)
    direction = normalize((1,0))
    crossings = positive_crossings(ray, direction, data)

    wtype=np.dtype([('time', np.float64),('pos_y', np.float64)])
    crossings_struct = np.empty(len(crossings[:,0]), dtype=wtype)
    crossings_struct['time'] = crossings[:,0]
    crossings_struct['pos_y'] = crossings[:,1]

    time_diff =  crossings_struct['time'][-1] - crossings_struct['time'][0]
    return (len(crossings)-1) / (time_diff), crossings_struct

def calculate_frequency_from_trace(filepath):
    """
    This takes a position trace csv file in the following format:
    time_ms, pos_x, pos_y, pos_z

    Note that the first position in the file is the initial position
    and should be dropped. We use this to calculate the starting position
    before the trace begins.
    """
    val = re.search(r'(\d+[.]\d+)[_](\d+)', filepath).group(1) 
    wtype=np.dtype([('time', np.float64),('pos_x', np.float64),('pos_y', np.float64), ('pos_z', np.float64)])
    data = np.genfromtxt(filepath, delimiter=",", skip_header=True, dtype=wtype)
    if len(data) < 2:
        return None, None
    initial_position = data['pos_y'][0]
   
    # drop first data point, since it's the original position
    data = data[1:]
    # tmp: truncate the data
    # data = data[:40000]
    if DEBUG:
        print(data.shape)

        print("%s Starting displacement: %s" % (val, initial_position))
    
    freq, crossings = frequency_from_cross_count(initial_position, data)

    return val, freq*1e-3 # Convert from ms to seconds.

@memory.cache
def results_for(files):
    results = []
    for f in files:
        result = calculate_frequency_from_trace(f)
        if (result[0] == None):
            print("Ignored %s since the trace was empty. Is it still processing?" % f)
            continue
        results.append(result)
    return results

def varying_length_theory(lengths_x):
    results = []
    for x_len in lengths_x:
        results.append(1/(float(x_len)**2))
    return results

def traces_for_test(test_name, scenario_id, integration_method_name):
    """
        We expect the test results to be in tests/analysis/scenarios/{unique_scenario_id}/dml_files/
    """
    if DEBUG:
        print("scenarios/%s/output/%s/%s/**/*/position_trace.csv" % (scenario_id, test_name, integration_method_name))
    return glob.glob("scenarios/%s/output/%s/%s/**/*/position_trace.csv" % (scenario_id, test_name, integration_method_name), recursive=True)

def clear_cache():
    memory.clear()

def calculate_frequency_for_test(test_name, scenario_id, integration_method_name):
    files = traces_for_test(test_name, scenario_id, integration_method_name)
    if DEBUG:
        print("Calculating frequencies for %s traces" % len(files))
    return results_for(files)

def get_x_theory(lengths):
    results = []
    for x_len in lengths:
        results.append(1/(float(x_len)**2))
    return results

def get_y_theory(lengths):
    results = []
    for x_len in lengths:
        results.append(float(x_len))
    return results

def get_z_theory(lengths):
    results = []
    for x_len in lengths:
        results.append(1)
    return results

def plot_energy_traces(test_name, scenario_id, integration_method_name):
    files = glob.glob("scenarios/%s/output/%s/%s/**/*/total_energy.csv" % (scenario_id, test_name, integration_method_name), recursive=True)
    energy_traces = []
    for f in files:
        # val = re.search(r'(\d+[.]\d+)[_](\d+)', filepath).group(1) 
        wtype=np.dtype([('time', np.float64),('epe', np.float64),('gpe', np.float64), ('ke', np.float64), ('total', np.float64)])
        data = np.genfromtxt(f, delimiter=",", skip_header=True, dtype=wtype)
        # return data['time']

        this_trace = go.Scatter(
            x = data['time'],
            y = data['total'],
            mode = 'lines',
            name = 'Total Trace'
        )

        energy_traces.append(this_trace)

        # this_trace = go.Scatter(
        #     x = data['time'],
        #     y = data['epe'] / np.max(data['epe']),
        #     mode = 'lines',
        #     name = 'epe'
        # )

        # energy_traces.append(this_trace)

        # this_trace = go.Scatter(
        #     x = data['time'],
        #     y = data['gpe'] / np.max(data['gpe']),
        #     mode = 'lines',
        #     name = 'gpe'
        # )

        # energy_traces.append(this_trace)

        # this_trace = go.Scatter(
        #     x = data['time'],
        #     y = data['ke'] / np.max(data['ke']),
        #     mode = 'lines',
        #     name = 'ke'
        # )

        # energy_traces.append(this_trace)

    layout = dict(title = 'Energy Test',
                  xaxis = dict(title = 'Time', zeroline=True),
                  yaxis = dict(title = 'Total Energy (EPE + GPE + KE'))

    fig = dict(data=energy_traces, layout=layout)
    iplot(fig, filename='energy-traces')

@memory.cache
def plot_get_traces(test_name,scenario_id, integration_method_name, direction, theory_func):
    freq_data = np.array(calculate_frequency_for_test(test_name, scenario_id, integration_method_name), dtype=np.float64)
    
    freq_data[:, 1] = freq_data[:, 1] / freq_data[:, 1].max()
    
    wtype=np.dtype([('size', np.float64),('freq', np.float64)])
    exp_data = np.empty(len(freq_data), dtype=wtype)
    exp_data['size'] = freq_data[:,0]
    exp_data['freq'] = freq_data[:,1]
    
    exp_data.sort(order='size')


    theory_lengths = np.linspace(exp_data['size'][0], exp_data['size'][-1])
    theories = np.array(theory_func(theory_lengths), dtype=np.float64)
    theories = theories / theories.max()
    
    wtype=np.dtype([('size', np.float64),('freq', np.float64)])
    theory_data = np.empty(len(theory_lengths), dtype=wtype)
    theory_data['size'] = theory_lengths
    theory_data['freq'] = theories
    
    theory_data.sort(order='size')

    # Create traces
    theory = go.Scatter(
        x = theory_data['size'],
        y = theory_data['freq'],
        mode = 'lines',
        name = 'Theory'
    )
    
    experimental = go.Scatter(
        x = exp_data['size'],
        y = exp_data['freq'],
        mode = 'markers',
        name = 'Experimental'
    )
    # display(exp_data['freq'])
    
    return [experimental, theory]
    

# direction is the direction the beam is varying, x y or z
def plot_results(test_name,scenario_id, integration_method_name, direction, theory_func):
    data = plot_get_traces(test_name, scenario_id, integration_method_name, direction, theory_func)

    # Edit the layout
    layout = dict(title = 'Varying Beam %s axis - %s' % (direction, integration_method_name),
                  xaxis = dict(title = 'Size (m)', zeroline=True),
                  yaxis = dict(title = 'Normalized Frequency', zeroline=False, range=[0, 1.2]))

    fig = dict(data=data, layout=layout)
    iplot(fig, filename='styled-line')

    if DEBUG:
        print("Frequency Variance", np.var(exp_data['freq']))

#     print("SAD:", np.sum(np.abs(y_data_results_normalized - y_theory_normalized)))
#     print("SSD:", np.sum(np.square(y_data_results_normalized - y_theory_normalized)))
#     print("correlation:", np.corrcoef(np.array((y_data_results_normalized, y_theory_normalized)))[0, 1])

def show_variance_results(output_name, integration_method_name):
    files = traces_for_test(output_name)
    
    runs = [ re.search(r'(\d+[.]\d+)[_](\d+)', file).group(2) for file in files ]
    lengths = [ re.search(r'(\d+[.]\d+)[_](\d+)', file).group(1) for file in files ]
    print(lengths[0])
    x_data = runs
    y_theory = np.array(get_theory_for(lengths))
#     print(y_theory)
    y_data = np.array(calculate_frequency_for_test(output_name))
    y_data_normalized = y_data #/ y_data.max()
#     y_theory_normalized = y_theory / y_theory.max()
    
    # Create traces
    experimental = go.Scatter(
        x = x_data,
        y = y_data,
        mode = 'markers',
        name = 'Experimental',

    )
    
#     theory = go.Scatter(
#         x = x_data,
#         y = y_theory_normalized,
#         mode = 'line',
#         name = 'Theory',
#         error_y=dict(
#             type='data',
#             arrayminus= abs(y_theory_normalized - y_data_normalized),
#             visible=False
#         )
#     )

    data = [experimental]

#     # Edit the layout
    layout = dict(title = 'Variance between runs - %s - %s' % (lengths[0], integration_method_name),
                  xaxis = dict(title = 'Run', zeroline=True),
                  yaxis = dict(title = 'Frequency', zeroline=True))

    fig = dict(data=data, layout=layout)
    iplot(fig, filename='styled-line')
    if DEBUG:
        print(np.sum(np.square(y_data - y_data)))
        print(np.var(y_data))

def show_performance_for(scenarios, integration_methods = INTEGRATION_METHODS, title = 'Performance Analysis', assign_dashes=True, show_integration_method_legend=False):
    all_data = []
    for i, scenario_id in enumerate(scenarios):
        clean_scenario_name = scenario_id.replace("_", " ").title()
        dash_idx = i % len(LINE_DASH_STYLES) 
        dash_style = LINE_DASH_STYLES[dash_idx]
        line = dict(dash=dash_style, width=3)
        for m in integration_methods:
            files = glob.glob("scenarios/%s/output/%s/%s/**/*/cpu_performance.csv" % (scenario_id, "performance", m), recursive=True)
            wtype=np.dtype([('bars', np.float64),('masses', np.float64),('bars_per_sec', np.float64), ('masses_per_second', np.float64)])
            gpu_all_data = np.array([],dtype=wtype)
            cpu_all_data = np.array([],dtype=wtype)

            for file in files:
                data = np.genfromtxt(file, delimiter=",", skip_header=True, dtype=wtype)
                cpu_all_data = np.append(cpu_all_data, data)
            
            if len(cpu_all_data) > 0:
                cpu_all_data.sort(order="bars")
                if show_integration_method_legend:
                    cpu_performance = go.Scatter(x=cpu_all_data['bars'], y=cpu_all_data['bars_per_sec'], name="%s - %s" % (clean_scenario_name, m))
                else:
                    cpu_performance = go.Scatter(x=cpu_all_data['bars'], y=cpu_all_data['bars_per_sec'], name="%s" % (clean_scenario_name))
                cpu_performance['line'] = line
                all_data.append(cpu_performance)

            files = glob.glob("scenarios/%s/output/%s/%s/**/*/gpu_performance.csv" % (scenario_id, "performance", m), recursive=True)

            for file in files:
                data = np.genfromtxt(file, delimiter=",", skip_header=True, dtype=wtype)
                gpu_all_data = np.append(gpu_all_data, data)

            if len(gpu_all_data) > 0:
                gpu_all_data.sort(order="bars")
                
                if show_integration_method_legend:
                    gpu_performance = go.Scatter(x=gpu_all_data['bars'], y=gpu_all_data['bars_per_sec'], name="%s - %s" % (clean_scenario_name, m))
                else:
                    gpu_performance = go.Scatter(x=gpu_all_data['bars'], y=gpu_all_data['bars_per_sec'], name="%s" % (clean_scenario_name))
                gpu_performance['line'] = line
                all_data.append(gpu_performance)
            
    # We need to manually set our chart axis
    # since plotly zooms in
    y_max_vals = [(lambda d: d.y.max())(d) for d in all_data]
    y_max = max(y_max_vals)
    # add 10% buffer
    y_max += y_max * .10
    legend = dict(
        xanchor='right',
        traceorder='normal',
        font=dict(
            family='sans-serif',
            size=12,
            color='#000'
        ),
        bgcolor='#f9f9f9',
        bordercolor='#FFFFFF',
        borderwidth=2
    )

    layout = dict(title=title,
                  legend = legend,
                  xaxis = dict(title = 'Bars'),
                  yaxis = dict(title = 'Bars per Second', range=[0, y_max]))

    fig = dict(data=all_data, layout=layout)
    return fig

def save_all(filename, fig):
    save_pdf(filename, fig)
    save_png(filename, fig)
    save_svg(filename, fig)

def save_png(filename, fig):
    filename += ".png"
    out_file = os.path.join('images', 'pngs', filename)
    pio.write_image(fig, out_file)

def save_pdf(filename, fig):
    filename += ".pdf"
    out_file = os.path.join('images', 'pdfs', filename)
    pio.write_image(fig, out_file)
    
def save_svg(filename, fig):
    filename += ".svg"
    out_file = os.path.join('images', 'svgs', filename)
    pio.write_image(fig, out_file)

def cross_analysis(num_values, only_files, show_x=False, show_y=True, show_z=False, calculate_crossings=True, show_histograms=True):
    show_debug_data = True
    y_plot_data = []
    z_plot_data = []
    x_plot_data = []
    datas = []
    crossings_list = []

    for idx, i in enumerate(only_files):
        # Load given file
        wtype=np.dtype([('time', np.float64),('pos_x', np.float64),('pos_y', np.float64), ('pos_z', np.float64)])
        try:
            data = np.genfromtxt(i, delimiter=",", skip_header=True, usecols=(0, 1, 2, 3), dtype=wtype)
        except IOError:
            print("1 -- loading trace failed for %s" % i)

        size = re.search(r'(\d+[.]\d+)[_](\d+)', i).group(1) 

        if (data.size < 1):
            print("2 -- loading trace failed for %s" % i)
            continue
        try:
            initial_position = data['pos_y'][0]
        except IndexError:
            print("3 -- loading trace failed for %s" % i)
            continue


        # for integration comparison
        # label = re.search(r'compare_integrations\\(\w+)', i).group(1)
        # label = "%s_%s" % (label, size)

        label = "%s" % (size)

        # drop first data point, since it's the original position
        # data = data[1:]
        data = data[:num_values]
        
        init_pos_line = np.empty((2,2))
        init_pos_line[0,0] = data['time'][0]
        init_pos_line[0,1] = initial_position
        init_pos_line[1,0] = data['time'][-1]
        init_pos_line[1,1] = initial_position
        
        if (calculate_crossings):
            freq, crossings = frequency_from_cross_count(initial_position, data)
            crossings.sort(order='time')
            cross_diff = np.diff(crossings['time'][:])
        if show_debug_data:
            if DEBUG:
                print("Start Debug Data: -----------")
                print("File %s of %s" % (idx+1, len(only_files)))
                print("Initial Position: ", initial_position)
                print("Release Position: ", data[0]['pos_y'])
                print("Varying Size: ", size)
                if (calculate_crossings):
                    print("Frequency: ", freq)
                    print("Number of Crosses: ", len(crossings['pos_y']))
                    print("Cross Diff Min: ", cross_diff.min())
                    print("Cross Diff Max: ", cross_diff.max())
                    print("Cross Diff Variance", np.var(cross_diff))
                    print("Cross Diff Average", np.average(cross_diff))
                    print("Time Window (Last - First Cross)", crossings['time'][-1] - crossings['time'][0])

                print("End Debug Data: -----------")

        
        if (calculate_crossings and show_histograms):
            hist = go.Histogram(x=cross_diff)
        
            layout = dict(title='Cross Diff Histogram - %s mm' % size)
            fig = dict(data=[hist], layout=layout)
            iplot(fig)

    # crossings = crossings[:200]
    # data[:,0] -= crossings[:,0].min()
    # crossings[:,0] -= crossings[:,0].min()
    # crossings_list.append(crossings)

        datas.append((label,data))
        
    x_data = []
    y_data = []
    z_data = []
    for label, data in datas:
        x_data.append(go.Scattergl(
            x = data['time'],
            y = data['pos_x'],
            # mode = 'markers',
            name = '%s' % label,
        ))

        y_data.append(go.Scattergl(
            x = data['time'],
            y = data['pos_y'],
            mode = 'markers',
            name = '%s' % label,
        ))

        z_data.append(go.Scattergl(
            x = data['time'],
            y = data['pos_z'],
            # mode = 'markers',
            name = '%s' % label,
        ))

    if show_x:
        layout = dict(title = "X Trace - %s mm" %size,
                    xaxis = dict(title = 'time (ms)'),
                    yaxis = dict(title = 'trace (x)'))
        fig = dict(data=x_data, layout=layout)
        iplot(fig, filename='x-trace') 
        
    if show_y:
        if calculate_crossings:
            p_data_cross = go.Scattergl(
                x = crossings['time'],
                y = crossings['pos_y'],#+ 0. 000001*np.random.random_sample(len(crossings['pos_y'])),
                mode = 'markers',
                text = list(range(0, len(crossings))),
                name = 'Crosses %s' % size,
            )
        
        layout = dict(title = "Y Trace - %s mm" % size,
                xaxis = dict(title = 'time (ms)'),
                yaxis = dict(title = 'trace (y)'))
        chart_data = y_data 
        if calculate_crossings:
            chart_data.append(p_data_cross)
        fig = dict(data=chart_data, layout=layout)
        #fig = dict(data=[p_data_data_cross], layout=layout)
        iplot(fig, filename='y-trace')  

    if show_z:
        layout = dict(title = "Z Trace - %s mm" % size,
                    xaxis = dict(title = 'time (ms)'),
                    yaxis = dict(title = 'trace (z)'))
        fig = dict(data=z_data, layout=layout)
        iplot(fig, filename='z-trace') 
        
        
#     cross_line = go.Scattergl(
#         x = init_pos_line[:,0],
#         y = init_pos_line[:,1],
#         mode = 'lines',
#         name = 'Crossline %s' % size,
#     )
        
        
#     z_data = go.Scattergl(
#     )
#     plot_data.append(cross_line)
        
            
#         p_data = go.Scattergl(
#             x = cross_diff[:39]/2,
#             y = crossings['pos'][:39], #             y = crossings_list[0][:,0] - crossings[:, 0],
#             mode = 'markers',
#             name = 'Crosses %s' % idx,
#         )

#         plot_data.append(p_data)