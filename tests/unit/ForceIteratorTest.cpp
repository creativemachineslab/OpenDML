#include <algorithm>
#include <boost/filesystem.hpp>
#include <glm/gtc/epsilon.hpp>

#include "gtest/gtest.h"

#include "Simulator.h"
#include "Volume.h"

using namespace boost::filesystem;
namespace ForceIteratorTests {

  class ForceIteratorTest : public ::testing::Test {
    protected:
    virtual void SetUp() {
      auto dml_path = current_path() / "unit" / "dml_valid" /
        "loadcase_properties.dml";
      DML::Config config = DML::Parser::LoadDML(dml_path.string());
      Simulator::instance().ConfigureFromDML(config);
    }
  };

  TEST_F(ForceIteratorTest, initialize_forces_correctly) {
    ASSERT_GT(Simulator::instance().volumes.size(), 0);  // sanity check
    EXPECT_EQ(CPUSpringMass::detail::prev_forces.size(), Simulator::instance().volumes.size());
    double tot_amnt = 0;
    for (int i=0 ; i<Simulator::instance().volumes.size() ; i++) {
      auto x = CPUSpringMass::detail::prev_forces[i];
      auto vol = CPUSpringMass::detail::volumes->at(i);
      auto vol_id = vol->dml_volume->GetID();
      if (vol_id == "ignore") {
        continue;
      }
      EXPECT_NE(0, vol->masses_inside.size())
        << "sanity check: vol should not be empty: " << vol_id;
      double amnt;
      if (vol_id == "fix" || vol_id == "accel") {
        amnt = 0;
      } else if (vol->masses_inside.size() != 0) {
          amnt = -50.0 / vol->masses_inside.size();
      } else {
        amnt = 0;
      }
      tot_amnt += amnt;
      EXPECT_TRUE(glm::all(glm::equal(x, glm::dvec3(0, amnt, 0))))
        << vol_id << " " << x.x << ", " << x.y << ", " << x.z << " expected: " << amnt;
    }
  }

  TEST_F(ForceIteratorTest, handles_time_correctly) {
    CPUSpringMass::enableUpdating();

    auto t_ns = Simulator::instance().time_step * 1e9;
    ASSERT_EQ(t_ns, CPUSpringMass::detail::time_step_ns);
    ASSERT_NE(t_ns, 0);  // sanity_check
    EXPECT_EQ(0, CPUSpringMass::GetSimulationTime());
    CPUSpringMass::Step();
    EXPECT_EQ(t_ns, CPUSpringMass::GetSimulationTime());
    CPUSpringMass::Step();
    EXPECT_EQ(t_ns + t_ns, CPUSpringMass::GetSimulationTime());
  }

  TEST(Forces, Constant) {
    auto x = Forces::Constant(dvec3(1.3, -1, 2));
    EXPECT_EQ(x.Next(0), dvec3(1.3, -1, 2));
    EXPECT_EQ(x.Next(1), dvec3(1.3, -1, 2));
    EXPECT_EQ(x.Next(2), dvec3(1.3, -1, 2));
    EXPECT_EQ(x.Next(3), dvec3(1.3, -1, 2));
    EXPECT_EQ(x.Next(4), dvec3(1.3, -1, 2));
  }
  TEST(Forces, Shock) {
    auto v = dvec3(1.5, 5, -4.5);
    auto ns = 7;
    auto x = Forces::Shock(v, ns);
    EXPECT_EQ(x.Next(0), v);
    EXPECT_EQ(x.Next(5), v);
    EXPECT_EQ(x.Next(6), v);
    EXPECT_EQ(x.Next(7), dvec3(0, 0, 0));
    EXPECT_EQ(x.Next(8), dvec3(0, 0, 0));
    EXPECT_EQ(x.Next(12), dvec3(0, 0, 0));
    EXPECT_EQ(x.Next(14), dvec3(0, 0, 0));
    EXPECT_EQ(x.Next(17), dvec3(0, 0, 0));
  }
  TEST(Forces, Pulse_no_repeat) {
    auto on_val = dvec3(1.1, 2, 3);
    auto off_val = dvec3(4, 5.4, -6);
    auto x = Forces::Pulse(on_val, off_val, 1, 2, false);
    EXPECT_EQ(x.Next(0), on_val);
    EXPECT_EQ(x.Next(1), off_val);
    EXPECT_EQ(x.Next(2), off_val);
    EXPECT_EQ(x.Next(3), off_val);
    EXPECT_EQ(x.Next(4), off_val);
  }
  TEST(Forces, Pulse_repeat) {
    auto on_val = dvec3(1, 2, 3);
    auto off_val = dvec3(4, 5, 6);
    auto x = Forces::Pulse(on_val, off_val, 1, 2, true);
    EXPECT_EQ(x.Next(0), on_val);
    EXPECT_EQ(x.Next(1), off_val);
    EXPECT_EQ(x.Next(2), off_val);
    EXPECT_EQ(x.Next(3), on_val);
    EXPECT_EQ(x.Next(4), off_val);
    EXPECT_EQ(x.Next(5), off_val);
    EXPECT_EQ(x.Next(6), on_val);
  }

}  // namespace ForceIteratorTest

