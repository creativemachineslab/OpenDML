#include "DeviceCode.cuh"
#include "GPUSpringMass.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/for_each.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>
#include <algorithm>
#include <numeric>

using namespace std;
using namespace glm;
using namespace cuda;

__device__ __inline__ void direct_euler_integrate(cuMass& mass, const double damping, const double time_step);
__device__ __inline__ void direct_verlet_initialize(cuMass& mass,
                                        const double damping,
                                        const double time_step_sq);
__device__ __inline__ void direct_verlet_integrate(cuMass& mass,
                                        const double damping,
                                        const double time_step_sq);


__device__ __inline__ void compute_spring_forces::operator()(cuSpring& spring) {
  const dvec3 connection = spring.mass2->position - spring.mass1->position;
  const dvec3 direction = glm::normalize(connection);
  const double rest_length = spring.restLength;
  const double current_length = glm::length(connection);

  const double diff_length = current_length - rest_length;
  const double scalar_force = diff_length * spring.stiffness;

  const dvec3 force_vec = direction * scalar_force;

  unsigned int m1_idx = atomicAdd(&spring.mass1->forces_idx, 1);
  unsigned int m2_idx = atomicAdd(&spring.mass2->forces_idx, 1);

  spring.mass1->forces[m1_idx] = force_vec;
  spring.mass2->forces[m2_idx] = -force_vec;
}

__device__ void compute_mass_forces::operator()(cuMass& mass) {
  if (mass.fixed) {
    mass.forces_idx = 0;
    return;
  }

  // dvec3 sum_forces(0, 0, 0);
  // for (unsigned int i = 0; i < mass.forces_idx; i++) {
  //   sum_forces += mass.forces[i];
  // }

  dvec3 sum_forces =
      thrust::reduce(thrust::seq, mass.forces, mass.forces + mass.forces_idx);

  mass.force += mass.appliedAcceleration * (mass.mass);
  mass.force += sum_forces + mass.externalForce;
  mass.forces_idx = 0;

  if (integration_method == IntegrationMethods::Euler) {
    direct_euler_integrate(mass, damping, time_step);
  } else if (integration_method == IntegrationMethods::Verlet) {
    direct_verlet_integrate(mass, damping, time_step_sq);
  } else if (integration_method == IntegrationMethods::InitVerlet) {
    direct_verlet_initialize(mass, damping, time_step_sq);
  }

  //    cuSpring* spring = mass.connected_springs_ptr[i];

  //    cuMass* m1 = spring->mass1;
  //    cuMass* connected_mass;

  //    if (m1 == &mass) {
  //      connected_mass = spring->mass2;
  //    } else {
  //      connected_mass = spring->mass1;
  //    }

  //    const dvec3 connection = connected_mass->position - mass.position;
  //    const dvec3 direction = glm::normalize(connection);
  //    const double rest_length = spring->restLength;
  //    const double current_length = glm::length(connection);

  //    const double diff_length = current_length - rest_length;
  //    // spring.diffLength        = diff_length;
  //    const double scalar_force = diff_length * spring->stiffness;

  //    const dvec3 force_vec = direction * scalar_force;

  //    mass.force += force_vec;
  //  }
  //}
}

__device__ __inline__ void direct_euler_integrate(cuMass& mass, const double damping, const double time_step) {
  const auto acc = mass.force / mass.mass;
  mass.currentVelocity = damping * (mass.currentVelocity + acc * time_step);
  mass.position += mass.currentVelocity * time_step;
  mass.force = dvec3(0, 0, 0);
}

__device__ __inline__ void direct_verlet_integrate(cuMass& mass, const double damping, const double time_step_sq) {
  const auto acc = mass.force / mass.mass;
  // TODO: this damping is most likely incorrect
  const auto new_position = mass.position +
                            (mass.position - mass.lastPosition) * damping +
                            acc * time_step_sq;
  mass.lastPosition = mass.position;
  mass.position = new_position;
  mass.force = dvec3(0, 0, 0);
}

__device__ __inline__ void direct_verlet_initialize(cuMass& mass, const double damping, const double time_step_sq) {
  const auto acc = mass.force / mass.mass;
  const auto new_position = mass.position + 0.5 * acc * time_step_sq;
  mass.lastPosition = mass.position;
  mass.position = new_position;
  mass.force = dvec3(0, 0, 0);
}

__device__ void euler_integrate::operator()(cuMass& mass) {
  if (mass.fixed)
    return;

  mass.lastPosition = mass.position;

  const auto acc = mass.force / mass.mass;
  mass.currentVelocity = damping * (mass.currentVelocity + acc * time_step);
  mass.position += mass.currentVelocity * time_step;
  mass.force = dvec3(0, 0, 0);
}

__device__ void verlet_integrate::operator()(cuMass& mass) {
  if (mass.fixed)
    return;

  const auto acc = mass.force / mass.mass;
  // TODO: this damping is most likely incorrect
  const auto new_position = mass.position +
                            (mass.position - mass.lastPosition) * damping +
                            acc * time_step_sq;
  mass.lastPosition = mass.position;
  mass.position = new_position;
  mass.force = dvec3(0, 0, 0);
}

__device__ void verlet_initialize::operator()(cuMass& mass) {
  if (mass.fixed)
    return;

  const auto acc = mass.force / mass.mass;
  const auto new_position = mass.position + 0.5 * acc * time_step_sq;
  mass.lastPosition = mass.position;
  mass.position = new_position;
  mass.force = dvec3(0, 0, 0);
}

__device__ RK4_State rk4_evaluate(cuMass* mass) {
  RK4_State out;

  out.position = mass->currentVelocity;
  const auto acc = mass->force / mass->mass;
  out.velocity += acc;
  mass->force = dvec3(0, 0, 0);

  return out;
}

struct rk4_init {
  rk4_init(double time_step) : time_step(time_step){};

  template <class Tuple>
  __device__ void operator()(Tuple t) {
    thrust::get<1>(t).initials.position = thrust::get<0>(t).position;
    thrust::get<1>(t).initials.velocity = thrust::get<0>(t).currentVelocity;
  }

 private:
  double time_step;
};

struct rk4_step_a {
  rk4_step_a(double damping, double time_step) : damping(damping), time_step(time_step){};

  __device__ void operator()(RK4_States& state) {
    auto mass = state.mass;
    const auto initial = state.initials;
    state.a = rk4_evaluate(state.mass);

    mass->position = initial.position + state.a.position * time_step * 0.5;
    mass->currentVelocity =
        damping * (initial.velocity + state.a.velocity * time_step * 0.5);
  }

 private:
  double damping;
  double time_step;
};

struct rk4_step_b {
  rk4_step_b(double damping, double time_step) : damping(damping), time_step(time_step){};

  __device__ void operator()(RK4_States& state) {
    auto mass = state.mass;
    const auto initial = state.initials;

    state.b = rk4_evaluate(state.mass);

    mass->position = initial.position + state.b.position * time_step * 0.5;
    mass->currentVelocity =
        damping * (initial.velocity + state.b.velocity * time_step * 0.5);
  }

 private:
  double damping;
  double time_step;
};

struct rk4_step_c {
  rk4_step_c(double damping, double time_step) : damping(damping), time_step(time_step){};

  __device__ void operator()(RK4_States& state) {
    auto mass = state.mass;
    const auto initial = state.initials;

    state.c = rk4_evaluate(state.mass);

    mass->position = initial.position + state.c.position * time_step;
    mass->currentVelocity = damping * (initial.velocity + state.c.velocity * time_step);
  }

 private:
  double damping;
  double time_step;
};

struct rk4_step_d {
  rk4_step_d(double damping, double time_step) : damping(damping), time_step(time_step){};

  __device__ void operator()(RK4_States& state) {
    state.d = rk4_evaluate(state.mass);
  }

 private:
  double damping;
  double time_step;
};

struct rk4_step_final {
  rk4_step_final(double damping, double time_step) : damping(damping), time_step(time_step){};

  __device__ void operator()(RK4_States& state) {
    auto mass = state.mass;
    const auto initials = state.initials;


    const auto a = state.a;
    const auto b = state.b;
    const auto c = state.c;
    const auto d = state.d;

    const dvec3 dxdt =
        1.0 / 6.0 * (a.position + 2.0 * (b.position + c.position) + d.position);
    const dvec3 dvdt =
        1.0 / 6.0 * (a.velocity + 2.0 * (b.velocity + c.velocity) + d.velocity);


    mass->position = initials.position + dxdt * time_step;
    mass->currentVelocity = damping * (initials.velocity + dvdt * time_step);
    // mass->currentVelocity = initials.velocity + dvdt * time_step;
  }

 private:
  double time_step;
  double damping;
};

// __device__ glm::dvec3 sum_mass_positions::operator()(
//     const glm::dvec3* lhs,
//     const glm::dvec3* rhs) {
// __device__ glm::dvec3 sum_mass_positions::operator()(const glm::dvec3& lhs,
//                                                      const glm::dvec3& rhs){
//     // return lhs.get() + rhs.get();
// };

struct sum_mass_positions {
  __device__ cuMass * operator()(const cuMass* lhs, const cuMass* rhs) {
    cuMass * mass = new cuMass();
    auto pos = lhs->position + rhs->position;
    mass->position = pos;
    return mass;
  }
};

struct set_traced_masses {
  set_traced_masses(dvec3* traced_positions, cuMass* masses)
      : traced_positions(traced_positions), masses(masses){};

  __device__ void operator()(thrust::tuple<int, int> t) {
    // const cuMass* m = masses + i;
    // dvec3* pos = traced_positions + i;
    printf("%d", thrust::get<0>(t));
    traced_positions[thrust::get<0>(t)] = masses[thrust::get<1>(t)].position;
    // *pos = m->position;
  }

 private:
  int cur_size = 0;
  dvec3* traced_positions;
  cuMass* masses;
};

void IntegrationMethod::compute_forces(const IntegrationMethods im) {
  thrust::for_each(thrust::device, device_data.springs.begin(),
                   device_data.springs.end(), compute_spring_forces());

  thrust::for_each(thrust::device, device_data.masses.begin(),
                   device_data.masses.end(), compute_mass_forces(damping, time_step, time_step_sq, im));
}

void RK4::initialize() {
  this->compute_forces(IntegrationMethods::RK4);

  thrust::host_vector<RK4_States> rk4_states;
  rk4_states.resize(host_data.masses.size());

  thrust::host_vector<int> indices(host_data.masses.size());
  std::iota(indices.begin(), indices.end(), 0);

  thrust::for_each(thrust::host, indices.begin(), indices.end(), [&](int i) {
    RK4_States& state = rk4_states[i];
    auto& m = host_data.masses[i];

    state.initials.position = m.position;
    state.initials.velocity = m.currentVelocity;

    state.mass = thrust::raw_pointer_cast(&device_data.masses[i]);
  });

  device_data.rk4_states = rk4_states;
}

void RK4::integrate() {
  auto iter_begin = thrust::make_zip_iterator(thrust::make_tuple(
      device_data.masses.begin(), device_data.rk4_states.begin()));
  auto iter_end = thrust::make_zip_iterator(thrust::make_tuple(
      device_data.masses.end(), device_data.rk4_states.end()));

  // Initialize
  thrust::for_each(thrust::device, iter_begin, iter_end, rk4_init(time_step));

  this->compute_forces(IntegrationMethods::RK4);

  // Step A
  thrust::for_each(thrust::device, device_data.rk4_states.begin(),
                   device_data.rk4_states.end(), rk4_step_a(damping, time_step));

  this->compute_forces(IntegrationMethods::RK4);

  // Step B
  thrust::for_each(thrust::device, device_data.rk4_states.begin(),
                   device_data.rk4_states.end(), rk4_step_b(damping, time_step));

  this->compute_forces(IntegrationMethods::RK4);

  // Step C
  thrust::for_each(thrust::device, device_data.rk4_states.begin(),
                   device_data.rk4_states.end(), rk4_step_c(damping, time_step));

  this->compute_forces(IntegrationMethods::RK4);

  // Step D
  thrust::for_each(thrust::device, device_data.rk4_states.begin(),
                   device_data.rk4_states.end(), rk4_step_d(damping, time_step));

  // Step Final
  // Should damping be applied at every step or only final?
  thrust::for_each(thrust::device, device_data.rk4_states.begin(),
                   device_data.rk4_states.end(), rk4_step_final(damping, time_step));
}

void Euler::integrate() {
  this->compute_forces(IntegrationMethods::Euler);
}

void Verlet::initialize() {
  // For the first Verlet integration step, we need to initialize with a 
  // different function, for all following steps, we run with the 
  // Verlet integration algorithm.
  this->compute_forces(IntegrationMethods::InitVerlet);
}

void Verlet::integrate() {
  this->compute_forces(IntegrationMethods::Verlet);
}

void Utils::set_gpu_device(int device_id) {
  cudaSetDevice(device_id);
  cout << "GPU Device was set to: " << device_id << endl;
  cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, device_id);
  cout << "CUDA device name: " << prop.name << endl;
  cout << "CUDA device major: " << prop.major << endl;
  cout << "CUDA device minor: " << prop.minor << endl;
}

__global__ void device_sum_trace(cuMass **masses_to_trace, const int N, glm::dvec3 * trace_results, int* trace_results_idx) {
  dvec3 sum(0,0,0);

  for (int i = 0; i < N; i++) {
    sum += masses_to_trace[i]->position;
  }

  sum.x = sum.x / double(N);
  sum.y = sum.y / double(N);
  sum.z = sum.z / double(N);

  trace_results[*trace_results_idx] = sum;
  *(trace_results_idx) += 1;
}

std::vector<glm::dvec3> Utils::get_trace_results() {
  int trace_size = 0;
  cudaMemcpy(&trace_size, device_data.trace_results_idx, sizeof(int), cudaMemcpyDeviceToHost);
  host_data.trace_results.resize(trace_size);
  thrust::copy_n(device_data.trace_results.begin(), trace_size, host_data.trace_results.begin());
  return host_data.trace_results;
}

void Utils::device_update_trace() {
  cuMass** masses_to_trace = thrust::raw_pointer_cast( &device_data.masses_to_trace[0] );
  glm::dvec3* trace_results = thrust::raw_pointer_cast( &device_data.trace_results[0] );
  device_sum_trace<<<1,1>>>(masses_to_trace, device_data.masses_to_trace.size(), trace_results, device_data.trace_results_idx);

  // cuMass * init_mass = new cuMass();
  // init_mass->position = dvec3(0,0,0);
  // auto sum = thrust::reduce(device_data.masses_to_trace.begin(), device_data.masses_to_trace.end(), init_mass, sum_mass_positions());
  // cout << sum->position.y << "\n";
  // typedef thrust::device_vector<int>::iterator IntIterator;
  // typedef thrust::tuple<IntIterator, IntIterator> IteratorTuple;
  // typedef thrust::zip_iterator<IteratorTuple> ZipIterator;

  // thrust::host_vector<int> indices;
  // std::iota(indices.begin(), indices.end(), 0);
  // thrust::device_vector<int> d_indices(indices.size());
  // d_indices = indices;

  // auto iter_begin = thrust::make_zip_iterator(thrust::make_tuple(
  //     d_indices.begin(), device_data.indices_to_trace.begin()));
  // auto iter_end = thrust::make_zip_iterator(
  //     thrust::make_tuple(d_indices.end(), device_data.indices_to_trace.end()));

  // thrust::for_each(
  //     thrust::device, iter_begin, iter_end,
  //     set_traced_masses(
  //         thrust::raw_pointer_cast(device_data.positions_to_trace.data()),
  //         thrust::raw_pointer_cast(device_data.masses.data())));
  // // thrust::plus<int> binary_op;

  // // // thrust::sum<dvec3> binary_pred;
  // // vector<int> test_indices = {1, 1, 1};
  // // vector<int> out_keys;
  // // // vector<dvec3> test_values = {dvec3(0, 0, 0), dvec3(0, 1, 0), dvec3(0, 0,
  // // // 1)};

  // // thrust::device_vector<int> d_test_indices;
  // // thrust::device_vector<int> d_out_keys;
  // // // thrust::device_vector<dvec3> d_test_values;

  // // d_test_indices = test_indices;
  // // d_out_keys = out_keys;
  // // // d_test_values = test_values;

  // // host_data.positions_to_trace = device_data.positions_to_trace;

  // thrust::equal_to<int> binary_pred;
  // thrust::host_vector<dvec3> results;
  // thrust::device_vector<dvec3> d_results(host_data.indices_to_trace.size());
  // d_results = results;
  // auto first_index = thrust::make_constant_iterator<int>(1);
  // auto last_index = first_index + host_data.indices_to_trace.size();

  // thrust::reduce_by_key(thrust::device,
  //                       first_index,  // keys_first
  //                       last_index,   // keys_last
  //                       // d_test_values.begin(),
  //                       device_data.positions_to_trace.begin(),  //
  //                       thrust::make_discard_iterator(),         //
  //                       d_results.begin(),  // values_output
  //                       binary_pred,        // binary_pred
  //                       sum_mass_positions());
  // results = d_results;
  // for (auto r : results) {
  //   cout << r.x << ", " << r.y << ", " << r.z << endl;
  // }
}

void Utils::copy_data_to_host() {
  host_data.masses = device_data.masses;
  host_data.springs = device_data.springs;
}

void Utils::copy_data_to_device() {
  device_data.masses = host_data.masses;
  device_data.springs = host_data.springs;
}

void Utils::set_masses_to_trace(std::vector<int> masses_to_trace) {
  cout << "Setting masses_to_trace\n";
  for (auto i : masses_to_trace) {
    cout << i << endl;
    auto mass_ptr = thrust::raw_pointer_cast(&device_data.masses[i]);
    device_data.masses_to_trace.push_back(mass_ptr);
  }
  // room for 1 million records should be plenty.
  device_data.trace_results.resize(1e6);

  cudaMalloc((void **)&device_data.trace_results_idx, sizeof(int));
  int init = 0;
  cudaMemcpy(device_data.trace_results_idx, &init, sizeof(int), cudaMemcpyHostToDevice);
  // device_data.indices_to_trace = masses_to_trace;
  // host_data.indices_to_trace = masses_to_trace;
}

void Utils::set_gpu_integration_method(IntegrationMethods im) {
  host_data.integration_method = im;
  device_data.integration_method = im;
}

void Utils::release_forces() {
  host_data.masses = device_data.masses;
  for (int i = 0; i < host_data.masses.size(); i++) {
    auto& cu_mass = host_data.masses[i];
    cu_mass.externalForce = dvec3(0, 0, 0);
  }
  dvec3 sum_forces(0, 0, 0);
  for (int i = 0; i < host_data.masses.size(); i++) {
    auto& cu_mass = host_data.masses[i];
    sum_forces += sum_forces;
    // cu_mass.externalForce = dvec3(0,0,0);
  }
  cout << "Sum forces y: " << sum_forces.y << endl;
  device_data.masses = host_data.masses;
}

void Utils::render_update(
    std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses) {

  host_data.masses = device_data.masses;

  std::vector<int> indices(host_data.masses.size());
  std::iota(indices.begin(), indices.end(), 0);

  thrust::for_each(thrust::host, indices.begin(), indices.end(), [&](auto& i) {
    masses->at(i)->position = host_data.masses[i].position;
  });
}

void Utils::masses_to_cuMasses(
    std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses) {
  // Copy masses to host cuMasses
  for (const auto m : *masses) {
    cuda::cuMass c_m;

    c_m.fixed = m->fixed;
    c_m.mass = m->mass;
    c_m.appliedAcceleration = m->appliedAcceleration;
    c_m.currentVelocity = m->currentVelocity;
    c_m.externalForce = m->externalForce;
    c_m.force = m->force;
    c_m.position = m->position;
    c_m.originalPosition = m->originalPosition;
    c_m.lastPosition = m->lastPosition;

    for (int i = 0; i < MAX_CONNECTED_SPRINGS; i++) {
      c_m.forces[i] = dvec3(0, 0, 0);
    }

    host_data.masses.push_back(c_m);
  }
}

void Utils::springs_to_cuSprings(
    std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses,
    std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs) {
  // Copy masses to host cuMasses;
  for (const auto s : *springs) {
    cuda::cuSpring c_s;

    c_s.stiffness = s->stiffness;
    c_s.mass = s->mass;

    const auto m1_idx =
        find(masses->begin(), masses->end(), s->mass1) - masses->begin();
    const auto m2_idx =
        find(masses->begin(), masses->end(), s->mass2) - masses->begin();

    c_s.mass1_idx = m1_idx;
    c_s.mass2_idx = m2_idx;

    c_s.mass1 = thrust::raw_pointer_cast(&device_data.masses[m1_idx]);
    c_s.mass2 = thrust::raw_pointer_cast(&device_data.masses[m2_idx]);

    c_s.restLength = s->restLength;
    c_s.diffLength = s->diffLength;

    host_data.springs.push_back(c_s);
  }
}

void Utils::set_connected_springs() {
  for (int i = 0; i < host_data.springs.size(); i++) {
    auto& s = host_data.springs[i];
    cuda::cuMass* m1 = &(host_data.masses[s.mass1_idx]);
    cuda::cuMass* m2 = &(host_data.masses[s.mass2_idx]);
    if (m1->num_connected_springs > 26 || m2->num_connected_springs > 26) {
      assert(false);
    }
  }
  for (int i = 0; i < host_data.springs.size(); i++) {
    auto& s = host_data.springs[i];
    cuda::cuMass* m1 = &(host_data.masses[s.mass1_idx]);
    cuda::cuMass* m2 = &(host_data.masses[s.mass2_idx]);

    m1->connected_springs[m1->num_connected_springs] = i;
    m2->connected_springs[m2->num_connected_springs] = i;

    m1->connected_springs_ptr[m1->num_connected_springs] =
        thrust::raw_pointer_cast(&device_data.springs[i]);
    m2->connected_springs_ptr[m2->num_connected_springs] =
        thrust::raw_pointer_cast(&device_data.springs[i]);

    m1->num_connected_springs++;
    m2->num_connected_springs++;
  }
}