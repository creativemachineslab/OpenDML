'''
This is a one-time analysis script to test the simulator behaves correctly.
Main output is a visualization of theoretical vs measured frequencies of the
beam.  It assumes data has previously been generated.  Called by ./run.sh
'''
import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt
from os.path import dirname, join, basename, abspath
from scipy import optimize as spo
from lxml import etree
import os
import argparse as ap
import glob
import numpy as np
import pandas as pd
import re
import seaborn as sns


def parse_independent_var(fp):
    var_value = re.sub(r'\.trace\.\w+\.csv$', '', basename(fp))
    var_name = basename(dirname(fp)).replace('data_', '')
    return var_name, float(var_value)


def compute_freq(fp, image_dir, plot_ffts=False):
    '''get accurate first fundamental frequency of a signal'''
    df = pd.read_csv(fp)
    #  df.plot(subplots=True)
    col = 'y'
    if df[col].astype('float').isnull().sum() > 0:
        return

    y = (df[col] - df[col].mean()).values
    x = np.arange(y.shape[0])
    # note: freq assumes each timestep is a second.
    # This is okay since we only need relative not absolute values.
    # If needs to change, make the simulator add time_step to the csv.

    # estimate correct parameters
    window = np.hanning(y.shape[0])
    fft = np.fft.fft(window * y)
    freq = np.fft.fftfreq(y.shape[0])

    i = np.abs(fft)[1:fft.shape[0]//2].argsort()[-1] + 1
    est_freq = freq[i]
    est_amp = np.percentile(y, 99.9)
    est_phase = np.angle(fft[i])

    # fine tune the parameter estimates
    def objective(t, freq, amp, phase):
        return amp * np.sin(t * freq * 2 * np.pi + phase)

    popt, _ = spo.curve_fit(objective, x, y, p0=(est_freq, est_amp, est_phase))

    if plot_ffts:
        f, axs = plt.subplots(3, 1)
        plt.suptitle(
            "Cantilever vibration, %s: %s" % parse_independent_var(fp))
        # raw signal
        df[col].plot(
            style='.', ax=axs[0], ms=1, title='signal: %s position' % col)
        # fft
        axs[1].plot(freq, np.abs(fft)**2)
        axs[1].set_title(r'FFT -- showing $\log(\mathrm{power})$')
        # reconstructed signal overlayed on original
        df = pd.DataFrame({
            'yhat': objective(x, *popt),
            'y_n': df[col] - df[col].mean()})
        df.plot(title='actual vs reconstructed signal', ms=1, ax=axs[2])

        plt.tight_layout()
        plt.show(block=False)
        plt.subplots_adjust(top=0.85)
        f.savefig(join(image_dir, "fft-%s-%s.png" % parse_independent_var(fp)))
        plt.close(f)
    return {'1st_freq': popt[0]}


def plot_total_energy(fp, image_dir):

    f, axs = plt.subplots(4, 1, figsize=(14, 14))
    axs = axs.ravel()
    df = pd.read_csv(fp).set_index('timestep')
    assert len(axs) == df.shape[1] + 1
    for ax, col in zip(axs, df.columns):
        title = col  # TODO remove
        if col == 'gravitational_potential_energy':
            title = ('Gravitational Potential Energy,  '
                     r'GPE $ = \sum_\mathrm{masses} \mathrm{gravity} *'
                     r'\mathrm{height} * \mathrm{mass}$')
        elif col == 'kinetic_energy':
            title = ('Kinetic Energy,  '
                     r'KE $ = \sum_\mathrm{masses} 0.5 *'
                     r'\mathrm{mass} * ||\mathrm{velocity}||_2^2$')
        elif col == 'elastic_potential_energy':
            title = ('Elastic Potential Energy,  '
                     r'EPE $ = \sum_\mathrm{springs} 0.5 * k * (L_0 - L)^2$')
        else:
            raise Exception(
                'unrecognized column in energy trace csv: %s' % col)
        df[col].plot(ax=ax)
        ax.set_title(title, y=1.05)
    df.sum(1).plot(ax=axs[-1])
    axs[-1].set_title('Total Energy (E=GPE + KE + EPE)', y=1.05)

    f.suptitle('Total Energy, %s %s' % parse_independent_var(fp), fontsize=35)
    plt.tight_layout()
    plt.subplots_adjust(top=0.90, hspace=.5)
    f.savefig(join(image_dir,
                   "energy-%s-%s.png" % parse_independent_var(fp)))
    plt.close(f)


def glob_csvs(ns):
    csvs_energy = glob.glob(join(ns.data_dir, 'data_*', '*.trace.energy.csv'))
    csvs = sorted(
        glob.glob(join(ns.data_dir, 'data_*', '*.trace.position.csv')))
    return csvs, csvs_energy


def main(ns):
    os.makedirs(ns.image_dir, exist_ok=True)
    csvs, csvs_energy = glob_csvs(ns)
    if ns.plot_energy:
        for fp in csvs_energy:
            plot_total_energy(fp, ns.image_dir)
    if ns.plot_results:
        _freqs = [compute_freq(fp, ns.image_dir, ns.plot_ffts) for fp in csvs]
        _mi = pd.MultiIndex.from_tuples(
            [parse_independent_var(fp) for fp in csvs],
            names=['var_name', 'var_value'])
        fundamental_freqs = pd.DataFrame(_freqs, index=_mi)['1st_freq']
        plot_experiment_results(fundamental_freqs, csvs_energy, ns)
    globals().update(locals())


def plot_experiment_results(fundamental_freqs, csvs_energy, ns):

    f, axs = plt.subplots(3, 1, figsize=(14, 14))  # dimension tests
    f2, axs2 = plt.subplots(3, 1, figsize=(14, 14))  # material tests
    f3, axs3 = plt.subplots(3, 1, figsize=(14, 14))  # rotation/energy tests
    figs = [f, f2, f3]
    axs = [x for y in [axs, axs2, axs3] for x in y.ravel()]
    assert len(axs) >= fundamental_freqs.index.levels[0].shape[0]
    for var_name in fundamental_freqs.index.levels[0]:
        tmp = fundamental_freqs.xs(var_name)
        tmp.name = "measured freq"
        #  tmp.index.name = var_name
        if var_name == 'beam_x_length':
            #  def func(x): return 1 / x**2
            #  tmp.index.name = 'L'
            tmp.index = tmp.index / get_side_length_from_dml('y', ns.data_dir)
            tmp.index.name = 'L / H'

            def func(x): return 1/x**2  
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto {1/L^2}$',
                tmp=tmp,
                theory_func=func,
                varying='L',
                ax=axs[0],
            )
        elif var_name == 'beam_y_length':
            #  def func(x): return x
            #  tmp.index.name = '%s (num unit cubes along y axis)' % var_name
            tmp.index = tmp.index / get_side_length_from_dml('x', ns.data_dir)
            tmp.index.name = 'H / L'  # hack: more mechanical engineery

            def func(x): return x  # hack: more mechanical engineery
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto H$',
                tmp=tmp,
                theory_func=func,
                varying='H',
                ax=axs[1],
            )
        elif var_name == 'beam_z_length':
            #  def func(x): return 1
            #  tmp.index.name = '%s (num unit cubes along z axis)' % var_name
            tmp.index = get_side_length_from_dml('x', ns.data_dir) / tmp.index
            tmp.index.name = 'L / W'  # hack: make more mechanical engineery

            def func(x): return 1
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto 1$',
                tmp=tmp,
                theory_func=func,
                varying='W',
                ax=axs[2],
            )
        elif var_name == 'density':
            tmp.index.name = r'density'
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto \sqrt{1/p}$',
                tmp=tmp,
                theory_func=lambda x: np.sqrt(1/x),
                varying=r'$\rho$',
                ax=axs[3],
            )
        elif var_name == 'modulus_elasticity':
            tmp.index.name = r'modulus of elasticity'
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto \sqrt{E}$',
                tmp=tmp,
                theory_func=lambda x: np.sqrt(x),
                varying='E',
                ax=axs[4],
            )
        elif var_name == 'spring_length':
            tmp.index.name = r'spring length'
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto 1$',
                tmp=tmp,
                theory_func=lambda x: 1,
                varying='$L_0$',
                ax=axs[5],
            )
        elif var_name == 'rotation':
            tmp.index.name = r'rotation'
            plot_test_result(
                image_dir=ns.image_dir,
                var_name=var_name,
                label=r'theoretical $f \propto 1$',
                tmp=tmp,
                theory_func=lambda x: 1,
                varying='force',
                ax=axs[6],
            )
        elif var_name == 'identical_output':
            pass  # useful pics are the signal plots (and reconstructed fft)
        else:
            raise Exception("unrecognized independent variable: %s" % var_name)

    plot_total_energy_all_experiments(ns, csvs_energy, axs[7], axs[8])

    for n, f in enumerate(figs):
        if n == 0:
            f.suptitle(
                'Cantilever Dimensions Experiments:\n'
                'Measuring first fundamental frequency for cantilever'
                '(normalized).  '
                r'Theory: $f = K / 2\pi * \sqrt{ \frac{EIg}{\rho A L^4} }$')
        elif n == 1:
            f.suptitle((
                'Cantilever Material Properties Experiments:\n'
                'Measuring first fundamental frequency for cantilever'
                '(normalized).  '
                r'Theory: $f = K / 2\pi * \sqrt{ \frac{EIg}{\rho A L^4} }$'))
        elif n == 2:
            f.suptitle('Cantilever Rotation and Energy Experiments')
        else:
            raise Exception("Unknown figure.  What title to add?")

        plt.tight_layout()
        plt.subplots_adjust(top=0.85)
        f.savefig(join(ns.image_dir, "freq_analysis-%s.png" % n))
        plt.close(f)
    globals().update(locals())


def plot_total_energy_all_experiments(ns, csvs_energy, mainax1, mainax2):
    # total energy, data prep
    edf = pd.concat(
        [pd.read_csv(x) for x in csvs_energy],
        keys=[parse_independent_var(fp) for fp in csvs_energy])\
        .reset_index(level=-1, drop=True).sort_index()
    edf.index.set_names(['experiment', 'indep_var'], inplace=True)
    edf.set_index('timestep', append=True, inplace=True)

    # Total Energy of each simulation, all experiments
    edftmp = edf.sum(1)\
        .groupby(['experiment', 'indep_var'])\
        .transform(lambda x: (x - x.mean()) / x.mean())\
        .unstack(level=['experiment', 'indep_var'])
    f, axs = plt.subplots(
        (len(edftmp.columns.levels[0]) + 1) // 2, 2, figsize=(14, 14))
    for col, ax in zip(edftmp.columns.levels[0], axs.ravel()):
        t = edftmp[col].stack()
        t.name = "total_energy"
        sns.tsplot(
            time='timestep', value='total_energy', unit='indep_var',
            data=t.reset_index(), ax=ax)
        sns.tsplot(
            time='timestep', value='total_energy', unit='indep_var',
            data=t.reset_index(), ax=mainax1)
        ax.set_title(col)
        #  edftmp[col].plot(ax=ax, title=col, cmap=plt.cm.YlGnBu)
        #  ax.legend(mode='expand', ncol=3, framealpha=.2)
    mainax1.set_title('Total Energy')
    f.text(0.04, 0.5, (r"% Deviation from Simulation's Avg. Energy "
                       r'($\frac{x - \mu}{\mu}$)'),
           va='center', rotation='vertical')
    f.subplots_adjust(top=0.88, hspace=.8)
    f.suptitle('Total Energy as Percent Deviation from Simulation Mean'
               ', grouped by Experiment')
    f.savefig(join(ns.image_dir, "%s.png" % 'total_energy'))
    plt.close(f)

    # Total Energy slope bar plot
    polyfit_slopes = edf.sum(1).groupby(['experiment', 'indep_var'])\
        .apply(lambda x: np.polyfit(x.reset_index()['timestep'].values,
                                    x.values, 1)[0])\
        .groupby(level=0).describe().drop(['count', 'min', 'max'], axis=1)
    f, ax = plt.subplots()
    for ax in [mainax2, ax]:
        polyfit_slopes.plot.bar(ylim=(-.3, .3), rot=35, ax=ax)
        ax.set_title("Energy Analysis: Distribution of slopes of the\n"
                     " best-fit line for each experiment's Total Energy")
    f.savefig(join(ns.image_dir, "%s.png" % 'total_energy_slopes'))
    plt.close(f)


def plot_test_result(image_dir,
                     var_name, label, tmp, theory_func, varying, ax):
    (tmp / tmp.max()).plot(
        ax=ax, style='.', ylim=(-.1, 1.1),
        title='%s. varying %s.' % (var_name, varying))

    ti = np.linspace(tmp.index.min(), tmp.index.max(), 100)
    tv = theory_func(ti)
    theoretical_freqs = pd.Series(tv, index=ti, name=label)
    theoretical_freqs = theoretical_freqs / theoretical_freqs.max()

    theoretical_freqs.plot(style='-.', ax=ax)
    ax.legend(fancybox=True, framealpha=0.5)

    f2, ax2 = plt.subplots(1, 1, figsize=(8, 5))
    (tmp / tmp.max()).plot(ax=ax2, style='.', ylim=(-.1, 1.1), title=(
        'Cantilever first fundamental frequency.'
        r' Varying %s.  %s.' % (varying, label))
    )
    theoretical_freqs.plot(style='-', ax=ax2)
    ax2.legend(loc='top right', fancybox=True, framealpha=0.5)
    plt.tight_layout()
    f2.savefig(join(image_dir, "%s.png" % var_name))
    plt.close(f2)


def get_side_length_from_dml(side, data_dir):
    assert side in {'x', 'y', 'z'}
    fp = join(data_dir, 'generate_dml_template.dml')
    xml = etree.parse(fp)
    cantilever = xml.xpath('/dml/volume[@id="cantilever"]')[0]
    opts = dict(zip(
        'xyz', [float(x) for x in cantilever.attrib['scale'].split(',')]))
    return opts[side]


def build_arg_parser():
    p = ap.ArgumentParser(formatter_class=ap.ArgumentDefaultsHelpFormatter)
    p.add_argument(
        '--plot-ffts', action="store_true",
        help="save a fft plot for each simulation.  Creates many images.")
    p.add_argument(
        '--plot-energy', action="store_true",
        help=("save a total energy plot for each simulation."
              "Creates many images."))
    p.add_argument(
        '--plot-results', action="store_true",
        help=("plots that summarize the experiment results"))
    p.add_argument(
        '--image-dir', help="dir to save img files",
        default=join(dirname(abspath(__file__)), 'img'))
    p.add_argument(
        '--data-dir', help='dir where data directories are',
        default=dirname(abspath(__file__)))
    return p


if __name__ == '__main__':
    main(build_arg_parser().parse_args())
