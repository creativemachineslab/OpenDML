#version 330 core

out vec4 FragColor;

in vec3 Normal;
in vec3 FragPos;
in vec3 Color;

uniform vec3 lightPos;
uniform vec3 lightColor;

void main() {
	// ambient light source
	float ambientStrength = 0.75;
	vec3 ambient = ambientStrength * lightColor;

	// diffuse lighting
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lightPos - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;

	vec3 result = (ambient + diffuse) * Color;
	FragColor = vec4(result, 1.0);
}