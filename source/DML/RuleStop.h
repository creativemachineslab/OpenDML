#pragma once
namespace DML {
struct RuleStop {
  unsigned long long simulation_ns;
  unsigned long long wall_clock_ns;
};
}  // namespace DML
