#pragma once
#include "LoadcaseProperty.h"

namespace DML {
class Loadcase {
 public:
  Loadcase();
  ~Loadcase();
  void AddProperty(LoadcaseProperty);
  void setID(std::string id);
  std::string GetID();
  std::vector<LoadcaseProperty> Properties();

 private:
  std::vector<LoadcaseProperty> properties;
  std::string id;
};
}  // namespace DML
