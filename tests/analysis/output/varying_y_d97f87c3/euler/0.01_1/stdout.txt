Hiding window!
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.1, 0.01, 0.01
 translate: 0, 0, 0
 rot: 0, 0, 0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.011, 0.011
 translate: -0.0025, -0.0005, -0.0005
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.011, 0.011
 translate: 0.0975, -0.0005, -0.0005
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.011, 0.011
 translate: 0.0975, -0.0005, -0.0005
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0, -200, 0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
  damping
   amount: 0.0
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: output/varying_y_d97f87c3/euler/0.01_1/
  integration
   method: euler
  stop
   simulation_time: 100
   wall_clock_time: 0
   time_unit: ms
 
1
cantilever xMin: 0
cantilever xMax: 0.1
cantilever yMin: 0
cantilever yMax: 0.01
cantilever zMin: 0
cantilever zMax: 0.01
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0105
anchor zMin: -0.0005
anchor zMax: 0.0105
1
loadpoint xMin: 0.0975
loadpoint xMax: 0.1025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0105
loadpoint zMin: -0.0005
loadpoint zMax: 0.0105
1
trace xMin: 0.0975
trace xMax: 0.1025
trace yMin: -0.0005
trace yMax: 0.0105
trace zMin: -0.0005
trace zMax: 0.0105
cantilever xMin: 0
cantilever xMax: 0.1
cantilever yMin: 0
cantilever yMax: 0.01
cantilever zMin: 0
cantilever zMax: 0.01
Output dir: output/varying_y_d97f87c3/euler/0.01_1/
Number under force: 9
Number fixed: 9
Number traced: 9
All masses were unique
force is: 10000
force per mass is: 60
Volume mass: 0.029887
BBOX From masses: 
mass_xmax: 0.1
mass_xmin: 0
mass_ymax: 0.01
mass_ymin: 0
mass_zmax: 0.01
mass_zmin: 0
Max Spring stiffness: 1.78757e+07
Updates enabled
