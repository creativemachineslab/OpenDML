#!/usr/bin/env bash

root_dir=$( cd "$( dirname "$( dirname "${BASH_SOURCE[0]}" )" )" && pwd )

if [ -z ${WORK+x} ]; then
	mkdir -p $root_dir/lib
	WORK=$root_dir/lib
	NUM_CORES='4'
fi
echo "WORK is set to: '$WORK'"
echo "NUM_CORES is set to : '$NUM_CORES'"

# load install funcs
. $root_dir/scripts/system_deps.sh

tacc_load_profile() {
# this should be in ~/.profile
	module load gcc/5.4.0

	export_shell_variables
	export NUM_CORES=20
}

full_install() {
# install and configure maverick with our dependencies

cd $root_dir
git submodule init
git submodule update
cd $WORK
tacc_load_profile
install_cmake
install_pugixml
install_gmp
install_mpfr
install_boost
install_cgal
install_test_python
install_test_python_libs
install_glm

cd $root_dir
scripts/build -DUSE_QT=0 -DUSE_CUDA=0

echo 'OpenDML Built successfully!'
}

run_tests() {
  cd $root_dir
  (nohup $WORK/OpenDML/tests/run_tacc.sh >$WORK/test_stdout-`date +%Y%m%d`.log 2>$WORK/test_stderr-`date +%Y%m%d`.log) &
}

# full_install
