#pragma once
#include <iostream>

namespace DML {

class RuleDevelopment {
 public:
  RuleDevelopment();
  ~RuleDevelopment();
  std::string rule;
};

}  // namespace DML
