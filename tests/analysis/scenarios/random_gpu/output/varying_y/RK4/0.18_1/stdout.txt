Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.18, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.181, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.181, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.181, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.18_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.18
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.1805
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.1805
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.1805
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.18_1
All masses were unique
Calculated number of masses: 7585
Max Spring Connections: 20
Min Spring Connections: 11
Average Spring Connections: 19.9341
Max Spring Length: 0.0187452
Min Spring Length: 0.00333491
Max Spring Stiffness: 2.68008e+07
Min Spring Stiffness: 4.76806e+06
Average Spring Stiffness: 1.40388e+07
7585 masses
initial centroid0.0892229
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
3600
3659
3679
3683
3698
3704
3706
3725
3728
3743
3744
3745
3757
3768
3779
3781
3785
3787
3793
3806
3845
3848
3864
3866
3944
3953
3967
3971
3977
3989
3992
4009
4016
4036
4089
4099
4155
4177
4200
4201
4203
4215
4241
4273
4306
4317
4333
4344
4352
4363
4380
4459
4512
4521
4568
4583
4605
4648
4657
4719
4726
4735
4747
4811
4813
4868
4874
4938
4993
5010
5115
5187
5292
5304
5527
5561
5604
5725
5727
5821
5924
6026
6140
6154
6194
6199
6366
6419
6523
6647
6664
6785
6884
6911
6987
6998
7066
7095
7120
7147
7215
7239
7273
7297
7324
7345
7392
7407
7424
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.0892229
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 1178.56
Avg Time Per Step: 0.00117856
Masses per Second: 6.43582e+06
Springs per Second: 6.4146e+07
