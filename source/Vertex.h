#pragma once

#include <glm/vec3.hpp>

struct Vertex {
  glm::dvec3 position;
  glm::vec3 normal;
  glm::vec3 color;
};
