#include "MainWindow.h"
#include "ArgParser.h"
#include "CPUSpringMass.h"
#include "Statistics.h"

#include <Qt3DExtras/Qt3DExtras>
#include <Qt3DRender/QRenderCapture>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>

#include <QtCore/QTimer>

#include "Analysis.h"
#include "Telemetry.h"
#include "Utils.h"

using namespace std;
using namespace QtCharts;

const QColor background_color = QColor(QRgb(0xECECEC));

MainWindow::MainWindow() {
  // Set output dir
  auto view = new Qt3DExtras::Qt3DWindow();
  view->defaultFrameGraph()->setClearColor(background_color);
  // view->defaultFrameGraph()->setClearColor(QColor(QRgb(0xFFA07A)));
  auto container = QWidget::createWindowContainer(view);
  auto screenSize = view->screen()->size();
  container->setMinimumSize(QSize(200, 100));
  container->setMaximumSize(screenSize);

  auto root_layout = new QHBoxLayout(this);
  auto side_panel_layout = new QVBoxLayout();
  side_panel_layout->setAlignment(Qt::AlignBottom);
  root_layout->addWidget(container, 1);
  root_layout->addLayout(side_panel_layout);

  auto input = new Qt3DInput::QInputAspect;
  view->registerAspect(input);

  // Root entity
  rootEntity = new Qt3DCore::QEntity();

  // Camera
  camera = view->camera();

  camera->lens()->setPerspectiveProjection(55.0f, 16.0f / 9.0f, 0.01f, 1000.0f);
  auto lightEntity = new Qt3DCore::QEntity(rootEntity);
  auto light = new Qt3DRender::QPointLight(lightEntity);
  light->setColor("white");
  light->setIntensity(1);
  lightEntity->addComponent(light);
  auto lightTransform = new Qt3DCore::QTransform(lightEntity);
  // lightTransform->setTranslation(camera->position());
  lightEntity->addComponent(lightTransform);

  // For camera controls
  auto camController = new Qt3DExtras::QOrbitCameraController(rootEntity);
  camController->setLinearSpeed(1.0f);
  camController->setLookSpeed(300.0f);
  camController->setCamera(camera);

  // Scenemodifier
  modifier = new SceneModifier(rootEntity, &Simulator::instance());
  modifier->AddBoundingBoxes();

  energy_chart = new LiveLineChart(&Analysis::GetCurrentyEnergy);
  chart_update_timer = new QTimer(this);
  chart_capture_timer = new QTimer(this);
  text_stats = new QPlainTextEdit(this);

  energy_chart->chart_image_directory = Simulator::instance().output_dir / "charts";

  for (const auto cv : energy_chart->chart_views) {
    side_panel_layout->addWidget(cv);
  }
  // Create a frame action
  // which we later use for getting
  // a signal each time the frame is rendered
  frame_action = new Qt3DLogic::QFrameAction;
  rootEntity->addComponent(frame_action);

  auto capture = new Qt3DRender::QRenderCapture();
  auto active_framegraph = view->activeFrameGraph();

  active_framegraph->setParent(capture);
  view->setActiveFrameGraph(capture);
  render_capture = new RenderCapture(capture);
  render_capture->capture_image_directory = Simulator::instance().output_dir / "capture";

  // Set root object of the scene
  view->setRootEntity(rootEntity);

  // Start/stop simulator button
  toggle_sim = new QPushButton("Start");
  reset_camera = new QPushButton("Reset Camera");

  // Add toggles
  toggle_masses = new QCheckBox(this);
  toggle_masses->setChecked(true);
  toggle_masses->setText(QStringLiteral("Show Masses"));

  toggle_springs = new QCheckBox(this);
  toggle_springs->setChecked(true);
  toggle_springs->setText(QStringLiteral("Show Springs"));

  toggle_force_vectors = new QCheckBox(this);
  toggle_force_vectors->setChecked(false);
  toggle_force_vectors->setText(QStringLiteral("Show Force Vectors"));

  toggle_bbox = new QCheckBox(this);
  toggle_bbox->setChecked(false);
  toggle_bbox->setText(QStringLiteral("Show Volumes"));

  toggle_capture_charts = new QCheckBox(this);
  toggle_capture_charts->setChecked(false);
  toggle_capture_charts->setText(QStringLiteral("Capture Charts"));

  toggle_capture_render = new QCheckBox(this);
  toggle_capture_render->setChecked(false);
  toggle_capture_render->setText(QStringLiteral("Capture Render"));

  // text_stats->setReadOnly(true);
  text_stats->setFixedHeight(60);
  side_panel_layout->addWidget(text_stats);
  side_panel_layout->addWidget(toggle_sim);
  side_panel_layout->addWidget(reset_camera);
  side_panel_layout->addWidget(toggle_masses);
  side_panel_layout->addWidget(toggle_springs);
  side_panel_layout->addWidget(toggle_force_vectors);
  side_panel_layout->addWidget(toggle_bbox);
  side_panel_layout->addWidget(toggle_capture_charts);
  side_panel_layout->addWidget(toggle_capture_render);

  connect(toggle_sim, &QPushButton::clicked, this, &MainWindow::ToggleSimulator);

  connect(toggle_capture_charts,
          &QCheckBox::stateChanged,
          this,
          &MainWindow::ToggleCaptureCharts);

  connect(toggle_capture_render,
          &QCheckBox::stateChanged,
          this,
          &MainWindow::ToggleCaptureRender);

  connect(reset_camera, &QPushButton::clicked, this, &MainWindow::ResetAndCenterCamera);

  connect(
      toggle_masses, &QCheckBox::stateChanged, modifier, &SceneModifier::enableMasses);

  connect(toggle_force_vectors,
          &QCheckBox::stateChanged,
          modifier,
          &SceneModifier::enableForceVectors);

  connect(
      toggle_springs, &QCheckBox::stateChanged, modifier, &SceneModifier::enableSprings);

  connect(toggle_bbox, &QCheckBox::stateChanged, modifier, &SceneModifier::enableBBox);

  modifier->render();

  // cout << "Centroid starting value: " << endl;
  // cout << Analysis::GetVolumeTrace().y << endl;

  // set from DML
  resize(1200, 800);
}

MainWindow::~MainWindow() {}

void MainWindow::ShowSimulatorOutput() {
  ResetAndCenterCamera();
}

void MainWindow::ResetAndCenterCamera() {
  auto bbox = Simulator::instance().lattice_volume->bbox;
  auto translateX = bbox.xmin() + (bbox.xmax() - bbox.xmin()) / 2.0f;
  auto translateY = bbox.ymin() + (bbox.ymax() - bbox.ymin()) / 2.0f;
  auto translateZ = bbox.xmax();
  camera->setPosition(QVector3D(0.0f, 0.0f, translateZ + 0.1f));
  camera->setUpVector(QVector3D(0, 1.0f, 0.0f));
  camera->setViewCenter(QVector3D(0, 0, 0));
  // jclay: tmp
  camera->translate(QVector3D(translateX, translateY, 0));
}

void MainWindow::ToggleSimulator() {
  if (!simulator_running) {
    connect(chart_update_timer, &QTimer::timeout, energy_chart, &LiveLineChart::update);

    connect(chart_update_timer, &QTimer::timeout, this, &MainWindow::UpdateStats);

    // connect(chart_update_timer, &QTimer::timeout, [=]() {
    //  if (Simulator::instance().released) {
    //    Telemetry::position_trace.update();
    //  }
    //});

    connect(frame_action,
            &Qt3DLogic::QFrameAction::triggered,
            modifier,
            &SceneModifier::render);

    // QObject::connect(frame_action, &Qt3DLogic::QFrameAction::triggered,
    //                 modifier, &CPUSpringMass::Step);

    // aim for ~30 fps
    chart_update_timer->start(33);
    simulator_running = true;
    toggle_sim->setText("Stop");
  } else {
    disconnect(
        chart_update_timer, &QTimer::timeout, energy_chart, &LiveLineChart::update);

    // todo
    // disconnect(telemetry_conn);

    // QObject::disconnect(frame_action, &Qt3DLogic::QFrameAction::triggered,
    //                    modifier, &CPUSpringMass::Step);

    disconnect(frame_action,
               &Qt3DLogic::QFrameAction::triggered,
               modifier,
               &SceneModifier::render);

    simulator_running = false;
    modifier->sim->cleanup();
    toggle_sim->setText("Start");
  }
  modifier->sim->setEnableUpdateLoop(simulator_running);
}

void MainWindow::UpdateStats() {
  const auto std_ed = Analysis::GetStdDevEnergy(100);
  QString stats = "";
  const auto time = Simulator::instance().spring_mass->GetSimulationTimeMS();
  stats.append(QString("Sim Time (Ms): %1\n").arg(time));
  stats.append(QString("Total Energy S.D.: %1\n").arg(std_ed.total_energy));
  stats.append(QString("Kinetic Energy S.D.: %1\n").arg(std_ed.kinetic_energy));
  stats.append(QString("GPE S.D.: %1\n").arg(std_ed.gravitational_potential_energy));
  stats.append(QString("EPE S.D.: %1\n").arg(std_ed.elastic_potential_energy));
  stats.append(QString("EPE S.D.: %1\n").arg(std_ed.elastic_potential_energy));
  text_stats->setPlainText(stats);
}

void MainWindow::ToggleCaptureCharts(bool enabled) {
  if (enabled) {
    connect(chart_capture_timer, &QTimer::timeout, energy_chart, &LiveLineChart::capture);

    chart_capture_timer->start(33);
  } else {
    disconnect(
        chart_capture_timer, &QTimer::timeout, energy_chart, &LiveLineChart::capture);
  }
}

void MainWindow::ToggleCaptureRender(bool enabled) {
  if (enabled) {
    render_capture_timer = new QTimer(this);
    connect(
        render_capture_timer, &QTimer::timeout, render_capture, &RenderCapture::capture);

    // one capture per second
    render_capture_timer->start(1000);
  } else {
    disconnect(
        render_capture_timer, &QTimer::timeout, render_capture, &RenderCapture::capture);
  }
}

#include <iostream>

void MainWindow::SimulateUntil(unsigned long long timeout_simulation_ns,
                               unsigned long long timeout_wall_clock_ns) {
  if (timeout_simulation_ns > 0)
    frame_action->connect(frame_action, &Qt3DLogic::QFrameAction::triggered, [=]() {
      // cout << CPUSpringMass::GetSimulationTime() - timeout_simulation_ns << endl;
      if (modifier->sim->spring_mass->isDone()) {
        disconnect(
            chart_update_timer, &QTimer::timeout, energy_chart, &LiveLineChart::update);

        disconnect(frame_action,
                   &Qt3DLogic::QFrameAction::triggered,
                   modifier,
                   &SceneModifier::render);

        // cout << "Stopped at " << time / 1e6 << " ms" << endl;
        simulator_running = false;
        modifier->sim->setEnableUpdateLoop(simulator_running);
        modifier->sim->cleanup();
        toggle_sim->setText("Start");
        // Simulator::instance().spring_mass->disableUpdating();
        qApp->quit();
      }
    });
  // if (timeout_wall_clock_ns > 0) {
  //  QTimer::singleShot(timeout_wall_clock_ns / 1000000, this,
  //                     QApplication::quit);
  //}
}

void MainWindow::PrintDebugInfo() {
  cout << "Number of springs: " << modifier->sim->springs->size() << endl;
  cout << "Number of masses: " << modifier->sim->masses->size() << endl;
  auto total_mass = 0.0;
  for (auto m : *modifier->sim->masses) {
    total_mass += m->mass;
  }

  for (const auto map : modifier->sim->volumes) {
    const auto volume = map.second;
    cout << volume->id << ": ";
    cout << volume->masses_inside.size() << " masses" << endl;
  }

  cout << "Total mass: " << total_mass << " kg" << endl;
}

int CreateAndRunQTApp(int argc, char** argv) {
  // QT might remove GUI specific args, but only ArgParser args are allowed.
  ArgParser::parse(argc, argv);
  string dml_fp = ArgParser::dmlFilepath.Get();
  DML::Config config = DML::Parser::LoadDML(dml_fp);
  Simulator::instance().ConfigureFromDML(config);
  auto volume = Simulator::instance().lattice_volume;

  QApplication app(argc, argv);

  MainWindow window;
  // let the main window manage the simulator until
  // we can come up with a better idea for who/what
  // manages this state
  window.resize(config.GetRule()->GetOutput().windowWidth,
                config.GetRule()->GetOutput().windowHeight);

  window.setWindowTitle(
      QString::fromStdString("OpenDML - "s + VERSION + " - "s + dml_fp));
  window.ShowSimulatorOutput();
  window.PrintDebugInfo();
  window.show();

  if (ArgParser::applyForces.Get()) {
    window.ToggleSimulator();
    window.toggle_masses->setChecked(false);
    window.toggle_springs->setChecked(false);
  }

  auto timeout_simulation_ns = config.GetRule()->GetStop().simulation_ns;
  auto timeout_wall_clock_ns = config.GetRule()->GetStop().wall_clock_ns;
  if (timeout_simulation_ns > 0 || timeout_wall_clock_ns > 0) {
    window.SimulateUntil(timeout_simulation_ns, timeout_wall_clock_ns);
  }
  return qApp->exec();
}
