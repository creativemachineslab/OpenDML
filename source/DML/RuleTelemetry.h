#pragma once
#include <iostream>

namespace DML {
class RuleTelemetry {
 public:
  RuleTelemetry() = default;
  ~RuleTelemetry() = default;
  std::string output_dir = "";
};
}  // namespace DML
