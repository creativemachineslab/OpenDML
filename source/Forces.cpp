#include "Forces.h"
#include <iostream>

namespace Forces {

Constant::Constant(dvec3 val) {
  this->val = val;
}
glm::dvec3 Constant::Next(unsigned long long time_ns) {
  return this->val;
}

Pulse::Pulse(dvec3 on_val,
             dvec3 off_val,
             unsigned long long on_ns,
             unsigned long long off_ns,
             bool repeat) {
  this->on_ns = on_ns;
  this->period = on_ns + off_ns;
  this->on_val = on_val;
  this->off_val = off_val;
  this->repeat = repeat;
}

glm::dvec3 Pulse::Next(unsigned long long time_ns) {
  if (!repeat && time_ns >= on_ns) {
    return off_val;
  } else if (time_ns % period < on_ns) {
    return on_val;
  } else {
    return off_val;
  }
}
}  // namespace Forces
