# Cronos

## Building from Source

### Dependencies

[Vcpkg](https://github.com/Microsoft/vcpkg) is used to manage the external dependencies.
After installing and boostrapping vcpkg, the `Cronos` dependencies can be installed as follows.

**System Dependencies**

```
sudo apt install build-essential yasm cmake
```

```
~/vcpkg> ./vcpkg install glm pugixml args boost-filesystem boost-chrono boost-timer cgal tbb spdlog parallelstl
```

#### Analysis Dependencies

Use of conda is recommended.

Create a new conda environment:

```
$> conda create -n opendml python=3.7
```


Install dependencies:
```
$> conda activate opendml
$> conda install lxml numpy requests psutil plotly joblib
$> conda install -c plotly plotly-orca
```

## Getting Started

See the [Getting Started Guide ](https://gitlab.com/creativemachineslab/OpenDML/wikis/Getting-Started-with-OpenDML) available in the Wiki

## Documentation

See the [wiki](https://gitlab.com/creativemachineslab/SpringCube/wikis/home) for more information.
