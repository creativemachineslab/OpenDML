Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.02, 0.19999999999999998
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.20099999999999998
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.20099999999999998
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.20099999999999998
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu\output\varying_z\verlet\0.2_1
  integration
   method: verlet
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.2
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.2005
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.2005
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.2005
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu\output\varying_z\verlet\0.2_1
All masses were unique
Calculated number of masses: 8405
Max Spring Connections: 20
Min Spring Connections: 10
Average Spring Connections: 19.9329
Max Spring Length: 0.0161284
Min Spring Length: 0.00332679
Max Spring Stiffness: 2.68663e+07
Min Spring Stiffness: 5.54168e+06
Average Spring Stiffness: 1.40232e+07
8405 masses
initial centroid0.00970312
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using Verlet integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
3621
3663
3671
3674
3684
3696
3703
3717
3719
3730
3734
3738
3740
3749
3754
3760
3761
3764
3790
3800
3804
3813
3826
3863
3883
3917
3926
3935
3977
3979
3986
3989
4010
4041
4052
4070
4086
4094
4147
4182
4201
4228
4233
4236
4237
4267
4302
4313
4356
4378
4385
4440
4490
4493
4496
4560
4572
4581
4583
4650
4673
4678
4681
4689
4725
4773
4798
4805
4849
4945
4967
5023
5044
5065
5190
5258
5307
5432
5467
5497
5522
5583
5637
5811
5874
5899
6109
6227
6292
6489
6553
6618
6713
6812
6826
6888
6956
7004
7092
7101
7157
7179
7328
7451
7469
7472
7649
7694
7795
7822
7852
7912
7952
7993
8019
8026
8084
8118
8136
8161
8164
8185
8210
8250
8259
8263
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.00970312
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 294.499
Avg Time Per Step: 0.000294499
Masses per Second: 2.854e+07
Springs per Second: 2.84442e+08
