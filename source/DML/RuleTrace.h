#pragma once
#include "Volume.h"

namespace DML {

class RuleTrace {
 public:
  RuleTrace();
  ~RuleTrace();
  DML::Volume* volume;
  std::string value;
  std::string file;
};

}  // namespace DML
