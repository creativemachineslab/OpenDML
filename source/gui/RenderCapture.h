/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd and/or its subsidiary(-ies).
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt3D module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/
#pragma once

#include <QLabel>
#include <Qt3DRender/QRenderCapture>

class RenderCapture : public QObject {
  Q_OBJECT
 public:
  RenderCapture(Qt3DRender::QRenderCapture* capture)
      : m_capture(capture), m_reply(nullptr), m_continuous(false) {}

  boost::filesystem::path capture_image_directory;
 public slots:
  // Designed to be called from QFrameAction triggered event
  void onFrameTriggered(float dt) { std::cout << dt << std::endl; }

  void onCompleted() {
    auto filename = QString("%1.png").arg(m_reply->captureId()).toStdString();
    auto out_path = capture_image_directory;

    if (!(exists(out_path))) {
      create_directories(out_path);
    }

    out_path = out_path / filename;
    QObject::disconnect(connection);
    m_reply->saveImage(QString().fromStdString(out_path.string()));
    delete m_reply;
    m_reply = nullptr;
    if (m_continuous)
      capture();
  }

  void setContinuous(bool continuos) { m_continuous = continuos; }

  void capture() {
    if (!m_reply) {
      m_reply = m_capture->requestCapture();
      connection = QObject::connect(m_reply,
                                    &Qt3DRender::QRenderCaptureReply::completed,
                                    this,
                                    &RenderCapture::onCompleted);
    }
  }

 private:
  Qt3DRender::QRenderCapture* m_capture;
  Qt3DRender::QRenderCaptureReply* m_reply;
  QMetaObject::Connection connection;
  QLabel* m_imageLabel;
  bool m_continuous;
};
