Performance Test Scenario Enabled
Running performance tests 
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 7.4, 0.02, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 7.3975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 7.3975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: false
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios/v100_performance_linux/output/performance/verlet/7.4_1
  integration
   method: verlet
   time_step: 1e-06
  stop
   simulation_time: 100
   wall_clock_time: 0
   time_unit: ms
 
is_random 0
1
cantilever xMin: 0
cantilever xMax: 7.4
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 7.3975
loadpoint xMax: 7.4025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 7.3975
trace xMax: 7.4025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 0
Building grid lattice
cantilever xMin: 0
cantilever xMax: 7.4
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
Output dir: scenarios/v100_performance_linux/output/performance/verlet/7.4_1
All masses were unique
Calculated number of masses: 37025
Max Spring Connections: 26
Min Spring Connections: 7
Average Spring Connections: 19.2709
Max Spring Length: 0.00866032
Min Spring Length: 0.00499964
Max Spring Stiffness: 1.7877e+07
Min Spring Stiffness: 1.03204e+07
Average Spring Stiffness: 1.34366e+07
37025 masses
initial centroid0.01
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: Tesla V100-SXM2-16GB
CUDA device major: 7
CUDA device minor: 0
Using Verlet integration method
GPU Acceleration Successfully Enabled
Initializing performance tests...
step_count: 100000
Elapsed Seconds: 77.0109
Avg Time Per Step: 0.000770109
Masses per Second: 4.80776e+07
Springs per Second: 4.63249e+08
