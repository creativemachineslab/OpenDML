Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.02, 0.08
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.081
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.081
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.081
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu\output\varying_z\verlet\0.08_1
  integration
   method: verlet
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.08
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.0805
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.0805
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.0805
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu\output\varying_z\verlet\0.08_1
All masses were unique
Calculated number of masses: 3485
Max Spring Connections: 20
Min Spring Connections: 11
Average Spring Connections: 19.8984
Max Spring Length: 0.018156
Min Spring Length: 0.00328517
Max Spring Stiffness: 2.72066e+07
Min Spring Stiffness: 4.92279e+06
Average Spring Stiffness: 1.40085e+07
3485 masses
initial centroid0.010243
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using Verlet integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
2496
2499
2501
2507
2517
2519
2530
2536
2537
2538
2540
2550
2567
2570
2573
2574
2576
2580
2584
2599
2602
2612
2615
2617
2621
2622
2629
2635
2649
2664
2668
2676
2683
2695
2707
2716
2731
2734
2749
2759
2793
2804
2806
2821
2854
2865
2870
2904
2942
2957
2964
2988
2994
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.010243
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 104.038
Avg Time Per Step: 0.000104038
Masses per Second: 3.34974e+07
Springs per Second: 3.33272e+08
