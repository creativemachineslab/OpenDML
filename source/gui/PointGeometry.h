#include "Vertex.h"

#include <Qt3DExtras/qt3dextras_global.h>
#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qgeometryrenderer.h>
#include <QtCore/qsize.h>
#include <qvector3d.h>
#include <vector>

class Point : public Qt3DRender::QGeometryRenderer {
  Q_OBJECT
 public:
  explicit Point(Qt3DCore::QNode* parent = nullptr);
  ~Point();
 public slots:
  void setVertices(std::vector<Vertex> vertices);
};

class PointGeometry : public Qt3DRender::QGeometry {
  Q_OBJECT
 public:
  explicit PointGeometry(QNode* parent = nullptr);
  ~PointGeometry();

  std::vector<Vertex> vertices;

  void init();
  void updateVertices();
  void updateIndices();

  Qt3DRender::QBuffer* vertexBuffer;
  Qt3DRender::QBuffer* indexBuffer;
  Qt3DRender::QAttribute* positionAttribute;
  Qt3DRender::QAttribute* colorAttribute;
  Qt3DRender::QAttribute* indexAttribute;

 public slots:
  void setVertices(std::vector<Vertex> vertices);
};
