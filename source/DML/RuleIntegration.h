#pragma once
#include <iostream>

namespace DML {
class RuleIntegration {
 public:
  RuleIntegration() = default;
  ~RuleIntegration() = default;
  std::string method = "euler";
  double time_step = 0.0;
};
}  // namespace DML
