#include "Material.h"

namespace DML {
Material::Material() {}

Material::~Material() {}

Lattice* Material::GetLattice() {
  return &lattice;
}

void Material::SetLattice(Lattice lattice) {
  this->lattice = lattice;
}
}  // namespace DML
