#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <boost/circular_buffer.hpp>
#include <glm/gtx/common.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/vec3.hpp>
#include <list>
#include "Simulator.h"

const int kNUM_ENERGY_TO_STORE = 1000;
const int kNUM_TRACE_TO_STORE = 1000;

struct EnergyData {
  double time = 0.0;
  double gravitational_potential_energy = 0.0;
  double kinetic_energy = 0.0;
  double elastic_potential_energy = 0.0;
  double total_energy = 0.0;
};

namespace Analysis {
EnergyData GetCurrentyEnergy();
glm::dvec3 GetVolumeTrace();
double GetStdDevTrace(int sample_count = kNUM_TRACE_TO_STORE);
EnergyData GetStdDevEnergy(int sample_count = kNUM_ENERGY_TO_STORE);
extern boost::circular_buffer<EnergyData> energy_data;
extern boost::circular_buffer<glm::dvec3> trace_data;
};  // namespace Analysis
