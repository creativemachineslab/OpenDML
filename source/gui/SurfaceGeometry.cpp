#include "SurfaceGeometry.h"

#include <Qt3DRender/qattribute.h>
#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qbufferdatagenerator.h>
#include <Qt3DRender/qpointsize.h>
#include <limits>

using namespace Qt3DRender;
using namespace Qt3DCore;
using namespace std;

Surface::Surface(QNode* parent) : QGeometryRenderer(parent) {
  SurfaceGeometry* geometry = new SurfaceGeometry(this);

  setPrimitiveType(PrimitiveType::Triangles);
  QGeometryRenderer::setGeometry(geometry);
}

Surface::~Surface() {}

void Surface::setPositions(std::vector<Vertex> positions) const {
  static_cast<SurfaceGeometry*>(geometry())->setPositions(positions);
}

// jclay: creates a plane of specified size in XZ plane
// returns a QByteArray in the following format:
// vec3 position <x.size, 0, z.size>
// vec2 texture coordinate
// vec3 normal <0, 1, 0>
// vec4 tangent
QByteArray createSurfaceVertexData(vector<Vertex> positions) {
  // vec3 for position, vec3 for normal
  const quint32 buf_size = positions.size() * 6 * sizeof(float);
  QByteArray bufferBytes;
  bufferBytes.resize(buf_size);
  float* fptr = reinterpret_cast<float*>(bufferBytes.data());

  for (auto p : positions) {
    // position
    *fptr++ = p.position.x;
    *fptr++ = p.position.y;
    *fptr++ = p.position.z;

    // normal
    *fptr++ = 0.0f;
    *fptr++ = 0.0f;
    *fptr++ = 1.0f;
    //*fptr++ = p.normal.x;
    //*fptr++ = p.normal.y;
    //*fptr++ = p.normal.z;
  }

  return bufferBytes;
}

// this is a bit messy since we're relying on the order in which
// the data is placed into the buffer. since we're in control of
// the whole process, it will suffice for now
QByteArray createSurfaceIndexData(int num_positions) {
  // Create the index data. 2 triangles per rectangular face
  QByteArray indexBytes;
  // 4 positions make a plane
  int num_planes = (num_positions / 4);
  indexBytes.resize(num_planes * 3 * 2 * sizeof(quint32));
  quint32* indexPtr = reinterpret_cast<quint32*>(indexBytes.data());

  for (quint32 i = 0; i < num_positions; i += 4) {
    *indexPtr++ = i;
    *indexPtr++ = i + 1;
    *indexPtr++ = i + 2;

    *indexPtr++ = i + 3;
    *indexPtr++ = i + 2;
    *indexPtr++ = i + 1;
  }

  return indexBytes;
}

class SurfaceVertexBufferFunctor : public QBufferDataGenerator {
 public:
  explicit SurfaceVertexBufferFunctor(vector<Vertex> positions) : positions(positions) {}

  ~SurfaceVertexBufferFunctor() {}

  QByteArray operator()() Q_DECL_FINAL { return createSurfaceVertexData(positions); }

  bool operator==(const QBufferDataGenerator& other) const Q_DECL_FINAL {
    const SurfaceVertexBufferFunctor* otherFunctor =
        functor_cast<SurfaceVertexBufferFunctor>(&other);
    if (otherFunctor != nullptr)
      return (otherFunctor->positions.size() == positions.size());
    return false;
  }

  QT3D_FUNCTOR(SurfaceVertexBufferFunctor)

 private:
  vector<Vertex> positions;
};

class SurfaceIndexBufferFunctor : public QBufferDataGenerator {
 public:
  explicit SurfaceIndexBufferFunctor(int num_positions) : num_positions(num_positions) {}

  ~SurfaceIndexBufferFunctor() {}

  QByteArray operator()() Q_DECL_FINAL { return createSurfaceIndexData(num_positions); }

  bool operator==(const QBufferDataGenerator& other) const Q_DECL_FINAL {
    const SurfaceIndexBufferFunctor* otherFunctor =
        functor_cast<SurfaceIndexBufferFunctor>(&other);
    if (otherFunctor != nullptr)
      return (otherFunctor->num_positions == num_positions);
    return false;
  }

  QT3D_FUNCTOR(SurfaceIndexBufferFunctor)

 private:
  int num_positions;
};

SurfaceGeometry::SurfaceGeometry(SurfaceGeometry::QNode* parent) : QGeometry(parent) {
  this->init();
}

SurfaceGeometry::~SurfaceGeometry() {}

void SurfaceGeometry::setPositions(vector<Vertex> positions) {
  this->positions = positions;
  updateVertices();
  updateIndices();
}

void SurfaceGeometry::updateVertices() {
  const int nVerts = positions.size();
  positionAttribute->setCount(nVerts);
  vertexBuffer->setDataGenerator(
      QSharedPointer<SurfaceVertexBufferFunctor>::create(positions));
}

void SurfaceGeometry::updateIndices() {
  const int indices = positions.size();
  indexAttribute->setCount((indices / 4) * 2 * 3);
  indexBuffer->setDataGenerator(
      QSharedPointer<SurfaceIndexBufferFunctor>::create(indices));
}

void SurfaceGeometry::init() {
  positionAttribute = new QAttribute();
  normalAttribute = new QAttribute();
  indexAttribute = new QAttribute();
  vertexBuffer = new Qt3DRender::QBuffer();
  indexBuffer = new Qt3DRender::QBuffer();

  const int nVerts = positions.size();
  const int stride = 3 * 2 * sizeof(float);

  positionAttribute->setName(QAttribute::defaultPositionAttributeName());
  positionAttribute->setVertexBaseType(QAttribute::Float);
  positionAttribute->setVertexSize(3);
  positionAttribute->setAttributeType(QAttribute::VertexAttribute);
  positionAttribute->setBuffer(vertexBuffer);
  positionAttribute->setByteStride(stride);
  positionAttribute->setCount(nVerts);

  normalAttribute->setName(QAttribute::defaultNormalAttributeName());
  normalAttribute->setVertexBaseType(QAttribute::Float);
  normalAttribute->setVertexSize(3);
  normalAttribute->setAttributeType(QAttribute::VertexAttribute);
  normalAttribute->setBuffer(vertexBuffer);
  normalAttribute->setByteStride(stride);
  normalAttribute->setByteOffset(3 * sizeof(float));
  normalAttribute->setCount(nVerts);

  indexAttribute->setAttributeType(QAttribute::IndexAttribute);
  indexAttribute->setVertexBaseType(QAttribute::UnsignedInt);
  indexAttribute->setBuffer(indexBuffer);

  // Each primitive has 3 vertives
  indexAttribute->setCount((positions.size() / 4) * 2 * 3);

  vertexBuffer->setDataGenerator(
      QSharedPointer<SurfaceVertexBufferFunctor>::create(positions));
  indexBuffer->setDataGenerator(
      QSharedPointer<SurfaceIndexBufferFunctor>::create(positions.size()));

  addAttribute(positionAttribute);
  addAttribute(normalAttribute);
  addAttribute(indexAttribute);
}
