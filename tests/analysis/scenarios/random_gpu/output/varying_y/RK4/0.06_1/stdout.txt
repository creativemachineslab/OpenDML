Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.06, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.061, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.061, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.061, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.06_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.06
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0605
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0605
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.0605
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.06_1
All masses were unique
Calculated number of masses: 2665
Max Spring Connections: 20
Min Spring Connections: 11
Average Spring Connections: 19.8964
Max Spring Length: 0.0168188
Min Spring Length: 0.00336359
Max Spring Stiffness: 2.65723e+07
Min Spring Stiffness: 5.31419e+06
Average Spring Stiffness: 1.40125e+07
2665 masses
initial centroid0.0285744
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
2004
2013
2014
2015
2016
2024
2025
2026
2027
2028
2030
2031
2033
2034
2039
2041
2042
2045
2049
2050
2053
2059
2067
2069
2073
2078
2088
2102
2105
2112
2119
2125
2126
2139
2143
2175
2208
2211
2215
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.0285744
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 388.284
Avg Time Per Step: 0.000388284
Masses per Second: 6.86353e+06
Springs per Second: 6.82799e+07
