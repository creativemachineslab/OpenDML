Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.25000000000000006, 0.02, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 0.24750000000000005, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 0.24750000000000005, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: false
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: false
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\rk4_cpu\output\varying_x\RK4\0.25_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 100
   wall_clock_time: 0
   time_unit: ms
 
is_random 0
1
cantilever xMin: 0
cantilever xMax: 0.25
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.2475
loadpoint xMax: 0.2525
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.2475
trace xMax: 0.2525
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 0
Building grid lattice
cantilever xMin: 0
cantilever xMax: 0.25
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
Output dir: scenarios\rk4_cpu\output\varying_x\RK4\0.25_1
All masses were unique
Calculated number of masses: 1275
Max Spring Connections: 26
Min Spring Connections: 7
Average Spring Connections: 19.0149
Max Spring Length: 0.00866026
Min Spring Length: 0.005
Max Spring Stiffness: 1.78757e+07
Min Spring Stiffness: 1.03205e+07
Average Spring Stiffness: 1.34487e+07
1275 masses
initial centroid0.01
Using RK4 integration method
Release damping at: 500ms
Release point position: 0.0100179
Elapsed Seconds: 1724.57
Avg Time Per Step: 0.00287428
Masses per Second: 443590
Springs per Second: 4.21741e+06
