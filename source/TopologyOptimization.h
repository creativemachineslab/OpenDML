#pragma once
#include <algorithm>
#include <memory>
#include <sstream>

#include "Globals.h"
#include "Mass.h"
#include "Spring.h"

namespace TopOpt {
// TODO: move this typedef somewhere else so it's shared
typedef std::shared_ptr<std::vector<std::shared_ptr<Mass>>> Masses;
typedef std::shared_ptr<std::vector<std::shared_ptr<Spring>>> Springs;

struct Optimizer {
  virtual void Optimize() = 0;
  virtual bool Enabled() = 0;
  virtual std::string Name() = 0;
};

struct DropSprings : Optimizer {
  Masses masses;
  Springs springs;

  const double initial_spring_count;
  const int initial_mass_count;
  double initial_total_mass = 0.0;

  int num_to_remove;
  double stop_at_pct_springs_remaining;

  DropSprings(int num_to_remove,
              double stop_at_pct_springs_remaining,
              Masses masses,
              Springs springs);

  void DeleteSpring(std::shared_ptr<Spring> spring);
  void DeleteMass(std::shared_ptr<Mass> mass);

  bool Enabled() override;
  void Optimize() override;

  std::string Name() override {
    std::ostringstream stm;
    stm << "DropSprings(" << num_to_remove << ", " << stop_at_pct_springs_remaining
        << ")";
    return stm.str();
  }

  void PrintSummary() {
    auto total_mass = 0.0;
    for (auto const m : *masses) {
      total_mass += m->mass;
    }
    std::cout << "DropSprings Optimization report:" << std::endl;
    std::cout << "Starting Mass: " << initial_total_mass << " kg" << std::endl;
    std::cout << "Ending Mass: " << total_mass << " kg" << std::endl;
    std::cout << "Bars Before: " << initial_spring_count << std::endl;
    std::cout << "Bars After: " << springs->size() << std::endl;
    std::cout << "Masses Before: " << initial_mass_count << std::endl;
    std::cout << "Masses After: " << masses->size() << std::endl;
    std::cout << std::endl;
  }
};

}  // namespace TopOpt
