#include "Geometry.h"
#include <CGAL/IO/Polyhedron_builder_from_STL.h>

using namespace std;

namespace Geometry {
// src:
// https://doc.cgal.org/latest/Polyhedron/Polyhedron_2polyhedron_prog_cube_8cpp-example.html
Halfedge_handle MakeCube(Polyhedron& P, float xmax, float ymax, float zmax) {
  // apply a small amount of fuzzing so that we can avoid boundary conditions
  // due to the way in which we are creating the volumes, there is often a condition
  // where the mass lies exactly on the boundary of the volume, so we can apply a little
  // "padding" to prevent this from happening
  const double eps = 0;  // 1e-7;
  CGAL_precondition(P.is_valid());
  Halfedge_handle h = P.make_tetrahedron(Point3(xmax + eps, -eps, -eps),
                                         Point3(-eps, -eps, zmax + eps),
                                         Point3(-eps, -eps, -eps),
                                         Point3(-eps, ymax + eps, -eps));
  cout << P.is_tetrahedron(h) << endl;
  Halfedge_handle g = h->next()->opposite()->next();  // Fig. (a)
  P.split_edge(h->next());
  P.split_edge(g->next());
  P.split_edge(g);  // Fig. (b)
  h->next()->vertex()->point() = Point3(xmax + eps, -eps, zmax + eps);
  g->next()->vertex()->point() = Point3(-eps, ymax + eps, zmax + eps);
  g->opposite()->vertex()->point() = Point3(xmax + eps, ymax + eps, -eps);  // Fig. (c)
  Halfedge_handle f = P.split_facet(g->next(),
                                    g->next()->next()->next());  // Fig. (d)
  Halfedge_handle e = P.split_edge(f);
  e->vertex()->point() = Point3(xmax + eps, ymax + eps, zmax + eps);  // Fig. (e)
  P.split_facet(e, f->next()->next());                                // Fig. (f)
  CGAL_postcondition(P.is_valid());
  CGAL::Polygon_mesh_processing::triangulate_faces(P);
  return h;
}

void ReadSTL(Polyhedron& P, string stl_path) {
  // Open STL file
  ifstream in(stl_path);
  if (in.fail()) {
    cerr << "Error! could not load STL file from" << stl_path << endl;
  }

  CGAL::Polyhedron_builder_from_STL<HalfedgeDS> stl_polyhedron(in);
  P.delegate(stl_polyhedron);
  auto vertices_count = P.size_of_vertices();
  if (!P.is_valid() || P.empty()) {
    cerr << "Error: Invalid polyhedron" << endl;
  }
}

Bbox PolyhedronBBOX(Polyhedron& P) {
  return CGAL::bounding_box(P.points_begin(), P.points_end()).bbox();
}

// Fill a given polygon with cubes of a specified size

void Translate(Polyhedron& P, vec3 translate) {
  const auto translation = Aff_transformation_3(
      CGAL::TRANSLATION, Vector(translate.x, translate.y, translate.z));
  transform(P.points_begin(), P.points_end(), P.points_begin(), translation);
}

void Scale(Polyhedron& P, double scale) {
  // TODO cgal only supports scaling with a scalar out of the box
  const auto scale_transform = Aff_transformation_3(CGAL::SCALING, scale);
  transform(P.points_begin(), P.points_end(), P.points_begin(), scale_transform);
}

void printBBOX(Bbox bbox, string name) {
  cout << name << " "
       << "xMin: " << bbox.xmin() << endl;
  cout << name << " "
       << "xMax: " << bbox.xmax() << endl;

  cout << name << " "
       << "yMin: " << bbox.ymin() << endl;
  cout << name << " "
       << "yMax: " << bbox.ymax() << endl;

  cout << name << " "
       << "zMin: " << bbox.zmin() << endl;
  cout << name << " "
       << "zMax: " << bbox.zmax() << endl;
}

void PrintPolyhedronDebug(Polyhedron& P) {
  CGAL::set_ascii_mode(std::cout);
  for (Vertex_iterator v = P.vertices_begin(); v != P.vertices_end(); ++v)
    cout << "  " << v->point() << endl;
}

shared_ptr<AABB_tree> InitializeAABB_Tree(Polyhedron& P) {
  auto tree = make_shared<AABB_tree>(faces(P).first, faces(P).second, P);
  tree->accelerate_distance_queries();
  return tree;
}

bool IsPointOutside(Point3 point, shared_ptr<AABB_tree> tree) {
  return !IsPointInside(point, tree);
}

Point3 ClosestPointOnSurface(Point3 query, shared_ptr<AABB_tree> tree) {
  return tree->closest_point(query);
}

double SquaredDistanceToSurface(Point3 point, shared_ptr<AABB_tree> tree) {
  return tree->squared_distance(point);
}

bool IsPointInside(Point3 point, shared_ptr<AABB_tree> tree) {
  auto inside_count = 0;
  auto outside_count = 0;
  // we take a vote since there are many cases
  // where the mass is on the exact boundary of the volume
  for (int i = 0; i < 10; i++) {
    auto direction = glm::normalize(vec3(glm::sphericalRand(1.0f)));
    Ray ray(point, Vector(direction.x, direction.y, direction.z));
    auto intersections = tree->number_of_intersected_primitives(ray);
    if (intersections == 1) {
      inside_count++;
    } else {
      outside_count++;
    }
  }
  return (inside_count > outside_count);
}
}  // namespace Geometry
