#define GLM_FORCE_PURE

#include <boost/timer/timer.hpp>
#include "ArgParser.h"
#include "CPUSpringMass.h"
#include "CSVWriter.h"
#include "DML/Config.h"
#include "Logger.h"
#include "Simulator.h"
#include "Telemetry.h"

#ifdef QT
#include "gui/MainWindow.h"
#endif

#include <chrono>

using namespace std;
using namespace boost::timer;

const int CURRENT_SCENARIO = SCENARIO::BEAM_TEST;

int run_hide_window() {
  const string dml_fp = ArgParser::dmlFilepath.Get();
  DML::Config config = DML::Parser::LoadDML(dml_fp);
  Simulator::instance().ConfigureFromDML(config);

  cpu_timer timer;

  Simulator::instance().spring_mass->enableUpdating();
  Simulator::instance().spring_mass->PerformUpdate();

  timer.stop();

  auto step_count =
      static_cast<double>(Simulator::instance().spring_mass->simulation_time /
                          Simulator::instance().spring_mass->time_step_ns);

  auto elapsed_secs = timer.elapsed().wall * 1e-9;
  auto avg_time_per_step = (elapsed_secs / step_count);
  auto masses_sec = Simulator::instance().masses->size() / avg_time_per_step;
  auto springs_sec = Simulator::instance().springs->size() / avg_time_per_step;

  cout << "Elapsed Seconds: " << elapsed_secs << endl;
  cout << "Avg Time Per Step: " << avg_time_per_step << endl;
  cout << "Masses per Second: " << masses_sec << endl;
  cout << "Springs per Second: " << springs_sec << endl;

  return 0;
}

int test_performance() {
  const string dml_fp = ArgParser::dmlFilepath.Get();
  DML::Config config = DML::Parser::LoadDML(dml_fp);
  Simulator::instance().ConfigureFromDML(config);

  cout << "Initializing performance tests..." << endl;

  Simulator::instance().spring_mass->enableUpdating();
  cpu_timer timer;
  Simulator::instance().spring_mass->PerformUpdate();
  timer.stop();

  auto step_count =
      static_cast<double>(Simulator::instance().spring_mass->simulation_time /
                          Simulator::instance().spring_mass->time_step_ns);

  auto elapsed_secs = timer.elapsed().wall * 1e-9;
  cout << "step_count: " << step_count << endl;
  auto avg_time_per_step = (elapsed_secs / step_count);
  auto masses_sec = Simulator::instance().masses->size() / avg_time_per_step;
  auto springs_sec = Simulator::instance().springs->size() / avg_time_per_step;

  cout << "Elapsed Seconds: " << elapsed_secs << endl;
  cout << "Avg Time Per Step: " << avg_time_per_step << endl;
  cout << "Masses per Second: " << masses_sec << endl;
  cout << "Springs per Second: " << springs_sec << endl;

  auto csv_path = Simulator::instance().output_dir;

  if (Simulator::instance().gpu_acceleration) {
    csv_path = csv_path / "telemetry" / "gpu_performance.csv";
  } else {
    csv_path = csv_path / "telemetry" / "cpu_performance.csv";
  }

  CSVWriter csv(",");
  csv.newRow() << "bars"
               << "masses"
               << "bars_per_second"
               << "masses_per_second";
  csv.newRow() << Simulator::instance().springs->size()
               << Simulator::instance().masses->size() << springs_sec << masses_sec;
  csv.writeToFile(csv_path.string());

  return 0;
}

int main(int argc, char** argv) {
  auto logger = Logger::getLogger("main");
  ArgParser::parse(argc, argv);
  Simulator::instance().hide_window = ArgParser::hideWindow.Get();
  Simulator::instance().gpu_device_id = ArgParser::gpuDeviceId.Get();
  if (ArgParser::testBeam.Get()) {
    Simulator::instance().scenario = SCENARIO::BEAM_TEST;
    cout << "Beam Test Scenario Enabled" << endl;
  }
  if (ArgParser::testPerformance.Get()) {
    Simulator::instance().scenario = SCENARIO::PERFORMANCE;
    Simulator::instance().hide_window = true;
    cout << "Performance Test Scenario Enabled" << endl;
  }
  try {
    if (ArgParser::testPerformance.Get()) {
      cout << "Running performance tests " << endl;
      test_performance();
    } else if (Simulator::instance().hide_window) {
      run_hide_window();
    } else {
#ifdef QT
      return CreateAndRunQTApp(argc, argv);
#endif
      throw std::invalid_argument("Must use --hide-window or compile with QT support");
    }
  } catch (std::exception& e) {
    logger->error(e.what());
    return 0;
  }
}
