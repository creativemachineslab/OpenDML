#include "Mass.h"
#include <algorithm>
#include <glm/integer.hpp>
#include <glm/vec3.hpp>
#include <iostream>
#include <memory>

using namespace std;

Mass::Mass(double massAmount, glm::dvec3 position) {
  this->mass = massAmount;
  this->position = position;
  this->originalPosition = position;
}

Mass::~Mass() {
  connected_springs.clear();
}

// keeping in mind that view (opengl) should be kept separate from the model
// this should likely live in a helper elsewhere.
Vertex Mass::GetVertex() {
  Vertex vert;
  vert.position = this->position;
  vert.normal = glm::vec3(1.0f, 1.0f, 1.0f);  // default for now
  vert.color = this->GetColor();
  return vert;
}

void Mass::RemoveSpring(std::shared_ptr<Spring> spring) {
  const auto position = find(connected_springs.begin(), connected_springs.end(), spring);
  if (position != connected_springs.end()) {
    connected_springs.erase(position);
  } else {
    std::cout << "connected spring not found" << std::endl;
    assert(false);
  }
  // connected_springs.erase(remove(connected_springs.begin(),
  // connected_springs.end(), spring), connected_springs.end());
}

// Used to update the connected tmp_springs
// when the position of a mass is changed
void Mass::UpdateSprings(double modulusElasticity, double springCrossSectionArea) {
  for (const auto& spring : connected_springs) {
    spring->restLength =
        glm::abs(glm::length(spring->mass1->position - spring->mass2->position));
    spring->stiffness = modulusElasticity * springCrossSectionArea / spring->restLength;
  }
}

glm::dvec3 Mass::GetColor() {
  if (glm::length(customColor) > 0) {
    return customColor;
  } else if (fixed) {
    return dvec3(1, 0, 0);
  } else if (UnderExternalForce()) {
    return dvec3(0, 1, 0);
  } else if (appliedAcceleration != dvec3(0, 0, 0)) {
    return dvec3(1, 1, 0);
  } else {
    return dvec3(1, 1, 1);
  }
  //} else if (traced) {
}
