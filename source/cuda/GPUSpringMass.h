#pragma once

#include "SpringMass.h"

#include <glm/vec3.hpp>
#include <memory>
#include <string>
#include <vector>

namespace cuda {

struct cuMass;

class GPUSpringMass : public SpringMass {
 public:
  virtual void Initialize(std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses,
                          std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs,
                          std::string integration_method,
                          double _time_step,
                          double _damping,
                          bool _is_rendering);
  virtual void Step();
  virtual void release();
  virtual void SetTracedMasses(std::vector<std::shared_ptr<Mass>> volume);
  virtual void PostUpdate();
  void UpdateTrace();

 private:
  void masses_to_cuMasses();
  void springs_to_cuSprings();
  void set_connected_springs();
  std::vector<glm::dvec3> prev_forces;
  unsigned long long measure_freq_from;
  unsigned long long perform_update_interval = 1;
};

// Device Code Utils
namespace Utils {
void masses_to_cuMasses(std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses);
void copy_data_to_device();
void copy_data_to_host();
void set_gpu_device(int device_id);
void set_gpu_integration_method(IntegrationMethods im);
std::vector<glm::dvec3> get_trace_results();
void release_forces();
void springs_to_cuSprings(std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses,
                          std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs);
void set_connected_springs();
void render_update(std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses);
void set_masses_to_trace(std::vector<int> masses_to_trace);
void device_update_trace();
};  // namespace Utils

class Euler : public IntegrationMethod {
 public:
  Euler(double time_step, double damping, GPUSpringMass* system)
      : IntegrationMethod(damping, time_step) {
    set_damping(damping), set_system(system);
  }

  void step() override { integrate(); }
  void initialize() override { integrate(); }
  void integrate();
  void set_damping(double damping) override { this->damping = damping; }
};

class Verlet : public IntegrationMethod {
 public:
  Verlet(double time_step, double damping, GPUSpringMass* system)
      : IntegrationMethod(damping, time_step) {
    set_damping(damping), set_system(system);
  }

  void step() override { integrate(); }
  void initialize();
  void integrate();
  void set_damping(double damping) override { this->damping = damping; }
};

struct RK4_State {
  glm::dvec3 position = glm::dvec3(0, 0, 0);
  glm::dvec3 velocity = glm::dvec3(0, 0, 0);
};

struct RK4_States {
  RK4_State a;
  RK4_State b;
  RK4_State c;
  RK4_State d;
  RK4_State initials;
  cuMass* mass;
};

class RK4 : public IntegrationMethod {
 public:
  RK4(double time_step, double damping, GPUSpringMass* system)
      : IntegrationMethod(damping, time_step) {
    set_damping(damping), set_system(system);
  }

  void step() override { integrate(); }
  void initialize();
  void integrate();

  // Damping not implemented yet for RK4
  void set_damping(double damping) override { this->damping = damping; }

  RK4_State state;
};

}  // namespace cuda
