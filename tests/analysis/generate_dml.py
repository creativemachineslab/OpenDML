"""
Generate sample dml files that change the length of a beam, but maintain
constant density and scale the load proportionally over same area as well
(but keep same total amount of force)
"""
from lxml import etree
import numpy as np
import pprint
import uuid
import argparse
from os.path import join, dirname, exists
import os

pp = pprint.PrettyPrinter(indent=4)
integration_methods = ["euler", "verlet", "RK4"]
# integration_methods = ["euler"]

parser = argparse.ArgumentParser(description='Generates DML files for performing cantilever beam tests')
parser.add_argument('--all', help="Create DML files for varying_x, varying_y, varying_z, and performance", action="store_true")
parser.add_argument('--name', help="Name for the ouptut scenario")
parser.add_argument('--varying_z', help="Create DML files for only varying_z", action="store_true")
parser.add_argument('--performance', help="Create DML files for only performance", action="store_true")
parser.add_argument('--energy_test', help="Create DML files performing energy test", action="store_true")
parser.add_argument('--compare_integrations', help="Create DML files with Euler, RK4 and Verlet integration methods for comparison", action="store_true")
parser.add_argument('--varying_x', help="Create DML files for only varying_x", action="store_true")
parser.add_argument('--varying_y', help="Create DML files for only varying_y", action="store_true")
parser.add_argument('--use_gpu', help="Enable GPU acceleration in simulation", action="store_true")
parser.add_argument('--random_lattice', help="Use random fill method to generate lattice", action="store_true")

# generate a random suffix so we can have a unique output directory for each run.

args = parser.parse_args()

simulation_time_ms = 500

scenario_name = ""
output_path = ""

def setup_beam(val, params, test_info):
    params['simulation_time'] = simulation_time_ms
    params['output_dir'] = os.path.join(output_path, test_info['name'], test_info['integration_method'], '%s_%s' % (str(round(val, 3)), str(params['copy_index'])))
    params['integration_method'] = test_info['integration_method']

    padding = 0.001
    # Resize loadpoint, anchor and trace to fit tip
    params['loadpoint_scale'][0] = params['cube_xyz'][0]
    params['loadpoint_scale'][1] = params['cantilever_xyz'][1] + padding
    params['loadpoint_scale'][2] = params['cantilever_xyz'][2] + padding

    # Apply a small constant force for loading condition
    params['loadcase_constant_force'][0] = 0
    # params['loadcase_constant_force'][1] = 0.002
    params['loadcase_constant_force'][1] = 1.0
    params['loadcase_constant_force'][2] = 0

    params['anchor_scale'][0] = params['cube_xyz'][0]
    params['anchor_scale'][1] = params['cantilever_xyz'][1] + padding
    params['anchor_scale'][2] = params['cantilever_xyz'][2] + padding

    params['trace_scale'][0] = params['cube_xyz'][0]
    params['trace_scale'][1] = params['cantilever_xyz'][1] + padding
    params['trace_scale'][2] = params['cantilever_xyz'][2] + padding

    # Translate loadpoint to tip of beam
    params['loadpoint_translate'][0] = \
        params['cantilever_xyz'][0] - params['cube_xyz'][0] / 2
    
    # Translate trace to tip of beam
    params['trace_translate'][0] = \
        params['cantilever_xyz'][0] - params['cube_xyz'][0] / 2
    
    # Translate anchor to fix beam at "wall"
    params['anchor_translate'][0] = \
        0.0 - params['cube_xyz'][0] / 2
    
    # Center up the bounding boxes
    params['loadpoint_translate'][1] = -padding/2
    params['loadpoint_translate'][2] = -padding/2

    params['anchor_translate'][1] = -padding/2
    params['anchor_translate'][2] = -padding/2

    params['trace_translate'][1] = -padding/2
    params['trace_translate'][2] = -padding/2
    params['time_step'] = 1e-6

    if args.use_gpu:
        params['use_gpu'] = "true"
    if args.random_lattice:
        params['random_lattice'] = "true"
    else:
        params['random_lattice'] = "false"

def compare_integrations():
    integration_methods = ["euler", "verlet", "RK4"]
    # var_values = np.arange(0.1, 0.4001, 0.05)
    var_values = np.array([1e-06*0.25, 1e-06*0.5, 1e-06, 1e-06/0.25, 1e-06/0.5,])
    # var_values = np.array([0.1, 0.2, 0.25, 0.3, 0.35, 0.4])
    print("Creating the following sizes for compare integrations: ")
    pp.pprint(var_values)
    print("With the following integration methods: ")
    pp.pprint(integration_methods)
    copy_count = 1


    def func_y(val, params):
        params['cantilever_xyz'][0] = 0.2  # keep x constant
        params['cantilever_xyz'][1] = 0.02 # keey y constant
        params['cantilever_xyz'][2] = 0.02  # keep z constant
        setup_beam(val, params, test_info)
        params['output_dir'] = os.path.join(output_path, test_info['name'], test_info['integration_method'], '%s' % "{:.1e}".format(val))
        params['damping_amount'] = 0.0
        params['simulation_time'] = 40
        params['time_step'] = val

    for m in integration_methods:
        test_info = dict(name='compare_integrations',
                        integration_method=m)
        generate_dml(var_values, test_info, func_y, copy_count)

def energy_test():
    """
    A simple energy test scenarion, run on a cantilever beam with Verlet.
    CPU is chosen as backend as the GPU does not yet implement energy telemetry.
    """
    var_values = np.array([0.1])
    copy_count = 1

    def func_y(val, params):
        params['cantilever_xyz'][0] = val  
        params['cantilever_xyz'][1] = 0.02 # keep y constant
        params['cantilever_xyz'][2] = 0.02  # keep z constant

        setup_beam(val, params, test_info)

    test_info = dict(name='varying_x',
                    integration_method='euler')
    generate_dml(var_values, test_info, func_y, copy_count)



def performance():
    # var_values = np.arange(0.1, 0.4001, 0.05)
    var_values = np.arange(0.2, 8.001, 0.20)
    # vals_detailed = [1.25, 1.30, 1.40, 1.45, 1.5, 1.55, 1.60]
    # vals_regular = [0.2, 0.4, 0.8, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
    # var_values = np.array(vals_regular + vals_detailed)
    # var_values = np.array([0.1, 0.2, 0.25, 0.3, 0.35, 0.4])
    print("Creating the following sizes for performance: ")
    pp.pprint(var_values)
    copy_count = 1

    def func_y(val, params):
        params['cantilever_xyz'][0] = val  # keep x constant
        params['cantilever_xyz'][1] = 0.02 # keey y constant
        params['cantilever_xyz'][2] = 0.02  # keep z constant

        setup_beam(val, params, test_info)
        params['simulation_time'] = 100

    for m in integration_methods:
        test_info = dict(name='performance',
                        integration_method=m)
        generate_dml(var_values, test_info, func_y, copy_count)

def varying_x():
    var_values = np.arange(0.1, 0.4001, 0.05)
    # var_values = np.array([0.1, 0.2, 0.25, 0.3, 0.35, 0.4])
    print("Creating the following sizes for varying_x: ")
    pp.pprint(var_values)
    copy_count = 1

    def func_y(val, params):
        params['cantilever_xyz'][0] = val  # keep x constant
        params['cantilever_xyz'][1] = 0.02 # keey y constant
        params['cantilever_xyz'][2] = 0.02  # keep z constant

        setup_beam(val, params, test_info)

    for m in integration_methods:
        test_info = dict(name='varying_x',
                        integration_method=m)
        generate_dml(var_values, test_info, func_y, copy_count)

def varying_y():
    var_values = np.arange(0.02, 0.201, 0.02) 
    print("Creating the following sizes for varying_y: ")
    pp.pprint(var_values)
    copy_count = 1

    def func_y(val, params):
        params['cantilever_xyz'][0] = 0.2  # keep x constant
        params['cantilever_xyz'][1] = val
        params['cantilever_xyz'][2] = 0.02  # keep z constant

        setup_beam(val, params, test_info)

    for m in integration_methods:
        test_info = dict(name='varying_y',
                        integration_method=m)
        generate_dml(var_values, test_info, func_y, copy_count)


def varying_z():
    var_values = np.arange(0.02, 0.201, 0.02)
    print("Creating the following sizes for varying_z: ")
    pp.pprint(var_values)
    copy_count = 1

    def func_z(val, params):
        params['cantilever_xyz'][0] = 0.2  # keep x constant
        params['cantilever_xyz'][1] = 0.02 # keep y constant
        params['cantilever_xyz'][2] = val  # keep z constant

        setup_beam(val, params, test_info)

    for m in integration_methods:
        test_info = dict(name='varying_z',
                        integration_method=m)
        generate_dml(var_values, test_info, func_z, copy_count)

def generate_dml(var_values, test_info, modify_params_func, copies=1):
    fp_in = join(dirname(__file__), 'generate_dml_template.dml')
    dir_name = join(dirname(__file__), 'scenarios', scenario_name, 'dml_files', test_info['name'])
    if copies > 1:
        fp_templ = join(dir_name, '%s' % test_info['integration_method'], '%s_%s.dml')
    else:
        fp_templ = join(dir_name, '%s' % test_info['integration_method'], '%s.dml')

    if not exists(dirname(fp_templ)):
        os.makedirs(dirname(fp_templ))
    for c_index in range(1, copies+1):
        for tval in var_values:
            xml = etree.parse(fp_in)
            modify_xml(xml, tval, modify_params_func)
            if copies > 1:
                modify_xml(xml, tval, modify_params_func, c_index)
                xml.write(fp_templ %
                          (round(tval, 3), c_index), pretty_print=True)
            else:
                modify_xml(xml, tval, modify_params_func)
                xml.write(fp_templ % "{:.1e}".format(tval), pretty_print=True)
                # xml.write(fp_templ % round(tval, 3), pretty_print=True)

def modify_xml(xml, tval, modify_params_func, copy_index=1):
    cantilever = xml.xpath('/dml/volume[@id="cantilever"]')[0]
    loadpoint = xml.xpath('/dml/volume[@id="loadpoint"]')[0]
    loadcase_force = xml.xpath('/dml/volume[@id="loadpoint"]')[0]
    trace = xml.xpath('/dml/volume[@id="trace"]')[0]
    loadcase_force = xml.xpath('/dml/loadcase/constant')[0]
    anchor = xml.xpath('/dml/volume[@id="anchor"]')[0]
    lattice = xml.xpath('/dml/material/lattice')[0]
    modulus = xml.xpath('/dml/material/elasticity')[0]
    stop = xml.xpath('/dml/rules/stop')[0]
    damping = xml.xpath('/dml/rules/damping')[0]
    telemetry = xml.xpath('/dml/rules/telemetry')[0]
    rules = xml.xpath('/dml/rules')[0]
    integration = xml.xpath('/dml/rules/integration')[0]

    # get default values and then modify them
    params = dict(
        anchor_scale=vec3(anchor.attrib['scale']),
        anchor_translate=vec3(anchor.attrib['translate']),
        cantilever_xyz=vec3(cantilever.attrib['scale']),
        loadpoint_translate=vec3(loadpoint.attrib['translate']),
        loadpoint_scale=vec3(loadpoint.attrib['scale']),
        loadcase_constant_force=vec3(loadpoint.attrib['scale']),
        trace_scale=vec3(trace.attrib['scale']),
        trace_translate=vec3(trace.attrib['translate']),
        cube_xyz=vec3(lattice.attrib['size']),
        random_lattice=str(lattice.attrib['random']),
        modulus_elasticity=float(modulus.attrib['modulus']),
        vertexmass=float(lattice.attrib['vertexmass']),
        simulation_time=int(stop.attrib['simulation_time']),
        damping_amount=float(damping.attrib['amount']),
        output_dir=str(telemetry.attrib['output_dir']),
        integration_method=str(integration.attrib['method']),
        use_gpu=str(rules.attrib['gpu_acceleration']),
        time_step=str(integration.attrib['time_step']),
        copy_index=int(copy_index)
    )
    modify_params_func(tval, params)

    cantilever.attrib['scale'] = '%s, %s, %s' % tuple(params['cantilever_xyz'])
    loadpoint.attrib['translate'] = \
        '%s, %s, %s' % tuple(params['loadpoint_translate'])
    loadpoint.attrib['scale'] = \
        '%s, %s, %s' % tuple(params['loadpoint_scale'])
    loadcase_force.attrib['magnitude'] = \
        '%s, %s, %s' % tuple(params['loadcase_constant_force'])
    trace.attrib['scale'] = \
        '%s, %s, %s' % tuple(params['trace_scale'])
    trace.attrib['translate'] = \
        '%s, %s, %s' % tuple(params['trace_translate'])
    anchor.attrib['scale'] = '%s, %s, %s' % tuple(params['anchor_scale'])
    anchor.attrib['translate'] = \
        '%s, %s, %s' % tuple(params['anchor_translate'])
    lattice.attrib['size'] = '%s, %s, %s' % tuple(params['cube_xyz'])
    lattice.attrib['vertexmass'] = '%s' % params['vertexmass']
    lattice.attrib['random'] = '%s' % params['random_lattice']
    modulus.attrib['modulus'] = '%s' % params['modulus_elasticity']
    stop.attrib['simulation_time'] = '%s' % params['simulation_time']
    damping.attrib['amount'] = '%s' % params['damping_amount']
    telemetry.attrib['output_dir'] = '%s' % params['output_dir']
    rules.attrib['gpu_acceleration'] = '%s' % params['use_gpu']
    integration.attrib['method'] = '%s' % params['integration_method']
    integration.attrib['time_step'] = '%s' % params['time_step']


def vec3(string):
    """ string in form "1,1,1" """
    return np.array([float(x) for x in string.split(',')])


def main():
    global scenario_name
    global output_path

    if args.name:
        scenario_name = args.name
    else:
        scenario_name = str(uuid.uuid4()).split("-")[0]

    output_path = join('scenarios', scenario_name, 'output')

    if args.all:
        varying_y()
        varying_x()
        varying_z()
        # performance()
    elif args.compare_integrations:
        compare_integrations()
    elif args.performance:
        performance()
    elif args.varying_x:
        varying_x()
    elif args.varying_y:
        varying_y()
    elif args.varying_z:
        varying_z()
    elif args.energy_test:
        energy_test()
    else:
        performance()
        varying_y()
        varying_x()
        varying_z()

    print(scenario_name)
if __name__ == '__main__':
    main()