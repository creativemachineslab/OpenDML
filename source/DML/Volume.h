#pragma once

#include <iostream>
#include <vector>
#include "glm/vec3.hpp"
#include "pugixml.hpp"

namespace DML {
class Volume {
 public:
  Volume();
  ~Volume();
  bool FromXML(pugi::xml_object_range<pugi::xml_attribute_iterator>);
  std::vector<std::string> validPrimitives{"cube", "sphere"};

  bool setScale(glm::vec3 scale);
  bool setColor(glm::vec3 color);
  bool setTranslate(glm::vec3 translate);
  bool setRotate(glm::vec3 rotate);
  bool setID(std::string id);
  bool setPrimitive(std::string primitive);
  bool setSTLPath(std::string stl_path);
  bool setMoveTo(std::string move_to);

  glm::vec3 Translate();
  glm::vec3 Scale();
  glm::vec3 Rotate();
  glm::vec3 Color();
  std::string Primitive();
  std::string STLPath();
  std::string MoveTo();

  std::string GetID();

 private:
  std::string id;
  std::string primitive;
  std::string stl_path;
  std::string move_to = "none";
  glm::vec3 scale;
  glm::vec3 translate;
  glm::vec3 color;
  glm::vec3 rotate;
  glm::vec3 _parseVector(std::string str, size_t expectedLength);
  bool _checkPrimitive(std::string primitive);
};
}  // namespace DML
