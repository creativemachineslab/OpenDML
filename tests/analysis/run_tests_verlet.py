import glob
import subprocess
import sys, os
import argparse

parser = argparse.ArgumentParser(description='The name of the folder containing the tests to run.')
parser.add_argument('folders', metavar='folders', nargs='+', type=str, help='The folder containing the tests to run')
args = parser.parse_args()

opendml_exe = "C:/Users/joel/CMakeBuilds/OpenDML/build/x64-Release/opendml.exe"

this_dir, filename = os.path.split(sys.argv[0]) # directory our script is in
for folder in args.folders:
    print("Processing %s" % folder)
    dml_files = glob.glob(os.path.join(this_dir, folder, '**/*/*.dml'), recursive=True)
    dml_files = [os.path.relpath(f, this_dir) for f in dml_files]
    print(dml_files)
    for d in dml_files:
        subprocess.run([opendml_exe, d, "--hide-window"], cwd=this_dir) 