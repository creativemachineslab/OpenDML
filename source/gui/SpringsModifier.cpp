#include "SpringsModifier.h"
#include <Qt3DCore/qtransform.h>
#include <Qt3DExtras/qpervertexcolormaterial.h>
#include "LinesGeometry.h"

SpringModifier::SpringModifier(Qt3DCore::QEntity* rootEntity, Simulator* sim)
    : root_entity(rootEntity), sim(sim) {
  springs_root = new Qt3DCore::QEntity(rootEntity);

  auto spring_entity = new Qt3DCore::QEntity(springs_root);
  auto line = new Lines();
  auto line_transform = new Qt3DCore::QTransform();
  auto line_material = new Qt3DExtras::QPerVertexColorMaterial();

  spring_entity->addComponent(line);
  spring_entity->addComponent(line_transform);
  spring_entity->addComponent(line_material);
}

SpringModifier::~SpringModifier() {}

void SpringModifier::SyncSprings() {
  updateSpringBuffer();

  auto spring_entities = springs_root->findChildren<Lines*>();

  for (auto se : spring_entities) {
    se->setVertices(spring_buffer);
  }
}

void SpringModifier::setEnabled(bool enabled) {
  springs_root->setEnabled(enabled);
}

bool SpringModifier::isEnabled() {
  return springs_root->isEnabled();
}

void SpringModifier::updateSpringBuffer() {
  if (!springs_mutex().try_lock()) {
    return;
  }
  if (sim->springs->size() != spring_buffer.size() * 2) {
    spring_buffer.resize(2 * sim->springs->size());
  }

  for (auto s : *sim->springs) {
    if (s->maxForce >= sim->max_spring_force) {
      sim->max_spring_force = s->maxForce;
    }
  }

  int buffer_idx = 0;
  for (auto s : *sim->springs) {
    auto m1 = s->mass1;
    auto m2 = s->mass2;
    Vertex vert1;
    Vertex vert2;

    float intensity = 0.0;
    // if (sim->max_spring_force > 0.0) {
    //   intensity = s->maxForce / sim->max_spring_force;
    // }

    // intensity = glm::pow(2, -intensity);

    vert1.position = m1->position;
    vert1.normal = vec3(1.0, 1.0, 1.0);
    vert1.color = intensity * vec3(1.0, 1.0, 1.0f);

    vert2.position = m2->position;
    vert2.normal = vec3(1.0, 1.0, 1.0);
    vert2.color = intensity * vec3(1.0, 1.0, 1.0f);

    spring_buffer[buffer_idx++] = vert1;
    spring_buffer[buffer_idx++] = vert2;
  }
  springs_mutex().unlock();
}
