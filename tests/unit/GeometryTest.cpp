#include <algorithm>
#include "gtest/gtest.h"
#include "Geometry.h"

using namespace std;

TEST(geometry, intersection_test) {
  Polyhedron poly;
  // create a unit cube
  Geometry::MakeCube(poly, 1.0, 1.0, 1.0);
  auto aabb_tree = make_shared<AABB_tree>(faces(poly).first, faces(poly).second, poly);
  auto inside_point = Point3(0.5, 0.5, 0.5);
  auto inside_test = Geometry::IsPointInside(inside_point, aabb_tree);
	EXPECT_EQ(inside_test, true);

  auto outside_point = Point3(10, 10, 10);
  auto outside_test = Geometry::IsPointInside(outside_point, aabb_tree);
	EXPECT_EQ(outside_test, false);

  auto border_point = Point3(0, 1, 0.5);
  auto border_test = Geometry::IsPointInside(border_point, aabb_tree);
	EXPECT_EQ(border_test, true);
}

TEST(geometry, make_cube) {
  Polyhedron poly;
  // create a unit cube
  Geometry::MakeCube(poly, 1.0, 1.0, 1.0);
  auto vertex_count = CGAL::vertices(poly).size();
  EXPECT_EQ(vertex_count, 8);

  CGAL::Bbox_3 bbox = CGAL::bbox_3(poly.points_begin(), poly.points_end());
  auto xmax = bbox.xmax();
  // beware double comparison errors, however we want to
  // observe that the created cube is *slightly* larger 
  // than 1 due to the fuzzing we're applying. See Geometry.cpp
  ASSERT_GT(bbox.xmax(), 1.0);
  ASSERT_LT(bbox.xmin(), 0.0);
}

TEST(geometry, translate) {
  Polyhedron poly;
  // create a unit cube
  Geometry::MakeCube(poly, 1.0, 1.0, 1.0);
  auto x_shift = 1.0;
  auto y_shift = 2.0;
  auto z_shift = 3.0;

  CGAL::Bbox_3 before_bbox = CGAL::bbox_3(poly.points_begin(), poly.points_end());
  Geometry::Translate(poly, vec3(x_shift, y_shift, z_shift));
  CGAL::Bbox_3 after_bbox = CGAL::bbox_3(poly.points_begin(), poly.points_end());

  auto x_diff = after_bbox.xmax() - before_bbox.xmax();
  auto y_diff = after_bbox.ymax() - before_bbox.ymax();
  auto z_diff = after_bbox.zmax() - before_bbox.zmax();

  ASSERT_DOUBLE_EQ(x_diff, 1.0);
  ASSERT_DOUBLE_EQ(y_diff, 2.0);
  ASSERT_DOUBLE_EQ(z_diff, 3.0);
}