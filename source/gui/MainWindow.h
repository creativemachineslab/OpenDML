#pragma once

#include "DML/Config.h"
#include "Simulator.h"

#include <Qt3DCore/qentity.h>
#include <Qt3DLogic/qframeaction.h>
#include <Qt3DRender/qcamera.h>
#include <Qt3DRender/qtechnique.h>
#include <Qt3DRender/qtexture.h>
#include <QtWidgets/qplaintextedit.h>
#include <QtGui/QScreen>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QWidget>
#include "LiveLineChart.h"
#include "RenderCapture.h"
#include "SceneModifier.h"
#include "Utils.h"

int CreateAndRunQTApp(int argc, char** argv);

class MainWindow : public QWidget {
  Q_OBJECT
 public:
  MainWindow();
  ~MainWindow();

  void ShowSimulatorOutput();
  void PrintDebugInfo();

  SceneModifier* modifier;
  Qt3DLogic::QFrameAction* frame_action;
  Qt3DCore::QEntity* rootEntity;
  Qt3DRender::QCamera* camera;
  RenderCapture* render_capture;

  // Charts
  LiveLineChart* energy_chart;
  QTimer* chart_update_timer;
  QTimer* render_capture_timer;
  QTimer* chart_capture_timer;

  QMetaObject::Connection telemetry_conn;

  // UI
  QPushButton* toggle_sim;
  QPushButton* reset_camera;
  QCheckBox* toggle_masses;
  QCheckBox* toggle_force_vectors;
  QCheckBox* toggle_bbox;
  QCheckBox* toggle_springs;
  QCheckBox* toggle_capture_charts;
  QCheckBox* toggle_capture_render;
  QPlainTextEdit* text_stats;

  int capture_count = 0;
  // State
  bool simulator_running = false;

  void SimulateUntil(unsigned long long timeout_simulation_ns,
                     unsigned long long timeout_wall_clock_ns);
 public slots:
  void ToggleSimulator();
  void ToggleCaptureCharts(bool enabled);
  void ToggleCaptureRender(bool enabled);
  void ResetAndCenterCamera();
  void UpdateStats();

 private:
};
