#pragma once

#include <CGAL/AABB_face_graph_triangle_primitive.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/Aff_transformation_3.h>
#include <CGAL/Bbox_3.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Side_of_triangle_mesh.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/aff_transformation_tags.h>
#include <CGAL/array.h>
#include <CGAL/bounding_box.h>
#include <CGAL/centroid.h>
#include <algorithm>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <iostream>
#include <limits>
#include <numeric>

typedef CGAL::Simple_cartesian<double> K;
typedef CGAL::Bbox_3 Bbox;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef CGAL::AABB_face_graph_triangle_primitive<Polyhedron> Primitive;
typedef CGAL::AABB_traits<K, Primitive> Traits;
typedef CGAL::AABB_tree<Traits> AABB_tree;
typedef CGAL::Aff_transformation_3<K> Aff_transformation_3;
typedef K::FT FT;
typedef K::Point_3 Point3;
typedef K::Vector_3 Vector;
typedef K::Ray_3 Ray;
typedef K::Segment_3 Segment;
typedef AABB_tree::Point_and_primitive_id Point_and_primitive_id;
typedef Polyhedron::HalfedgeDS HalfedgeDS;
typedef Polyhedron::Halfedge_handle Halfedge_handle;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef Polyhedron::Point_iterator Point_iterator;

using glm::dvec3;
using glm::vec3;

namespace Geometry {
Halfedge_handle MakeCube(Polyhedron& P, float xmax, float ymax, float zmax);
void ReadSTL(Polyhedron& P, std::string stl_path);
void PrintPolyhedronDebug(Polyhedron& P);

Bbox PolyhedronBBOX(Polyhedron& P);
void printBBOX(Bbox bbox, std::string name);
std::vector<Point3> FillPolyhedronWithCubes(Polyhedron& P,
                                            std::shared_ptr<AABB_tree> tree,
                                            double cube_x,
                                            double cube_y,
                                            double cube_z);
void Translate(Polyhedron& P, vec3 translate);
void Scale(Polyhedron& P, double scale);
double SquaredDistanceToSurface(Point3 point, std::shared_ptr<AABB_tree> tree);
Point3 ClosestPointOnSurface(Point3 query, std::shared_ptr<AABB_tree> tree);
bool IsPointInside(Point3 point, std::shared_ptr<AABB_tree> tree);
bool IsPointOutside(Point3 point, std::shared_ptr<AABB_tree> tree);
std::shared_ptr<AABB_tree> InitializeAABB_Tree(Polyhedron& P);
}  // namespace Geometry
