#ifdef USE_INTEL_PSTL
#include <pstl/algorithm>
#include <pstl/execution>
#include <pstl/numeric>

#define std_par std
#else
// Use the standard library implementation
#include <algorithm>
#include <execution>
#include <numeric>

#define std_par std
#endif

#include "CPUSpringMass.h"

#include "Simulator.h"

#include <boost/math/common_factor_rt.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <map>
#include <memory>
#include <numeric>
#include <taskflow/taskflow.hpp>
#include "Analysis.h"
#include "Timer.h"

// This can probably be removed...
#include <glm/ext.hpp>

#include <boost/timer/timer.hpp>
using namespace boost::timer;
#include "Logger.h"
#include "Telemetry.h"

using namespace std;
using namespace std_par;

struct RK4_State {
  glm::dvec3 position = glm::dvec3(0, 0, 0);
  glm::dvec3 velocity = glm::dvec3(0, 0, 0);
};

struct RK4_States {
  RK4_State a;
  RK4_State b;
  RK4_State c;
  RK4_State d;
  RK4_State initials;
  shared_ptr<Mass> mass;
};

// Implement Rk4 Method
class RK4 : public IntegrationMethod {
 public:
  RK4(double time_step, double damping, CPUSpringMass* system)
      : time_step(time_step), damping(damping) {
    set_system(system);
    masses = Simulator::instance().masses;
    rk4_states.resize(masses->size());
  }

  // jclay: still need to refactor this to reuse the graph we create.
  // as I see it (yet to be validated), the big advantage of using taskflow
  // is building the task graph once, keeping it in memory and then simply
  // re-dispatching on every timstep. if we can keep the graph in memory and
  // re-dispatch tasks, we should see some nice improvements here.
  void step() override {
    tf::Taskflow tf(std::thread::hardware_concurrency());
    auto exec = execution::par_unseq;
    vector<pair<tf::Task, tf::Task>> tasks;

    // Initialize state
    std_par::transform(exec,
                       masses->begin(),
                       masses->end(),
                       rk4_states.begin(),
                       rk4_states.begin(),
                       [](auto m, RK4_States state) -> RK4_States {
                         state.initials.position = m->position;
                         state.initials.velocity = m->currentVelocity;
                         state.mass = m;
                         return state;
                       });

    auto computeMassForces = [&, this]() {
      return tf.parallel_for(
          rk4_states.begin(), rk4_states.end(), [&](RK4_States& state) {
            this->system->ComputeMassForces(state.mass);
          });
    };

    tasks.push_back(computeMassForces());

    auto step_A =
        tf.parallel_for(rk4_states.begin(), rk4_states.end(), [&](RK4_States& state) {
          auto mass = state.mass;
          const auto initial = state.initials;
          state.a = evaluate(state.mass);

          mass->position = initial.position + state.a.position * time_step * 0.5;
          mass->currentVelocity = initial.velocity + state.a.velocity * time_step * 0.5;
        });

    tasks.push_back(step_A);
    tasks.push_back(computeMassForces());

    auto step_B =
        tf.parallel_for(rk4_states.begin(), rk4_states.end(), [&](RK4_States& state) {
          auto mass = state.mass;
          const auto initial = state.initials;

          state.b = evaluate(state.mass);

          mass->position = initial.position + state.b.position * time_step * 0.5;
          mass->currentVelocity = initial.velocity + state.b.velocity * time_step * 0.5;
        });

    tasks.push_back(step_B);
    tasks.push_back(computeMassForces());

    auto step_C =
        tf.parallel_for(rk4_states.begin(), rk4_states.end(), [&](RK4_States& state) {
          auto mass = state.mass;
          const auto initial = state.initials;

          state.c = evaluate(state.mass);

          mass->position = initial.position + state.c.position * time_step;
          mass->currentVelocity = initial.velocity + state.c.velocity * time_step;
        });

    tasks.push_back(step_C);
    tasks.push_back(computeMassForces());

    auto step_D =
        tf.parallel_for(rk4_states.begin(), rk4_states.end(), [&](RK4_States& state) {
          state.d = evaluate(state.mass);
        });

    tasks.push_back(step_D);

    auto step_Final =
        tf.parallel_for(rk4_states.begin(), rk4_states.end(), [&](RK4_States& state) {
          auto mass = state.mass;
          const auto initials = state.initials;

          const auto a = state.a;
          const auto b = state.b;
          const auto c = state.c;
          const auto d = state.d;

          const glm::dvec3 dxdt =
              1.0 / 6.0 * (a.position + 2.0 * (b.position + c.position) + d.position);
          const glm::dvec3 dvdt =
              1.0 / 6.0 * (a.velocity + 2.0 * (b.velocity + c.velocity) + d.velocity);

          mass->position = initials.position + dxdt * time_step;
          mass->currentVelocity = initials.velocity + dvdt * time_step;
        });

    tasks.push_back(step_Final);

    vector<tf::Task> lin_tasks;

    for (const auto& task : tasks) {
      lin_tasks.push_back(task.first);
      lin_tasks.push_back(task.second);
    }

    tf.linearize(lin_tasks);
    tf.wait_for_all();
  }

  RK4_State evaluate(const shared_ptr<Mass>& mass) {
    RK4_State out;

    out.position = mass->currentVelocity;
    const auto acc = mass->force / mass->mass;
    out.velocity += acc;
    mass->force = glm::dvec3(0, 0, 0);

    return out;
  }

  // Damping not implemented yet for RK4
  void set_damping(double damping) override { this->damping = damping; }

  vector<RK4_States> rk4_states;
  RK4_State state;
  shared_ptr<vector<shared_ptr<Mass>>> masses;
  double damping;
  double time_step;

 private:
};

class Euler : public IntegrationMethod {
 public:
  Euler(double time_step, double damping, CPUSpringMass* system)
      : time_step(time_step), damping(damping) {
    set_system(system);
  }

  void step() override {
    // Calculate forces for every mass.
    // The force is updated on the corresponding mass object.
    for_each(execution::par_unseq,
             Simulator::instance().masses->begin(),
             Simulator::instance().masses->end(),
             [this](auto m) { this->system->ComputeMassForces(m); });

    // Take the forces and integrate them into velocity and then position.
    for_each(execution::par_unseq,
             Simulator::instance().masses->begin(),
             Simulator::instance().masses->end(),
             [this](auto m) { this->integrate(m); });
  }

  void initialize(const shared_ptr<Mass>& mass) override { integrate(mass); }

  void integrate(const shared_ptr<Mass>& mass) override {
    if (mass->fixed)
      return;

    mass->lastPosition = mass->position;

    const auto acc = mass->force / mass->mass;
    mass->currentVelocity = damping * (mass->currentVelocity + acc * time_step);
    // temporarily constrain the velocity on z -axis
    /* mass->currentVelocity.z = 0; */
    mass->position += mass->currentVelocity * time_step;
    mass->force = glm::dvec3(0, 0, 0);
  }

  void set_damping(double damping) override { this->damping = damping; }

 private:
  double damping;
  double time_step;
};

class Verlet : public IntegrationMethod {
 public:
  Verlet(double time_step, double damping, CPUSpringMass* system)
      : time_step(time_step), damping(damping), time_step_sq(time_step * time_step) {
    set_system(system);
  };

  void step() override {
    // Calculate forces for every mass.
    // The force is updated on the corresponding mass object.
    for_each(execution::par_unseq,
             Simulator::instance().masses->begin(),
             Simulator::instance().masses->end(),
             [this](auto m) { this->system->ComputeMassForces(m); });

    // Verlet needs to initialize for the first  step
    if (!initialized) {
      // Take the forces and integrate them into velocity and then position.
      for_each(execution::par_unseq,
               Simulator::instance().masses->begin(),
               Simulator::instance().masses->end(),
               [this](auto m) { this->initialize(m); });

      initialized = true;
    } else {
      // Take the forces and integrate them into velocity and then position.
      for_each(execution::par_unseq,
               Simulator::instance().masses->begin(),
               Simulator::instance().masses->end(),
               [this](auto m) { this->integrate(m); });
    }
  }

  void initialize(const shared_ptr<Mass>& mass) override {
    if (mass->fixed)
      return;

    const auto acc = mass->force / mass->mass;
    const auto new_position = mass->position + 0.5 * acc * time_step_sq;
    mass->lastPosition = mass->position;
    mass->position = new_position;
    mass->force = glm::dvec3(0, 0, 0);
  }

  void integrate(const shared_ptr<Mass>& mass) override {
    if (mass->fixed)
      return;

    const auto acc = mass->force / mass->mass;
    // this implementation of damping is likely not physically accurate
    // see
    // https://www.gamedev.net/forums/topic/675751-verlet-integration-and-dampening/
    const auto new_position = mass->position +
                              (mass->position - mass->lastPosition) * damping +
                              acc * time_step_sq;
    mass->lastPosition = mass->position;
    mass->position = new_position;
    mass->force = glm::dvec3(0, 0, 0);
  }

  void set_damping(double damping) override { this->damping = damping; }

 private:
  bool initialized = false;
  double damping;
  double time_step;
  double time_step_sq;
};

// Release any external forces
void CPUSpringMass::release() {
  for (auto m : *masses) {
    m->externalForce = glm::dvec3(0, 0, 0);
  }
}

void CPUSpringMass::ComputeMassForces(const shared_ptr<Mass>& mass) {
  if (mass->fixed)
    return;

  mass->force += mass->appliedAcceleration * (mass->mass);
  mass->force += mass->externalForce;
  mass->force += ConnectedSpringForces(mass);

  mass->update_count++;
}

void CPUSpringMass::ComputeSpringForces(const shared_ptr<Spring>& spring) {
  auto m1 = spring->mass1;
  auto m2 = spring->mass2;
  const auto connection = m2->position - m1->position;
  const auto direction = glm::normalize(connection);
  const auto rest_length = spring->restLength;
  const auto current_length = glm::length(connection);
  // 4 times the original length seems like a safe "failsafe"
  // once we implement proper yield handling this should be fixed
  if (current_length > rest_length * 4) {
    cerr << "Length of spring exceeded 4x original length" << endl;
    cerr << "there is probably something wrong with your config" << endl;
    cerr << "double check the forces you are applying, and the bardiam" << endl;
    // assert(false);
  }
  const auto diff_length = current_length - rest_length;
  spring->diffLength = diff_length;
  const double scalar_force = diff_length * spring->stiffness;

  const auto force_vec = direction * scalar_force;
  m1->force += force_vec;
  m2->force -= force_vec;
}

glm::dvec3 CPUSpringMass::ForceAppliedBySpring(const shared_ptr<Mass>& m1,
                                               const shared_ptr<Mass>& m2,
                                               const shared_ptr<Spring>& spring) {
  // Using Hookes Law
  const auto connection = m2->position - m1->position;
  const auto direction = glm::normalize(connection);
  const auto rest_length = spring->restLength;
  const auto current_length = glm::length(connection);
  // 4 times the original length seems like a safe "failsafe"
  // once we implement proper yield handling this should be fixed
  if (current_length > rest_length * 4) {
    cerr << "Length of spring exceeded 4x original length\n";
    cerr << "there is probably something wrong with your config\n";
    cerr << "double check the forces you are applying, and the bardiam\n";
    this->failed = true;
    // assert(false);
  }
  const auto diff_length = current_length - rest_length;
  spring->diffLength = diff_length;
  const double scalar_force = diff_length * spring->stiffness;

  const auto force_vec = direction * scalar_force;
  return force_vec;
}

glm::dvec3 CPUSpringMass::ConnectedSpringForces(const shared_ptr<Mass>& this_mass) {
  shared_ptr<Mass> connected_mass;
  auto spring_forces = glm::dvec3(0, 0, 0);

  for (auto& spring : this_mass->connected_springs) {
    const auto m1 = spring->mass1;

    if (m1 == this_mass) {
      connected_mass = spring->mass2;
    } else {
      connected_mass = spring->mass1;
    }

    const auto spring_force_vec = ForceAppliedBySpring(this_mass, connected_mass, spring);
    const auto spring_force = glm::length(spring_force_vec);

    if (spring_force >= spring->maxForce) {
      spring->maxForce = spring_force;
    }

    spring_forces += spring_force_vec;
  }

  return spring_forces;
}

void CPUSpringMass::FirstStep() {
  // UpdateExternalForces();
  // for_each(execution::par_unseq, masses->begin(), masses->end(),
  //         [this](auto m) { this->ComputeMassForces(m); });
  // for_each(execution::par_unseq, masses->begin(), masses->end(),
  //         [this](auto m) { integration_method->initialize(m); });
}

void CPUSpringMass::Initialize(
    std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses,
    std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs,
    string integration_method,
    double time_step,
    double damping,
    bool is_rendering) {
  // prev_forces.resize(_volumes->size(), dvec3(0, 0, 0));
  // volumes = std::move(_volumes);
  this->damping = damping;
  this->masses = masses;
  this->springs = springs;
  this->time_step = time_step;
  this->is_rendering = is_rendering;
  this->time_step_ns = time_step * 1e9;

  // Set integration method
  if (integration_method == "euler") {
    this->integration_method = make_unique<Euler>(time_step, damping, this);
    cout << "Using Euler integration method" << endl;

  } else if (integration_method == "verlet") {
    this->integration_method = make_unique<Verlet>(time_step, damping, this);
    cout << "Using Verlet integration method" << endl;

  } else if (integration_method == "RK4") {
    this->integration_method = make_unique<RK4>(time_step, damping, this);
    cout << "Using RK4 integration method" << endl;

  } else {
    auto msg = "Integration method " + integration_method + "does not exist";
    throw invalid_argument(msg);
  }
}

// This can likely go away, but keeping it for API compat for now.
void CPUSpringMass::Step() {
  this->integration_method->step();
  simulation_time += time_step_ns;
}

void CPUSpringMass::AddTopologyOptimizer(shared_ptr<TopOpt::Optimizer> optimizer,
                                         unsigned long long interval_ns) {
  optimizers.push_back({optimizer, interval_ns});
  if (perform_update_interval == 1) {
    perform_update_interval = interval_ns;
  } else {
    perform_update_interval =
        // std::gcd(interval_ns, perform_update_interval);
        boost::math::gcd_evaluator<unsigned long long>()(interval_ns,
                                                         perform_update_interval);
  }
}

// Any actions we want to run after updates are done.
void CPUSpringMass::PostUpdate() {
  Telemetry::PositionTrace::instance().write();
  Telemetry::TotalEnergyTrace::instance().write();
  disableUpdating();
  is_done = true;
}

void CPUSpringMass::StepNoTime() {
  // if (!released)
  // UpdateExternalForces();
  // for_each(execution::par_unseq, masses->begin(), masses->end(),
  //         [this](auto m) { this->ComputeMassForces(m); });

  // initials.clear();
  // a_vec.clear();
  // b_vec.clear();
  // c_vec.clear();
  // d_vec.clear();

  // initials.resize(masses->size());
  // a_vec.resize(masses->size());
  // b_vec.resize(masses->size());
  // c_vec.resize(masses->size());
  // d_vec.resize(masses->size());

  // for (int i = 0; i < masses->size(); i++) {
  //  auto mass  = (*masses)[i];
  //  auto state = initials[i];
  //  auto a     = rk4_method->evaluate(mass, state);
  //  a_vec.push_back(a);
  //}

  // for (int i = 0; i < masses->size(); i++) {
  //  auto mass  = (*masses)[i];
  //  auto state = initials[i];
  //  auto a     = a_vec[i];
  //  auto b     = rk4_method->evaluate(mass, state, time_step * 0.5, a);
  //  b_vec.push_back(b);
  //}

  // for (int i = 0; i < masses->size(); i++) {
  //  auto mass  = (*masses)[i];
  //  auto state = initials[i];
  //  auto b     = b_vec[i];
  //  auto c     = rk4_method->evaluate(mass, state, time_step * 0.5, b);
  //  c_vec.push_back(c);
  //}

  // for (int i = 0; i < masses->size(); i++) {
  //  auto mass  = (*masses)[i];
  //  auto state = initials[i];
  //  auto c     = c_vec[i];
  //  auto d     = rk4_method->evaluate(mass, state, time_step, c);
  //  d_vec.push_back(d);
  //}

  // for_each(execution::par_unseq, masses->begin(), masses->end(),
  //         [this](auto m) { integration_method->integrate(m); });
}

// Disable this now to rule it out as a problem
void CPUSpringMass::UpdateExternalForces() {
  // compute delta between current force and prev_force for each volume
  // if the delta changes, update the value on all masses in the volume
  /*
  for (auto i = 0; i < volumes->size(); i++) {
    auto vol = (*volumes)[i];
    if (vol->masses_inside.size() == 0)
      continue;

    const auto prev_force = prev_forces[i];
    auto cur_force        = dvec3(0, 0, 0);
    for (const auto &force_iter : vol->forces) {
      cur_force += force_iter->Next(simulation_time);
    }
    // distribute force evenly over all masses in volume
    cur_force /= vol->masses_inside.size();

    auto delta     = cur_force - prev_force;
    prev_forces[i] = cur_force;

    if (!vol->fixed && delta != dvec3(0, 0, 0)) {
      for (const auto &mass : vol->masses_inside) {
        mass->externalForce += delta;
      }
    }
  }
  */
}

// void CPUSpringMass::RemoveTopologyOptimizer(int idx) {
// std::cout << "Removing Topology Optimizer: " <<
// std::get<0>(optimizers[idx])->Name() << std::endl; const auto end  =
// optimizers.size() - 1; optimizers[idx] = optimizers[end];
// optimizers.pop_back();
// unsigned long long x = 1;
// for (const auto &kv : optimizers) {
//   auto interval_ns = std::get<1>(kv);
//   if (x == 1) {
//     x = interval_ns;
//   } else {
//     x = boost::math::gcd_evaluator<unsigned long long>()(x, interval_ns);
//     // x = std::gcd(x, interval_ns);
//   }
// }
// perform_update_interval = x;
// }
