#include "Rule.h"

using namespace std;

namespace DML {
Rule::Rule() {}

Rule::~Rule() {}

vector<Loadcase> Rule::GetLoadcases() {
  return this->loadcases;
}

vector<Volume> Rule::GetVolumes() {
  return this->volumes;
}

void Rule::AppendLoadcase(Loadcase lc) {
  this->loadcases.push_back(lc);
}

void Rule::AppendVolume(Volume v) {
  this->volumes.push_back(v);
}

DML::RuleDamping Rule::GetDamping() {
  return this->damping;
}

DML::RuleOutput Rule::GetOutput() {
  return this->output;
}

DML::RuleLoad Rule::GetLoad() {
  return this->load;
}

DML::RuleMechanics Rule::GetMechanics() {
  return this->mechanics;
}

DML::RuleTrace Rule::GetTrace() {
  return this->trace;
}

DML::RuleStop Rule::GetStop() {
  return this->stop;
}

void Rule::SetDamping(DML::RuleDamping rule) {
  this->damping = rule;
}

void Rule::SetOutput(DML::RuleOutput rule) {
  this->output = rule;
}

void Rule::SetLoad(DML::RuleLoad load) {
  this->load = load;
}

void Rule::SetMechanics(DML::RuleMechanics mechanics) {
  this->mechanics = mechanics;
}

void Rule::SetDevelopment(DML::RuleDevelopment development) {
  this->development = development;
}

void Rule::SetTrace(DML::RuleTrace trace) {
  this->trace = trace;
}

void Rule::SetStop(DML::RuleStop stop) {
  this->stop = stop;
}
}  // namespace DML
