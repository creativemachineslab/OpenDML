from lxml import etree

import pathlib
import glob
import subprocess
import sys, os, time, datetime
import argparse

def save_system_info(output_dir):
    import wmi
    out_file = os.path.join(output_dir, "system_info.txt")
    f = open(out_file, 'w')

    computer = wmi.WMI()
    computer_info = computer.Win32_ComputerSystem()[0]
    os_info = computer.Win32_OperatingSystem()[0]
    proc_info = computer.Win32_Processor()[0]
    gpu_info = computer.Win32_VideoController()[0]

    os_name = os_info.Name.encode('utf-8').split(b'|')[0]
    os_version = ' '.join([os_info.Version, os_info.BuildNumber])
    system_ram = float(os_info.TotalVisibleMemorySize) / 1048576  # KB to GB

    with open(out_file, 'w') as f:
        f.write('OS Name: {0}\n'.format(os_name.decode('ascii')))
        f.write('OS Version: {0}\n'.format(os_version))
        f.write('CPU: {0}\n'.format(proc_info.Name))
        f.write('RAM: {0} GB\n'.format(system_ram))
        f.write('Graphics Card: {0}\n'.format(gpu_info.Name))

def run(command, _cwd):
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, cwd=_cwd)
    return iter(popen.stdout.readline, b'')

parser = argparse.ArgumentParser(description='The name of the folder containing the tests to run.')
parser.add_argument('scenario', metavar='scenario', type=str, help='The folder containing the tests to run')

args = parser.parse_args()

opendml_exe = "../../build/Release/opendml.exe"
(mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(opendml_exe)
print("OpenDML.exe last modified: %s" % time.ctime(mtime))
time.sleep(3)

this_dir, filename = os.path.split(sys.argv[0]) # directory our script is in
for var_type in ['performance']:
    for integration_method in ["euler", "verlet", "RK4"]:
        dml_files = glob.glob(os.path.join(this_dir, 'scenarios', args.scenario, 'dml_files', var_type, integration_method, '**/*.dml'), recursive=True)
        dml_files = [os.path.relpath(f, this_dir) for f in dml_files]
        print("Selected for processing: ")
        [print(f) for f in dml_files]
        for d in dml_files:
            xml = etree.parse(os.path.join(this_dir,d))
            telemetry = xml.xpath('/dml/rules/telemetry')[0]
            output_dir = os.path.join(this_dir, telemetry.attrib["output_dir"])
            pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
            log_file = os.path.join(output_dir, "stdout.txt")
            done_file = os.path.join(output_dir, "DONE")
            # if (os.path.exists(done_file)):
            #     print("Marked as done, skipping -", d)
            # else:
            print("Processing -", d)
            f = open(log_file,"bw+")
            # save_system_info(output_dir)
            start_time = time.clock()
            for line in run([opendml_exe, d, "--test-performance"], this_dir):
                f.write(line)
                print(line.decode('utf-8'), end='')
            f.close()
            # Write DONE file
            f = open(done_file, "w+")
            f.close()
            elapsed_seconds = time.clock() - start_time
            m, s = divmod(elapsed_seconds, 60)
            h, m = divmod(m, 60)
            print("Done in - %d:%02d:%02d" % (h, m, s))
