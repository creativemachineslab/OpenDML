#pragma once
#include "Loadcase.h"

namespace DML {
class RuleLoad {
 public:
  RuleLoad();
  ~RuleLoad();
  DML::Loadcase* loadcase;
};

}  // namespace DML
