#include "SceneModifier.h"
#include "LinesGeometry.h"

#include <Qt3DLogic/qframeaction.h>
#include <Qt3DRender/qeffect.h>
#include <Qt3DRender/qparameter.h>

#include <QtCore/QDebug>
#include <glm/geometric.hpp>
#include <iostream>

SceneModifier::SceneModifier(Qt3DCore::QEntity* rootEntity, Simulator* sim)
    : m_rootEntity(rootEntity), sim(sim) {
  // Initialize the entities managed by this scene...
  masses_modifier = new MassModifier(rootEntity, this->sim);
  springs_modifier = new SpringModifier(rootEntity, this->sim);
}

SceneModifier::~SceneModifier() {}

void SceneModifier::AddBoundingBoxes() {
  vector<QColor> bbox_colors = {
      QColor(QRgb(0xff0000)), QColor(QRgb(0xffffff)), QColor(QRgb(0x00ff50))};
  bbox_root = new Qt3DCore::QEntity(m_rootEntity);
  bbox_root->setEnabled(false);  // hide by default
  int vol_count = 0;
  for (auto volume_kv : sim->volumes) {
    auto v = volume_kv.second;

    auto cubeEntity = new Qt3DCore::QEntity(bbox_root);
    auto cube = new Qt3DExtras::QCuboidMesh();

    auto cube_transform = new Qt3DCore::QTransform();
    auto cube_material = new Qt3DExtras::QPhongAlphaMaterial();
    auto scale = QVector3D(v->bbox.xmax() - v->bbox.xmin(),
                           v->bbox.ymax() - v->bbox.ymin(),
                           v->bbox.zmax() - v->bbox.zmin());
    auto translate = scale * 0.5;  // move back to origin after scaling
    translate += QVector3D(v->bbox.xmin(), v->bbox.ymin(), v->bbox.zmin());
    cube_material->setAlpha(0.5f);
    // make sure we don't overflow our color options
    // cube_material->setAmbient(bbox_colors[vol_count % bbox_colors.size()]);

    // TODO, this is error prone if the user does not pass in a valid color...
    cube_material->setAmbient(
        QColor((int)v->color.r, (int)v->color.g, (int)v->color.b, 255.0));
    cube_transform->setScale3D(scale);
    cube_transform->setTranslation(translate);

    cubeEntity->addComponent(cube);
    cubeEntity->addComponent(cube_transform);
    cubeEntity->addComponent(cube_material);
    vol_count++;
  }
}

void SceneModifier::enableMasses(bool enabled) {
  masses_modifier->setEnabled(enabled);
}

void SceneModifier::enableBBox(bool enabled) {
  bbox_root->setEnabled(enabled);
}

void SceneModifier::enableForceVectors(bool enabled) {
  masses_modifier->setForcesEnabled(enabled);
}

void SceneModifier::enableSprings(bool enabled) {
  springs_modifier->setEnabled(enabled);
}

void SceneModifier::render() {
  if (masses_modifier->isEnabled()) {
    masses_modifier->SyncMasses();
  }
  if (springs_modifier->isEnabled()) {
    springs_modifier->SyncSprings();
  }
}
