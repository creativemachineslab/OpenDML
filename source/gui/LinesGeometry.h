// For drawing a single line

// ============================
// Point.h
// ============================

#include <Qt3DExtras/qt3dextras_global.h>
#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qgeometryrenderer.h>
#include <QtCore/qsize.h>
#include <qvector3d.h>
#include <Qt3DRender/Qt3DRender>
#include <vector>
#include "Vertex.h"

class Lines : public Qt3DRender::QGeometryRenderer {
  Q_OBJECT
 public:
  explicit Lines(Qt3DCore::QNode* parent = nullptr);
  ~Lines();
 public slots:
  void setVertices(std::vector<Vertex> vertices);
};

class LinesGeometry : public Qt3DRender::QGeometry {
  Q_OBJECT
 public:
  explicit LinesGeometry(Qt3DCore::QNode* parent = nullptr);
  ~LinesGeometry();

  void init();

  void updateVertices();
  void updateIndices();

  std::vector<Vertex> vertices;
  Qt3DRender::QBuffer* vertexBuffer;
  Qt3DRender::QBuffer* indexBuffer;
  Qt3DRender::QAttribute* positionAttribute;
  Qt3DRender::QAttribute* colorAttribute;
  Qt3DRender::QAttribute* indexAttribute;

 public slots:
  void setVertices(std::vector<Vertex> vertices);
};
