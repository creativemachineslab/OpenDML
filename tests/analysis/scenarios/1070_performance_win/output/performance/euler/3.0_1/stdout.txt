Performance Test Scenario Enabled
Running performance tests 
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 3.0000000000000004, 0.02, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 2.9975000000000005, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 2.9975000000000005, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: false
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\1070_performance_win\output\performance\euler\3.0_1
  integration
   method: euler
   time_step: 1e-06
  stop
   simulation_time: 100
   wall_clock_time: 0
   time_unit: ms
 
is_random 0
1
cantilever xMin: 0
cantilever xMax: 3
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 2.9975
loadpoint xMax: 3.0025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 2.9975
trace xMax: 3.0025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 0
Building grid lattice
cantilever xMin: 0
cantilever xMax: 3
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
Output dir: scenarios\1070_performance_win\output\performance\euler\3.0_1
All masses were unique
Calculated number of masses: 15025
Max Spring Connections: 26
Min Spring Connections: 7
Average Spring Connections: 19.2575
Max Spring Length: 0.00866032
Min Spring Length: 0.00499988
Max Spring Stiffness: 1.78761e+07
Min Spring Stiffness: 1.03204e+07
Average Spring Stiffness: 1.34372e+07
15025 masses
initial centroid0.01
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using Euler integration method
GPU Acceleration Successfully Enabled
Initializing performance tests...
step_count: 100000
Elapsed Seconds: 76.6389
Avg Time Per Step: 0.000766389
Masses per Second: 1.96049e+07
Springs per Second: 1.88771e+08
