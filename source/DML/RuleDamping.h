#pragma once
#include <iostream>

namespace DML {
class RuleDamping {
 public:
  RuleDamping();
  ~RuleDamping();
  std::string type;
  double amount = 0;
};
}  // namespace DML
