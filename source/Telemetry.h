#pragma once
#include "Analysis.h"
#include "Mass.h"
#include "Simulator.h"

#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <memory>

namespace Telemetry {
class CsvWriter {
 protected:
  virtual std::string header() { return ""; }
  std::ofstream outfile;

 public:
  CsvWriter();
  ~CsvWriter();
  void open(std::string filepath);
  void close();
  boost::filesystem::path telemetry_dir;
  // void addRow(...)  // subclasses implement this
};

struct PositionTraceData {
  double time;
  double x;
  double y;
  double z;
};

class PositionTrace : public CsvWriter {
  std::string header() override { return "time_ms, x,y,z"; }

 public:
  std::function<dvec3(void)> data_source;
  void update();
  void write();
  void debug();
  void replay(std::vector<glm::dvec3> positions);

  static auto& instance() {
    static PositionTrace pt;
    return pt;
  }

 private:
  std::vector<PositionTraceData> position_traces;
};

class ConnectedSprings : public CsvWriter {
  std::string header() override { return "x,y,z,spring_count"; }

 public:
  void update();

  static auto& instance() {
    static ConnectedSprings pt;
    return pt;
  }

 private:
};

class TotalEnergyTrace : public CsvWriter {
  std::string header() override { return "timestep,epe,gpe,ke,total"; }

 public:
  void write();
  void update();

  void AddRow(unsigned long long timestep, double gpe, double ke, double epe);
  static auto& instance() {
    static TotalEnergyTrace pt;
    return pt;
  }

 private:
  std::vector<EnergyData> energy_traces;
};

}  // namespace Telemetry
