---
header-includes:
 - \usepackage{float}
---

\floatplacement{figure}{H}

# Approach:

We create a cantilever beam from a lattice of masses and springs, apply
a force to the free end, and measure the y position of a tip mass for
100k iterations.  By applying a Fourier Transform to the position
measurements, we identify the strongest frequency in the signal and
label that the first fundamental frequency.

By repeating this process many times with varying parameters, we create
simple experiments to validate that the simulator's physics is
realistic.  The results of experiments below demonstrate that the
simulator conforms to the expected first fundamental frequency as
defined by Euler Bernoulli Beam Theory.

Each experiment has only 1 independent variable.  The `beam_x_length`
experiment varies only beam length, $L$.  `beam_y_length` changes beam
height, $H$.  `beam_z_length` varies $W$.  We also changed the
density, $\rho$ (`masses_on_smallest_side`) by adjusting the number of
masses we can pack into the unit cube.

The theoretical values were derived from Euler Bernoulli Beam Theory.
Specifically, we check that the measured frequencies are proportional to
the theoretical ones.

\pagebreak

# Euler Bernoulli Beam Theory

The equation for the first fundamental frequency of a cantilever beam
subjected to free vibration and a uniformly distributed load (gravity):

$$f = \frac{K}{2\pi} \sqrt{\frac{EIg}{\rho A L^4}}$$

- $K_i/2\pi$ is a constant corresponding to ith fundamental frequency.
- $E$ is the modulus of elasticity
- $I$ is second moment of area
- $g$ is constant acceleration, only applied in Y direction (0, -9.8, 0)
- $L$ is beam length along x axis
- $\rho$ is mass density
- $A$ is the area of a yz cross-section, $A = WH$

$I$, the 2nd moment of area, measures spread of masses along one
particular axis, and is a proxy for stiffness due to geometry.  Recall
the beam length, $L$ is along x axis, force is applied parallel to the y
axis, and beam vibrates along y.  We therefore want the spread of masses
of y.

$$I = \iint H^2 \,dH\,dW = H^3 W / 12$$

Finally, we reformulate the equation in terms of beam dimensions,
$L$, $H$ and $W$ and we drop variables that are constant in our
experiments:

$$f \propto \sqrt{\frac{I}{\rho AL^4}} \propto \sqrt{\frac{WH^3}{\rho WH L^4}} = \sqrt{\frac{H^2}{\rho L^4}}$$

## `beam_x_length`:

This experiment varies only L.

$f \propto \sqrt{1/L^4} = \frac{1}{l^2}$

## `beam_y_length` and `beam_z_length`:

These experiments vary only H and W, respectively.

$f_y \propto \sqrt{H^2} = H$

$f_z \propto \sqrt{1} = 1$


## masses_on_smallest_side

This experiment varies only $\rho$.

$f \propto \sqrt{1/\rho}$

\pagebreak
# Results

![beam_x_length](./beam_x_length.png)

\pagebreak

![beam_y_length](./beam_y_length.png)

\pagebreak

![beam_z_length](./beam_z_length.png)

\pagebreak

![masses_on_smallest_side](./masses_on_smallest_side.png)

\pagebreak

![freq_analysis](./freq_analysis.png)
