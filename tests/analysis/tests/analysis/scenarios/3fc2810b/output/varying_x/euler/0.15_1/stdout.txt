XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.15000000000000002, 0.02, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: -0.0025, -0.0005, -0.0005
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 0.14750000000000002, -0.0005, -0.0005
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.021
 translate: 0.14750000000000002, -0.0005, -0.0005
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0, -200, 0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: .\tests\analysis\scenarios\3fc2810b\output\varying_x\euler\0.15_1
  integration
   method: euler
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
1
cantilever xMin: 0
cantilever xMax: 0.15
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.1475
loadpoint xMax: 0.1525
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.1475
trace xMax: 0.1525
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.0205
cantilever xMin: 0
cantilever xMax: 0.15
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.02
Output dir: .\tests\analysis\scenarios\3fc2810b\output\varying_x\euler\0.15_1
All masses were unique
Number of masses is correct.
Debug Output
==========
mass_xmax	0.15
mass_xmin	0
mass_ymax	0.02
mass_ymin	0
mass_zmax	0.02
mass_zmin	0
max_spring_stiffness	1.78757e+07
min_spring_stiffness	1.03205e+07
total_volume_mass	0.175924
total_force	500
load_volume_count	25
anchor_volume_count	25
traced_count	25
cantilever_count	775
total_masses	775
total_spring_count	7302
==========
Centroid starting value: 
0
Number of springs: 7302
Number of masses: 775
anchor: 25 masses
cantilever: 775 masses
loadpoint: 25 masses
trace: 25 masses
Total mass: 0.175924 kg
Stopped at 500.017 ms
