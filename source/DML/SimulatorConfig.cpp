#include "SimulatorConfig.h"

SimulatorConfig::SimulatorConfig() {}

SimulatorConfig::~SimulatorConfig() {}

bool SimulatorConfig::setWindowHeight(size_t h) {
  this->windowHeight = h;
  return true;
}

bool SimulatorConfig::setWindowWidth(size_t w) {
  this->windowWidth = w;
  return true;
}

bool SimulatorConfig::setWindowSize(size_t w, size_t h) {
  this->windowHeight = h;
  this->windowWidth = w;
  return true;
}
