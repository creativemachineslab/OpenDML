// For drawing a single line

// ============================
// Point.h
// ============================
#include "Vertex.h"

#include <Qt3DExtras/qt3dextras_global.h>
#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qgeometryrenderer.h>
#include <QtCore/qsize.h>
#include <qvector3d.h>
#include <Qt3DRender/Qt3DRender>
#include <vector>

class Surface : public Qt3DRender::QGeometryRenderer {
  Q_OBJECT
 public:
  explicit Surface(Qt3DCore::QNode* parent = nullptr);
  ~Surface();
 public slots:
  void setPositions(std::vector<Vertex> positions) const;
};

class SurfaceGeometry : public Qt3DRender::QGeometry {
  Q_OBJECT
 public:
  explicit SurfaceGeometry(QNode* parent = nullptr);
  ~SurfaceGeometry();

  void init();

  void updateVertices();
  void updateIndices();

  std::vector<Vertex> positions;
  Qt3DRender::QBuffer* vertexBuffer;
  Qt3DRender::QBuffer* indexBuffer;
  Qt3DRender::QAttribute* positionAttribute;
  Qt3DRender::QAttribute* normalAttribute;
  Qt3DRender::QAttribute* indexAttribute;

 public slots:
  void setPositions(std::vector<Vertex> positions);
};
