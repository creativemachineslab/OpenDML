#include "TopologyOptimization.h"

namespace TopOpt {
using namespace std;

DropSprings::DropSprings(int num_to_remove,
                         double stop_at_pct_springs_remaining,
                         Masses masses,
                         Springs springs)
    : num_to_remove(num_to_remove)
    , stop_at_pct_springs_remaining(stop_at_pct_springs_remaining)
    , masses(masses)
    , springs(springs)
    , initial_spring_count(springs->size())
    , initial_mass_count(masses->size()) {
  for (auto const m : *masses) {
    initial_total_mass += m->mass;
  }
}

void DropSprings::DeleteSpring(shared_ptr<Spring> s) {
  auto position = find(springs->begin(), springs->end(), s);
  if (position != springs->end()) {
    springs->erase(position);
  } else {
    std::cout << "spring was not found" << endl;
    assert(false);
  }
  // springs->erase(remove(springs->begin(), springs->end(), s), springs->end());
}

void DropSprings::DeleteMass(shared_ptr<Mass> mass) {
  auto position = find(masses->begin(), masses->end(), mass);
  if (position != masses->end()) {
    masses->erase(position);
  } else {
    assert(false);
  }
  // masses->erase(remove(masses->begin(), masses->end(), s->mass1), masses->end());
}

void DropSprings::Optimize() {
  springs_mutex().lock();
  mass_mutex().lock();
  vector<shared_ptr<Spring>> springs_to_remove;

  // sort springs by maxForce
  auto maxForceComp = [](shared_ptr<Spring> lhs, shared_ptr<Spring> rhs) -> bool {
    // send undesirable springs to end...
    if (lhs->mass1->fixed || lhs->mass2->fixed || lhs->mass1->UnderExternalForce() ||
        lhs->mass2->UnderExternalForce()) {
      return false;
    }
    return lhs->maxForce < rhs->maxForce;
  };
  std::sort(springs->begin(), springs->end(), maxForceComp);

  int copied_count = 0;
  copy_if(springs->begin(),
          springs->end(),
          back_inserter(springs_to_remove),
          [&](shared_ptr<Spring> spring) {
            if (copied_count >= num_to_remove) {
              return false;
            }
            if (spring->mass1->fixed || spring->mass2->fixed ||
                spring->mass1->UnderExternalForce() ||
                spring->mass2->UnderExternalForce()) {
              return false;
            }
            copied_count++;
            return true;
          });
  for (const auto s : springs_to_remove) {
    if (s->mass1->fixed || s->mass2->fixed) {
      assert(false);
    }

    DeleteSpring(s);
    s->mass1->RemoveSpring(s);
    s->mass2->RemoveSpring(s);
  }

  // brute force approach, this needs some work
  // basically we need to iterate over all the
  // connected springs and remove them from not just _this_ mass, but
  // also the other mass the spring is connected to. If either of those
  // drop below 2 connected springs, we remove it. It is possible that
  // after 1 pass the following process actually causes a new mass to have
  // less than 2 springs, so we should do a few more passes to ensure we get all
  // of them
  for (int i = 0; i < 3; i++) {
    vector<shared_ptr<Mass>> masses_to_delete;
    for (const auto m : *masses) {
      if (m->connected_springs.size() <= 2) {
        vector<shared_ptr<Spring>> con_springs_to_remove;
        for (const auto sc : m->connected_springs) {
          con_springs_to_remove.push_back(sc);
        }
        for (const auto sc : con_springs_to_remove) {
          sc->mass1->RemoveSpring(sc);
          sc->mass2->RemoveSpring(sc);
          DeleteSpring(sc);
        }

        masses_to_delete.push_back(m);
      }
    }

    for (const auto m : masses_to_delete) {
      DeleteMass(m);
    }
  }

  springs_mutex().unlock();
  mass_mutex().unlock();

  PrintSummary();
}

bool DropSprings::Enabled() {
  auto enough_springs = [=]() -> bool {
    double pct_springs_remaining =
        static_cast<double>(springs->size()) / initial_spring_count;
    return stop_at_pct_springs_remaining < pct_springs_remaining;
  };
  return enough_springs();
}

}  // namespace TopOpt
