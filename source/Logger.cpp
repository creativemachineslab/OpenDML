#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

namespace Logger {
std::shared_ptr<spdlog::logger> getLogger(const std::string& loggerName) {
  return spdlog::stdout_color_mt(loggerName);
}
}  // namespace Logger
