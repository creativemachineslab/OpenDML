#include "Config.h"

using namespace std;

namespace DML {
Config::Config() {}

Config::~Config() {}

void Config::SetRule(Rule rule) {
  this->rule = rule;
}

void Config::SetMaterial(Material material) {
  this->material = material;
}

void Config::AppendVolume(Volume volume) {
  volumes.push_back(volume);
}

void Config::AppendLoadcase(Loadcase loadcase) {
  loadcases.push_back(loadcase);
}

Rule* Config::GetRule() {
  return &rule;
}

Material* Config::GetMaterial() {
  return &material;
}

vector<Volume>* Config::GetVolumes() {
  return &volumes;
}

vector<Loadcase>* Config::GetLoadcases() {
  return &loadcases;
}
}  // namespace DML
