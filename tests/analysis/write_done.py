from lxml import etree

import pathlib
import glob
import subprocess
import sys, os, time, datetime
import argparse

def run(command, _cwd):
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, cwd=_cwd)
    return iter(popen.stdout.readline, b'')

parser = argparse.ArgumentParser(description='The name of the folder containing the tests to run.')
parser.add_argument('scenario', metavar='scenario', type=str, help='The folder containing the tests to run')

args = parser.parse_args()

this_dir, filename = os.path.split(sys.argv[0]) # directory our script is in
for var_type in ['varying_x', 'varying_y', 'varying_z']:
    for integration_method in ["euler", "verlet"]:
        dml_files = glob.glob(os.path.join(this_dir, 'scenarios', args.scenario, 'dml_files', var_type, integration_method, '**/*.dml'), recursive=True)
        dml_files = [os.path.relpath(f, this_dir) for f in dml_files]
        # print(dml_files)
        for d in dml_files:
            xml = etree.parse(os.path.join(this_dir,d))
            telemetry = xml.xpath('/dml/rules/telemetry')[0]
            output_dir = os.path.join(this_dir, telemetry.attrib["output_dir"])
            if (os.path.exists(output_dir)):
                trace_file = os.path.join(output_dir, "telemetry", "position_trace.csv")
                if (os.path.exists(trace_file)):
                    if (os.path.getsize(trace_file) > 10000): # bytes
                        f = open(os.path.join(output_dir, "DONE"), 'w+')
                        f.close()
            else:
                print("does not exist", output_dir)