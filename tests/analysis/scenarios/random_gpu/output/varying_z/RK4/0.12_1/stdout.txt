Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.02, 0.12000000000000001
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.12100000000000001
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.12100000000000001
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.12100000000000001
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_z\RK4\0.12_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.12
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.1205
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.1205
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.1205
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_z\RK4\0.12_1
All masses were unique
Calculated number of masses: 5125
Max Spring Connections: 20
Min Spring Connections: 9
Average Spring Connections: 19.9169
Max Spring Length: 0.0177774
Min Spring Length: 0.00340636
Max Spring Stiffness: 2.62386e+07
Min Spring Stiffness: 5.02763e+06
Average Spring Stiffness: 1.40254e+07
5125 masses
initial centroid0.00946819
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
3084
3100
3109
3114
3122
3143
3163
3168
3171
3173
3174
3193
3199
3204
3216
3217
3233
3234
3243
3255
3261
3273
3277
3292
3300
3321
3331
3362
3386
3404
3410
3417
3426
3430
3432
3442
3473
3487
3508
3532
3580
3598
3600
3608
3687
3705
3717
3733
3806
3817
3833
3921
3958
3969
4010
4077
4157
4172
4177
4211
4234
4260
4276
4294
4356
4364
4418
4422
4447
4511
4530
4535
4574
4605
4637
4666
4675
4714
4749
4796
4798
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.00946819
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 741.83
Avg Time Per Step: 0.00074183
Masses per Second: 6.90859e+06
Springs per Second: 6.87988e+07
