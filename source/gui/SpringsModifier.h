#pragma once

#include "Simulator.h"
#include "Vertex.h"

#include <Qt3DCore/QEntity>
#include <QtCore/QObject>
#include <memory>
#include <vector>

class SpringModifier : public QObject {
  Q_OBJECT

 public:
  SpringModifier(Qt3DCore::QEntity* rootEntity, Simulator* sim);
  ~SpringModifier();
 public slots:
  void SyncSprings();
  void setEnabled(bool enabled);
  bool isEnabled();

 private:
  void updateSpringBuffer();
  Qt3DCore::QEntity* root_entity;
  Qt3DCore::QEntity* springs_root;
  Simulator* sim;
  vector<Vertex> spring_buffer;
};
