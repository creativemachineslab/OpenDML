#include "LiveLineChart.h"
#include <QtCharts/qvalueaxis.h>
#include <qlineseries.h>

using namespace QtCharts;
using namespace std;

LiveLineChart::LiveLineChart(std::function<EnergyData(void)> data_source)
    : data_source(data_source) {
  auto id_titles = {make_pair("gpe"s, "Gravitational Potential Energy"),
                    make_pair("ke"s, "Total Energy"),
                    make_pair("epe"s, "Energies"),
                    make_pair("trace"s, "Position Trace")};

  for (const auto c : id_titles) {
    auto chart = new QChart();
    // chart->legend()->hide();
    chart->setTitle(c.second);
    chart->setMargins(QMargins(0, 0, 0, 0));

    auto chart_view = new QChartView(chart);
    chart_view->setRenderHint(QPainter::Antialiasing);
    chart_view->setMinimumWidth(500);
    chart_view->setMaximumHeight(300);
    chart_views.push_back(chart_view);
    charts.push_back(make_pair(c.first, chart));
  }
}

void LiveLineChart::update() {
  update_count++;

  auto ds = data_source();

  if (epe.size() > max_elems_to_show) {
    epe.pop_front();
    ke.pop_front();
    gpe.pop_front();
    trace.pop_front();
    total.pop_front();
  }

  epe.push_back(QPointF(update_count, ds.elastic_potential_energy));
  gpe.push_back(QPointF(update_count, ds.gravitational_potential_energy));
  ke.push_back(QPointF(update_count, ds.kinetic_energy));

  auto epe_series = new QLineSeries();
  epe_series->replace(epe);
  epe_series->setName("EPE");

  auto gpe_series = new QLineSeries();
  gpe_series->replace(gpe);
  gpe_series->setName("GPE");

  auto ke_series = new QLineSeries();
  ke_series->replace(ke);
  ke_series->setName("KE");

  auto total_energy_series = new QLineSeries();

  total.push_back(QPointF(update_count,
                          ds.elastic_potential_energy +
                              ds.gravitational_potential_energy + ds.kinetic_energy));

  total_energy_series->replace(total);

  // auto get_double_vec = [](QList<QPointF> data) {
  //  vector<double> dbl_vec;
  //  auto data_vec = data.toVector().toStdVector();
  //  for (auto t : data_vec) {
  //    dbl_vec.push_back(t.y());
  //  }
  //  return dbl_vec;
  //};
  //
  // auto test_vec = get_double_vec(total);
  // auto totals_std_dev = StandardDeviation(get_double_vec(total));

  auto trace_series = new QLineSeries();

  for (const auto c : charts) {
    if (c.first == "gpe") {
      c.second->removeAllSeries();
      c.second->addSeries(gpe_series);
      c.second->createDefaultAxes();
      // auto va = static_cast<QValueAxis *>(c.second->axisY());
      // va->setMax(93.25);
      // va->setMin(93);
    }
    if (c.first == "ke") {
      c.second->removeAllSeries();
      c.second->addSeries(total_energy_series);
      c.second->createDefaultAxes();
      // auto va = static_cast<QValueAxis *>(c.second->axisY());
      // va->setMax(93.25);
      // va->setMin(93);
    }
    if (c.first == "epe") {
      c.second->removeAllSeries();
      c.second->addSeries(epe_series);
      // c.second->addSeries(gpe_series);
      c.second->addSeries(ke_series);
      c.second->createDefaultAxes();
    }
    if (c.first == "trace") {
      trace.push_back(QPointF(update_count, Analysis::GetVolumeTrace().y));
      trace_series->replace(trace);
      c.second->removeAllSeries();
      c.second->addSeries(trace_series);
      c.second->createDefaultAxes();
    }

    // va->setLabelFormat(QString("%.3e"));
  }
  // chart->addSeries(ke_series);
  // chart->addSeries(epe_series);
}

void LiveLineChart::capture() {
  int id = 0;
  for (const auto cv : chart_views) {
    auto chart_dir = QString("chart_%1").arg(id).toStdString();
    auto filename = QString("%1.png").arg(update_count).toStdString();
    auto out_path = chart_image_directory / chart_dir;

    if (!(exists(out_path))) {
      create_directories(out_path);
    }

    out_path = out_path / filename;

    cv->grab().save(QString().fromStdString(out_path.string()));
    id++;
  }
}
