#pragma once

#include "Logger.h"
#include "SpringMass.h"
#include "TopologyOptimization.h"
#include "Volume.h"

#include <functional>
#include <glm/glm.hpp>
#include <memory>
#include <numeric>
#include <tuple>
#include <vector>

class CPUSpringMass : public SpringMass {
 public:
  void AddTopologyOptimizer(shared_ptr<TopOpt::Optimizer> optimizer,
                            unsigned long long interval_ns);

  virtual void Step();
  virtual void Initialize(std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses,
                          std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs,
                          std::string integration_method,
                          double _time_step,
                          double _damping,
                          bool _is_rendering);

  virtual void ComputeMassForces(const std::shared_ptr<Mass>& mass);
  virtual void release();
  virtual void PostUpdate();
  dvec3 ConnectedSpringForces(const std::shared_ptr<Mass>& this_mass);
  dvec3 ForceAppliedBySpring(const std::shared_ptr<Mass>& this_mass,
                             const std::shared_ptr<Mass>& connected_mass,
                             const std::shared_ptr<Spring>& spring);

  void UpdateExternalForces();
  void ComputeSpringForces(const std::shared_ptr<Spring>& spring);
  void FirstStep();
  void StepNoTime();

 private:
  std::vector<glm::dvec3> prev_forces;
  std::unique_ptr<std::vector<std::shared_ptr<Volume>>> volumes;
  std::vector<std::tuple<std::shared_ptr<TopOpt::Optimizer>, unsigned long long>>
      optimizers;
  unsigned long long perform_update_interval = 1;
};
