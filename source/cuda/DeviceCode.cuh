#pragma once

#include "GPUSpringMass.h"

#include <glm/glm.hpp>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#define MAX_CONNECTED_SPRINGS 32

namespace cuda {

struct cuMass;
struct cuSpring;

struct cuMass {
  bool fixed;
  double mass;
  glm::dvec3 appliedAcceleration;
  glm::dvec3 currentVelocity;
  glm::dvec3 externalForce;
  glm::dvec3 force;
  glm::dvec3 position;
  glm::dvec3 originalPosition;
  glm::dvec3 lastPosition;
  unsigned int connected_springs[MAX_CONNECTED_SPRINGS];
  cuSpring* connected_springs_ptr[MAX_CONNECTED_SPRINGS];
  unsigned int num_connected_springs = 0;
  unsigned int forces_idx = 0;
  glm::dvec3 forces[MAX_CONNECTED_SPRINGS];
};

struct cuSpring {
  double stiffness;
  double mass;
  unsigned int mass1_idx;
  unsigned int mass2_idx;

  cuMass* mass1;
  cuMass* mass2;

  double restLength;
  double diffLength;
};

struct d_data {
  thrust::device_vector<cuMass> masses;
  thrust::device_vector<cuSpring> springs;
  thrust::device_vector<cuMass *> masses_to_trace;
  thrust::device_vector<int> indices_to_trace;
  IntegrationMethods integration_method;
  // thrust::device_vector<int> masses_to_trace;
  int * trace_results_idx = 0;
  thrust::device_vector<glm::dvec3> trace_results;
  thrust::device_vector<RK4_States> rk4_states;
} device_data;

struct h_data {
  thrust::device_vector<glm::dvec3 *> positions_to_trace;
  thrust::host_vector<int> indices_to_trace;
  thrust::host_vector<cuMass> masses;
  thrust::host_vector<cuSpring> springs;
  std::vector<glm::dvec3> trace_results;
  IntegrationMethods integration_method;
} host_data;

struct compute_spring_forces {
  __device__ void operator()(cuSpring& spring);
};

struct compute_mass_forces {
  compute_mass_forces(double damping, double time_step, double time_step_sq, IntegrationMethods im)
      : damping(damping), time_step(time_step), time_step_sq(time_step_sq), integration_method(im) {};
  __device__ void operator()(cuMass& mass);

  IntegrationMethods integration_method;
  double damping;
  double time_step;
  double time_step_sq;
};

struct euler_integrate {
  euler_integrate(double damping, double time_step)
      : damping(damping), time_step(time_step){};
  __device__ void operator()(cuMass& mass);

 private:
  double damping;
  double time_step;
};

struct verlet_integrate {
  verlet_integrate(double damping, double time_step, double time_step_sq)
      : damping(damping), time_step(time_step), time_step_sq(time_step_sq){};

  __device__ void operator()(cuMass& mass);

 private:
  double damping;
  double time_step;
  double time_step_sq;
};

struct verlet_initialize {
  verlet_initialize(double damping, double time_step, double time_step_sq)
      : damping(damping), time_step(time_step), time_step_sq(time_step_sq){};

  __device__ void operator()(cuMass& mass);

 private:
  double damping;
  double time_step;
  double time_step_sq;
};

};  // namespace cuda
