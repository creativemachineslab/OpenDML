#include "Telemetry.h"

using namespace std;

namespace Telemetry {
CsvWriter::CsvWriter() {}

CsvWriter::~CsvWriter() {
  close();
}

void CsvWriter::open(string filepath) {
  if (!(exists(telemetry_dir))) {
    create_directories(telemetry_dir);
  }

  auto out_path = telemetry_dir / filepath;
  outfile.open(out_path.string(), ofstream::out | ofstream::trunc);
  outfile << header().c_str() << endl;
}

void CsvWriter::close() {
  outfile.close();
}

void ConnectedSprings::update() {
  if (outfile.is_open()) {
    auto masses = Simulator::instance().masses;
    for (const auto m : *masses) {
      outfile << m->originalPosition.x;
      outfile << ",";
      outfile << m->originalPosition.y;
      outfile << ",";
      outfile << m->originalPosition.z;
      outfile << ",";
      outfile << m->connected_springs.size();
      outfile << endl;
    }
  }
}

void PositionTrace::write() {
  if (outfile.is_open()) {
    for (const auto pt : position_traces) {
      outfile << std::fixed << std::setprecision(20) << pt.time << "," << pt.x << ","
              << pt.y << "," << pt.z << std::endl;
    }
  } else {
    cout << "Position trace file is closed..." << endl;
  }
}

void PositionTrace::debug() {
  const auto time = Simulator::instance().spring_mass->GetSimulationTimeMS();
  auto volume = Simulator::instance().volumes["trace"];
  auto centroid = dvec3(0, 0, 0);
  PositionTraceData trace = {};
  auto row_selected = false;
  auto row_y = 0.0;
  for (const auto& m : volume->masses_inside) {
    if (!row_selected) {
      row_y = m->originalPosition.y;
      row_selected = true;
    }
    if (row_y != m->originalPosition.y) {
      continue;
    }
    trace.time = time;
    trace.x = m->position.x;
    trace.y = m->position.y;
    trace.z = m->position.z;
    position_traces.push_back(trace);

    trace.time = time;
    trace.x = m->currentVelocity.x;
    trace.y = m->currentVelocity.y;
    trace.z = m->currentVelocity.z;
    position_traces.push_back(trace);
  }
  trace.time = 999.0;
  trace.x = 0.0;
  trace.y = 0.0;
  trace.z = 0.0;
  position_traces.push_back(trace);
}

// This is used by the GPU method to "replay"
// the results and fill the data structures on the CPU
void PositionTrace::replay(std::vector<glm::dvec3> positions) {
  auto config = Simulator::instance().dml_config;
  const auto timeout_simulation_ns = config.GetRule()->GetStop().simulation_ns;
  const auto time_step_ms = Simulator::instance().spring_mass->time_step * 1e3;
  double time = 0.0;
  for (const auto p : positions) {
    PositionTraceData trace = {};
    trace.time = time;
    trace.x = p.x;
    trace.y = p.y;
    trace.z = p.z;
    position_traces.push_back(trace);
    time += time_step_ms;
  }
  cout << "position_traces_size " << position_traces.size() << endl;
}

void PositionTrace::update() {
  const auto position = data_source();
  const auto time = Simulator::instance().spring_mass->GetSimulationTimeMS();
  PositionTraceData trace = {};
  trace.time = time;
  trace.x = position.x;
  trace.y = position.y;
  trace.z = position.z;
  position_traces.push_back(trace);
}

void TotalEnergyTrace::update() {
  auto energy = Analysis::GetCurrentyEnergy();
  energy.time = Simulator::instance().spring_mass->simulation_time / 1e6;
  energy_traces.push_back(energy);
}

void TotalEnergyTrace::write() {
  if (outfile.is_open()) {
    for (const auto e : energy_traces) {
      outfile << std::fixed << std::setprecision(20) << e.time << ","
              << e.elastic_potential_energy << ", " << e.gravitational_potential_energy
              << ", " << e.kinetic_energy << ", " << e.total_energy << ", " << std::endl;
    }
  } else {
    cout << "Position trace file is closed..." << endl;
  }
}

void TotalEnergyTrace::AddRow(unsigned long long timestep,
                              double gpe,
                              double ke,
                              double epe) {
  if (outfile.is_open()) {
    outfile << timestep << "," << gpe << "," << ke << "," << epe << endl;
  }
}
}  // namespace Telemetry
