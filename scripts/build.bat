call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
git submodule init
git submodule update
mkdir build
pushd build
echo ************************************
echo Generating Cmake Files
echo ************************************
cmake .. -G Ninja -DQt5_Dir=C:\Qt -DCMAKE_TOOLCHAIN_FILE=C:\vcpkg\scripts\buildsystems\vcpkg.cmake
echo ************************************
echo Building
echo ************************************
cmake --build .
