#pragma once
#include "Volume.h"

namespace DML {
class Lattice {
 public:
  Lattice();
  ~Lattice();

  DML::Volume* volume;
  double bardiam;
  glm::vec3 offset;
  glm::vec3 size;
  std::string cell;
  bool is_random = false;
  double vertexmass;
};
}  // namespace DML
