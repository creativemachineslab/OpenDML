#include "Loadcase.h"

using namespace std;

namespace DML {
Loadcase::Loadcase() {}

Loadcase::~Loadcase() {}

void Loadcase::AddProperty(LoadcaseProperty prop) {
  properties.push_back(prop);
}

void Loadcase::setID(string id) {
  this->id = id;
}

string Loadcase::GetID() {
  return this->id;
}

// Returns a collection of all LoadcaseProperties
vector<LoadcaseProperty> Loadcase::Properties() {
  return this->properties;
}

}  // namespace DML
