## Windows

### Windows Development Environment
Recommended Environment:
* [Visual Studio Community 2015 SP3](https://my.visualstudio.com/Downloads?q=visual%20studio%202015&wt.mc_id=o~msft~vscom~older-downloads)
* Use Visual Studio 2017 at your own risk, since upgrades seem prone to break CUDA integrations. Visual Studio 2015 is highly recommended for now.
* CUDA 9.1 Toolkit
* [VCPKG](https://github.com/Microsoft/vcpkg) to build our C++ library dependencies. 
* Cmake 3.9 or greater (since we rely on built in support for CUDA)

### Setup VCPKG
```
# In powershell

PS> cd ~
PS> mkdir tools
PS> cd tools
PS> git clone https://github.com/Microsoft/vcpkg.git
PS> cd vcpkg
PS> .\bootstrap-vckpg.bat
PS> .\vcpkg integrate install # Keep the output showing `CMAKE_TOOLCHAIN_FILE` variable
PS> .\vcpkg integrate powershell # You may need to 
PS> Set-ExecutionPolicy Unrestricted -Scope CurrentUser # May need to run this to allow the vcpkg powershell integration to work
```

### Install dependencies
```
PS> vcpkg --triplet x64-windows install freeglut glew glm pugixml
```

### Build CMake
Note that CUDA only supports x64

```
git clone <this repository>
git submodule update --init
PS> cmake . -G "Visual Studio 14 2015 Win64" -DCMAKE_TOOLCHAIN_FILE=C:/Users/joelr/tools/vcpkg/scripts/buildsystems/vcpkg.cmake
```

### Helper Scripts
There are several PowerShell scripts.  The scripts may need to be
customized to point to your vcpkg installation.

- `scripts\run_cmake.ps1` runs cmake and set the proper variables
  for the build target and the vcpkg cmakefiles.
- `scripts\clean_cmake.ps1` cleans up cmake cache and other build files.
- `scripts\test\crossplatform_build.ps1` performs a test build using
  Windows environment and one using Ubuntu (through WSL). It then
  verifies that the binary was produced and executes.

## Setup for Linux Support

### Dependencies

A bash script identifies all dependencies (and sub-dependencies)
necessary to build OpenDML from source.  You may prefer to try your OS package
manager to install dependencies, but you can also use the install script below:

```
. ./scripts/system_deps.sh
export_shell_variables
install_LIBRARY
```

Alternatively, you may use your package manager to install dependencies locally:

The following are necessary to build.
```
 apt-get install build-essential libglm-dev libpugixml-dev libboost-dev libboost-filesystem-dev ninja-build cmake libcgal-dev git -y
```

Refer to `./scripts/tacc.sh` for an example

### Build and compile OpenDML

```

git clone <this repository>
git submodule update --init
./scripts/build [options]
```

You may supply these build options to the build script:

```
-DUSE_QT=0
-DUSE_CUDA=0
-DUSE_OpenMP=0
```

Also, defining `NUM_CORES` will change parallelization of build time

```
export NUM_CORES=8  # 8 is default
```

### Run opendml

```
./scripts/run_sim -h
```

If QT is disabled, you must supply --hide-window and --apply-forces.
The --fp-trace-position option is not strictly neessary.
# TODO: change default value if QT disabled...

```
./scripts/run_sim \
    --hide-window \
    --apply-forces \
    --timeout-ts 5000 \
    --fp-trace-position test.csv

```
### cmake options that might be needed for Qt to work

```
-DCMAKE_PREFIX_PATH=~/Qt/5.10.1/gcc_64/lib/cmake/
```

