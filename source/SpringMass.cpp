#include "SpringMass.h"
#include "GPUSpringMass.h"
#include "Telemetry.h"

using namespace std;

// For load and release scenario... hardcoded for now.

void SpringMass::enableUpdating() {
  is_updating = true;
}

void SpringMass::disableUpdating() {
  is_updating = false;
}

bool SpringMass::isDone() {
  return is_done;
}

void SpringMass::PerformUpdate() {
  if (Simulator::instance().scenario == SCENARIO::BEAM_TEST) {
    Telemetry::PositionTrace::instance().update();
  }

  auto config = Simulator::instance().dml_config;
  const auto timeout_simulation_ns = config.GetRule()->GetStop().simulation_ns;
  const auto timeout_wall_clock_ns = config.GetRule()->GetStop().wall_clock_ns;
  const auto start = chrono::high_resolution_clock::now();

  const auto timecond = [=]() -> bool {
    return timeout_wall_clock_ns > 0 ? chrono::duration_cast<chrono::nanoseconds>(
                                           chrono::high_resolution_clock::now() - start)
                                               .count() < timeout_wall_clock_ns
                                     : true;
  };

  const auto timestepcond = [=]() -> bool {
    return (timeout_simulation_ns > 0
                ? timeout_simulation_ns >
                      Simulator::instance().spring_mass->GetSimulationTime()
                : true);
  };

  while (is_updating && timecond() && timestepcond()) {
    Step();

    const bool print_progress = false;

    if (print_progress) {
      if (simulation_time % 1000 == 0) {
        cout << (release_at_ns - simulation_time) << "\n";
      }
    }

    Telemetry::TotalEnergyTrace::instance().update();

    if (Simulator::instance().scenario == SCENARIO::BEAM_TEST && !released &&
        simulation_time == release_at_ns) {
      Simulator::instance().released = true;
      this->released = true;

      this->release();

      cout << "Release damping at: " << simulation_time / 1e6 << "ms" << endl;
      cout << "Release point position: " << Analysis::GetVolumeTrace().y << endl;

      // Restore damping
      damping = 1.0;
      this->integration_method->set_damping(damping);

      measure_freq_from = simulation_time;
    }

    if (Simulator::instance().scenario == SCENARIO::BEAM_TEST && released) {
      if (!Simulator::instance().gpu_acceleration) {
        Telemetry::PositionTrace::instance().update();
      }
    }
  }
  // for (int i = 0; i < optimizers.size(); i++) {
  //   auto opt               = std::get<0>(optimizers[i]);
  //   const auto interval_ns = std::get<1>(optimizers[i]);
  //   if (simulation_time % interval_ns < time_step_ns) {
  //     std::cout << "Running Optimizers" << std::endl;
  //     opt->Optimize();
  //     if (!opt->Enabled()) {
  //       RemoveTopologyOptimizer(i);
  //     }
  //   }
  // }

  if (Simulator::instance().scenario == SCENARIO::BEAM_TEST) {
    PostUpdate();
  }
}

unsigned long long SpringMass::GetSimulationTime() {
  if (Simulator::instance().scenario == SCENARIO::BEAM_TEST) {
    // Don't count time until the beam has been released
    // A hacky way to handle setting up the loadcase before we
    // release damping and allow sim to continue.
    if (released) {
      const auto time = (simulation_time - release_at_ns);
      return time;
    } else {
      return 0;
    }
  } else {
    const auto time = (simulation_time);
    return time;
  }
}

double SpringMass::GetSimulationTimeSeconds() {
  return static_cast<double>(GetSimulationTime() / 1e9);
}

double SpringMass::GetSimulationTimeMS() {
  return static_cast<double>(GetSimulationTime() / 1e6);
}