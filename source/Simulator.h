#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <limits>
#include <memory>
#include <string>
#include <thread>
#include <unordered_set>
#include "Analysis.h"
#include "DML/Parser.h"
#include "Geometry.h"
#include "Mass.h"
#include "Spring.h"
#include "SpringMass.h"
#include "Utils.h"
#include "Volume.h"

#define VERSION "0.0.1"s

enum SCENARIO { BEAM_TEST, PERFORMANCE };

class Simulator {
 public:
  void cleanup();
  void Reset();
  void DumpSpringData();

  void LogPerformance(double time_step,
                      int volume_width,
                      int volume_height,
                      int volume_depth,
                      int bars,
                      float ms_per_frame,
                      float fps);
  bool FileExists(std::string& name);
  void FlattenMassGrid(
      std::vector<std::vector<std::vector<std::shared_ptr<Mass>>>>& m_grid,
      std::shared_ptr<std::unordered_set<std::shared_ptr<Mass>>> output_array);
  int Flatten3dCoordinate(dvec3 cord);
  void BuildCube();
  void BuildLattice();
  void BuildRandomLattice(int num_masses, dvec3 min_bound, dvec3 max_bound);
  void CullLattice();
  void AttachSprings();
  void AttachSpring(dvec3 current_mass_pos, dvec3 next_mass_pos);
  void ConfigureFromDML(DML::Config dml_config);
  void CreateVolumesFromDML(std::vector<DML::Volume>, std::string);
  void AssignForcesToVolumes();
  void AssignMassesToVolumes();
  void setEnableUpdateLoop(bool status);
  void enableGPU();

  DML::Config dml_config;

  std::thread* physics_thread;

  // setup for timer
  int frameCount = 0;

  int volumeScalar;

  // The directory used to output the data related to a run.
  boost::filesystem::path output_dir;

  std::map<std::string, std::shared_ptr<Volume>> volumes;
  double volumeX;
  double volumeY;
  double volumeZ;
  int numMasses;
  double massOfEachMass;
  double modulusElasticity;
  bool hide_window = true;
  double springCrossSectionArea;
  bool gpu_acceleration = false;
  int gpu_device_id = 0;

  // tmp state for testing
  bool released = false;

  dvec3 gravity = dvec3(0, 0, 0);

  double density = 0.0;
  double pointSize = 0.0;  // size of color-coded mass points

  double cubeX;
  double cubeY;
  double cubeZ;

  double bardiam;
  int fixedWidth = 1;

  // the volume which gets filled with the lattice
  std::shared_ptr<Volume> lattice_volume;

  int masses_per_col;
  int masses_per_row;
  int masses_per_slice;

  // Settings
  bool is_logging_performance = false;
  bool is_cpu_only = true;

  // determine timestep based on stiffness.  seems to work.
  // const double time_step = 0.00012649;
  double max_stiffness = 0;                                   // for computing time_step
  double min_stiffness = std::numeric_limits<double>::max();  // for computing time_step
  double time_step;

  double max_spring_force = 0.0;
  double damping = 1.0;

  int scenario = SCENARIO::BEAM_TEST;

  // Define global data structs
  std::shared_ptr<std::unordered_set<std::shared_ptr<Spring>>> tmp_springs =
      std::make_shared<std::unordered_set<std::shared_ptr<Spring>>>();
  std::shared_ptr<std::unordered_set<std::shared_ptr<Mass>>> tmp_masses =
      std::make_shared<std::unordered_set<std::shared_ptr<Mass>>>();

  std::shared_ptr<std::vector<std::shared_ptr<Spring>>> springs =
      std::make_shared<std::vector<std::shared_ptr<Spring>>>();
  std::shared_ptr<std::vector<std::shared_ptr<Mass>>> masses =
      std::make_shared<std::vector<std::shared_ptr<Mass>>>();
  std::vector<std::vector<std::vector<std::shared_ptr<Mass>>>> m_grid;
  std::vector<Vertex> mass_buffer;
  std::vector<Vertex> spring_buffer;

  std::unique_ptr<SpringMass> spring_mass;

  // Use singleton design pattern for now
  // In the future, we can look into
  // dependency injection
  static auto& instance() {
    static Simulator simulator;
    return simulator;
  }

  ~Simulator() = default;

 private:
  Simulator() = default;
  enum class LatticeTypes { Full, Reduced, RandomFill };
};
