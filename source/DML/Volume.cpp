#include "Volume.h"
#include <pugixml.hpp>
#include <set>
#include <stdexcept>

using namespace std;

namespace DML {
Volume::Volume() {}

Volume::~Volume() {}

bool Volume::setPrimitive(string primitive) {
  this->primitive = primitive;
  return true;
}

bool Volume::setID(string id) {
  this->id = id;
  return true;
}

bool Volume::setScale(glm::vec3 scale) {
  this->scale = scale;
  return true;
}

bool Volume::setColor(glm::vec3 color) {
  this->color = color;
  return true;
}

bool Volume::setTranslate(glm::vec3 translate) {
  this->translate = translate;
  return true;
}

bool Volume::setRotate(glm::vec3 rotate) {
  this->rotate = rotate;
  return true;
}

bool Volume::setSTLPath(string stl_path) {
  this->stl_path = stl_path;
  return true;
}

bool Volume::setMoveTo(string move_to) {
  const static std::set<std::string> move_to_opts = {"origin", "centroid", "none"};
  if (move_to_opts.count(move_to)) {
    this->move_to = move_to;
  } else {
    throw std::invalid_argument("Unrecognized Volume move_to value: " + move_to);
  }
  return true;
}

string Volume::GetID() {
  return this->id;
}

glm::vec3 Volume::Translate() {
  return this->translate;
}

glm::vec3 Volume::Scale() {
  return this->scale;
}

glm::vec3 Volume::Color() {
  return this->color;
}

glm::vec3 Volume::Rotate() {
  return this->rotate;
}

string Volume::STLPath() {
  return stl_path;
}

string Volume::Primitive() {
  return primitive;
}

string Volume::MoveTo() {
  return move_to;
}
}  // namespace DML
