#include "Simulator.h"
#include <thread>
#include "ArgParser.h"
#include "CPUSpringMass.h"
#include "Telemetry.h"
#include "TopologyOptimization.h"

#ifdef CUDA
#include "GPUSpringMass.h"
#endif

// remove me after debugging
#include <glm/ext.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/random.hpp>
#include <tuple>

using namespace std;
using namespace glm;

auto update_count = 0;

using Point = dvec3;

const int iterations = 100;
const int g_points_to_connect = 20;

vector<Point> points;

template <typename T, typename A>
size_t arg_max(std::vector<T, A> const& vec) {
  return std::distance(vec.begin(), max_element(vec.begin(), vec.end()));
}

template <typename T, typename A>
size_t arg_min(std::vector<T, A> const& vec) {
  return std::distance(vec.begin(), min_element(vec.begin(), vec.end()));
}

template <typename T, typename A>
vector<size_t> arg_sort(std::vector<T, A> const& vec) {
  vector<size_t> idx(vec.size());
  iota(idx.begin(), idx.end(), 0);

  sort(
      idx.begin(), idx.end(), [&vec](size_t i1, size_t i2) { return vec[i1] < vec[i2]; });

  return idx;
}

template <typename T, typename A>
std::vector<T, A> order_by_indices(std::vector<T, A> const& vec,
                                   std::vector<size_t> indices) {
  // initialize original index locations
  std::vector<T, A> result(vec.size());

  for (size_t i = 0; i < result.size(); i++) {
    result[i] = vec[indices[i]];
  }

  return result;
}

vector<Point> generate_candidates(int num_candidates, Point min_bound, Point max_bound) {
  vector<Point> candidates;
  for (int i = 0; i < num_candidates; i++) {
    Point rand_vec = linearRand(min_bound, max_bound);
    candidates.push_back(rand_vec);
  }
  return candidates;
}

// returns distance, index pair
std::tuple<vector<double>, vector<size_t>> nearest_neighbor_query(Point point, int k) {
  vector<double> distances;
  vector<size_t> indices;

  for (int i = 0; i < points.size(); i++) {
    auto a = points[i];

    double norm = 0;
    if (a == point) {
      norm = 999;
    } else {
      norm = glm::l2Norm(a - point);
    }

    distances.push_back(norm);
    indices.push_back(i);
  };

  auto distances_indices = arg_sort(distances);
  const auto sorted_distances = order_by_indices(distances, distances_indices);
  const auto sorted_indices = order_by_indices(indices, distances_indices);

  auto k_distances =
      vector<double>(sorted_distances.begin(), sorted_distances.begin() + k);
  auto k_incides = vector<size_t>(sorted_indices.begin(), sorted_indices.begin() + k);

  return std::tie(k_distances, k_incides);
}

vector<double> find_nearest_neighbor(vector<Point> candidates) {
  vector<double> distances;
  for (auto p : candidates) {
    auto [_distances, indices] = nearest_neighbor_query(p, 1);
    distances.push_back(_distances[0]);
  }
  return distances;
}

void Simulator::Reset() {}

void Simulator::BuildRandomLattice(int num_masses, dvec3 min_bound, dvec3 max_bound) {
  // replace with bbox bounds.
  for (int i = 0; i < num_masses; i++) {
    if (points.size() > 0) {
      int num_candidates = iterations;

      // if (points.size() < iterations) {
      //   num_candidates = static_cast<int>(points.size());
      // } else {
      //   num_candidates = iterations;
      // }

      auto candidates = generate_candidates(num_candidates, min_bound, max_bound);

      auto distances = find_nearest_neighbor(candidates);
      auto max_idx = arg_max(distances);
      points.push_back(candidates[max_idx]);

    } else {
      auto p = linearRand(min_bound, max_bound);
      points.push_back(p);
    }
  }

  auto [_distances, indices] =
      nearest_neighbor_query(points[0], static_cast<int>(points.size()));
  points = order_by_indices(points, indices);

  for (const auto& p : points) {
    const auto mass = make_shared<Mass>(massOfEachMass, p);
    masses->push_back(mass);
  }

  // Spring connections
  int spring_connect_iterations = 1;
  for (int k = spring_connect_iterations; k > 0; k--) {
    for (int i = 0; i < masses->size(); i++) {
      auto p = points[i];
      // auto [_distances, indices] = nearest_neighbor_query(p,
      // static_cast<int>(masses->size()));

      // at worst case, all springs will already be connected, so we
      // we need to fetch more neighbors in the event we have to skip some.
      auto [_distances, indices] = nearest_neighbor_query(p, 2 * g_points_to_connect);

      for (auto j : indices) {
        auto p2 = points[j];
        auto currentMass = masses->at(i);
        auto nextMass = masses->at(j);

        if (currentMass->connected_springs.size() >= g_points_to_connect / k)
          break;

        if (nextMass->connected_springs.size() >= g_points_to_connect / k)
          continue;

        // A really poor way to check, but it works for now.
        bool has_duplicate = false;

        for (auto s : *springs) {
          if ((s->mass1 == currentMass || s->mass2 == currentMass) &&
              (s->mass1 == nextMass || s->mass2 == nextMass)) {
            has_duplicate = true;
            break;
          }
        }

        if (has_duplicate) {
          continue;
        }

        if (currentMass == nullptr || nextMass == nullptr) {
          cout << "Error attaching springs" << endl;
          assert(false);
        }

        double restLength = glm::abs(
            glm::length(currentMass->originalPosition - nextMass->originalPosition));
        double stiffness = (modulusElasticity * springCrossSectionArea) / restLength;
        double spring_mass = springCrossSectionArea * restLength * density;
        currentMass->mass += (spring_mass / 2);
        nextMass->mass += (spring_mass / 2);

        if (stiffness > max_stiffness) {
          max_stiffness = stiffness;
        }

        if (stiffness < min_stiffness) {
          min_stiffness = stiffness;
        }

        auto spring = make_shared<Spring>(currentMass, nextMass, restLength, stiffness);

        springs->push_back(spring);

        currentMass->connected_springs.push_back(spring);
        nextMass->connected_springs.push_back(spring);
      }
    }
  }
}

void Simulator::BuildLattice() {
  // TODO
  Bbox bbox = Geometry::PolyhedronBBOX(lattice_volume->polyhedron);
  Geometry::printBBOX(bbox, "cantilever");
  int x_idx = 0;
  int y_idx = 0;
  int z_idx = 0;
  masses_per_row = (bbox.xmax() - bbox.xmin()) / cubeX;
  masses_per_col = (bbox.ymax() - bbox.ymin()) / cubeY;
  masses_per_slice = (bbox.zmax() - bbox.zmin()) / cubeZ;
  masses_per_row++;
  masses_per_col++;
  masses_per_slice++;
  m_grid.resize(masses_per_row);
  for (double x = bbox.xmin(); x <= bbox.xmax(); x += cubeX) {
    m_grid[x_idx].resize(masses_per_col);
    for (double y = bbox.ymin(); y <= bbox.ymax(); y += cubeY) {
      m_grid[x_idx][y_idx].resize(masses_per_slice);
      for (double z = bbox.zmin(); z <= bbox.zmax(); z += cubeZ) {
        // if (Geometry::IsPointInside(Point3(x, y, z), volume->polyhedron,
        // volume->aabb_tree)) {
        auto posVec = glm::vec3(x, y, z);

        // TODO 1: noise feature. hardcoded for now
        // auto noise = glm::sphericalRand(cubeX / 4);
        // posVec += noise;

        const auto mass = make_shared<Mass>(massOfEachMass, posVec);
        m_grid[x_idx][y_idx][z_idx] = mass;
        //}
        z_idx++;
      }
      z_idx = 0;
      y_idx++;
    }
    y_idx = 0;
    x_idx++;
  }
  x_idx = 0;
}

void Simulator::CullLattice() {
  // Note: probably need to implement DML Parsing for this parameter
  // if distance between mass and surface is smaller than this
  // when filling with lattice, go ahead and move the mass to the surface.
  auto _avg_cube_dims = (cubeX + cubeY + cubeZ) / 3;
  auto max_distance_to_surface_for_move = (_avg_cube_dims / 2) * (_avg_cube_dims / 2);

  unordered_set<shared_ptr<Mass>> masses_to_remove;
  for (auto m : *tmp_masses) {
    auto query_point = Point3(m->position.x, m->position.y, m->position.z);
    if (Geometry::IsPointInside(query_point, lattice_volume->aabb_tree)) {
      continue;
    }
    if (Geometry::SquaredDistanceToSurface(query_point, lattice_volume->aabb_tree) <=
        max_distance_to_surface_for_move) {
      // move to new position
      auto new_point =
          Geometry::ClosestPointOnSurface(query_point, lattice_volume->aabb_tree);
      m->position.x = new_point.x();
      m->position.y = new_point.y();
      m->position.z = new_point.z();
      m->UpdateSprings(modulusElasticity, springCrossSectionArea);
    } else {
      // remove
      unordered_set<shared_ptr<Spring>> springs_to_remove;
      for (const auto s : m->connected_springs) {
        springs_to_remove.insert(s);
      }

      for (const auto& s : springs_to_remove) {
        s->mass1->connected_springs.erase(remove(s->mass1->connected_springs.begin(),
                                                 s->mass1->connected_springs.end(),
                                                 s),
                                          s->mass1->connected_springs.end());
        s->mass2->connected_springs.erase(remove(s->mass2->connected_springs.begin(),
                                                 s->mass2->connected_springs.end(),
                                                 s),
                                          s->mass2->connected_springs.end());
        tmp_springs->erase(s);
      }

      masses_to_remove.insert(m);
    }
  }

  for (const auto m : masses_to_remove) {
    tmp_masses->erase(m);
  }
}

// transform 3d array index into a 1d array index
int Simulator::Flatten3dCoordinate(dvec3 cord) {
  return cord.z * masses_per_row * masses_per_col + cord.y * masses_per_row + cord.x;
}

void Simulator::AttachSpring(dvec3 current_mass_pos, dvec3 next_mass_pos) {
  auto currentMass = m_grid[current_mass_pos.x][current_mass_pos.y][current_mass_pos.z];
  auto nextMass = m_grid[next_mass_pos.x][next_mass_pos.y][next_mass_pos.z];

  if (currentMass == nullptr || nextMass == nullptr) {
    cout << "Error attaching springs" << endl;
    assert(false);
  }

  double restLength =
      glm::abs(glm::length(currentMass->originalPosition - nextMass->originalPosition));
  double stiffness = (modulusElasticity * springCrossSectionArea) / restLength;
  double spring_mass = springCrossSectionArea * restLength * density;
  currentMass->mass += spring_mass / 2;
  nextMass->mass += spring_mass / 2;

  if (stiffness > max_stiffness) {
    max_stiffness = stiffness;
  }

  if (stiffness < min_stiffness) {
    min_stiffness = stiffness;
  }

  auto spring = make_shared<Spring>(currentMass, nextMass, restLength, stiffness);

  tmp_springs->emplace(spring);

  currentMass->connected_springs.push_back(spring);
  nextMass->connected_springs.push_back(spring);
}

void Simulator::AttachSprings() {
  // helpful for visual debugging
  auto lattice_type = LatticeTypes::Full;

  bool showXYPlaneSprings = true;
  bool showZXPlaneSprings = true;
  bool showZYPlaneSprings = true;
  bool showLongDiagonals = true;
  bool showDiagonals = true;

  if (lattice_type == LatticeTypes::Reduced) {
    showLongDiagonals = false;
  }

  for (int x = 0; x < masses_per_row; x++) {
    for (int y = 0; y < masses_per_col; y++) {
      for (int z = 0; z < masses_per_slice; z++) {
        // these if statements have a lot of verbosity on puprose
        // being more explicit and verbose makes it easier to read IMO
        const dvec3 currentMassIdx = dvec3(x, y, z);
        dvec3 nextMassIdx;
        if (showXYPlaneSprings || showZXPlaneSprings) {
          // [horizontal] link right
          if (x < masses_per_row - 1) {
            nextMassIdx = dvec3(x + 1, y, z);
            AttachSpring(currentMassIdx, nextMassIdx);
          }
        }

        if (showZYPlaneSprings || showZXPlaneSprings) {
          // link back
          if (z < masses_per_slice - 1) {
            const auto nextMassPos = dvec3(x, y, z + 1);
            AttachSpring(currentMassIdx, nextMassPos);
          }
        }

        if (showXYPlaneSprings || showZYPlaneSprings) {
          // [vertical] link up
          if (y < masses_per_col - 1) {
            const dvec3 nextMassPos = dvec3(x, y + 1, z);
            AttachSpring(currentMassIdx, nextMassPos);
          }
        }

        // if (showXYPlaneSprings) {
        // [diagonal][xy-plane] link front face up to right
        if (showDiagonals && y < masses_per_col - 1 && x < masses_per_row - 1) {
          nextMassIdx = dvec3(x + 1, y + 1, z);
          AttachSpring(currentMassIdx, nextMassIdx);
        }

        // [diagonal][xy-plane] link front face up to left
        if (showDiagonals && x > 0 && y < masses_per_col - 1) {
          nextMassIdx = dvec3(x - 1, y + 1, z);
          AttachSpring(currentMassIdx, nextMassIdx);
        }
        //}

        // if (showZXPlaneSprings) {
        // [diagonal][zx-plane] link top and bottom faces diagonal
        if (showDiagonals && z < masses_per_slice - 1 && x < masses_per_row - 1) {
          nextMassIdx = dvec3(x + 1, y, z + 1);
          AttachSpring(currentMassIdx, nextMassIdx);
        }

        // [diagonal][zx-plane] link top and bottom faces diagonal
        if (showDiagonals && x > 0 && z < masses_per_slice - 1) {
          nextMassIdx = dvec3(x - 1, y, z + 1);
          AttachSpring(currentMassIdx, nextMassIdx);
        }
        //}

        // if (showZYPlaneSprings) {
        // [diagonal][zy-plane] link side faces
        if (showDiagonals && y > 0 && z < masses_per_slice - 1) {
          nextMassIdx = dvec3(x, y - 1, z + 1);
          AttachSpring(currentMassIdx, nextMassIdx);
        }

        // [diagonal][zy-plane] link side faces
        if (showDiagonals && z < masses_per_slice - 1 && y < masses_per_col - 1) {
          nextMassIdx = dvec3(x, y + 1, z + 1);
          AttachSpring(currentMassIdx, nextMassIdx);
        }
        //}

        if (showLongDiagonals) {
          // [long-diagonal] link top and bottom faces diagonal
          if (z > 0 && x < masses_per_row - 1 && y > 0) {
            nextMassIdx = dvec3(x + 1, y - 1, z - 1);
            AttachSpring(currentMassIdx, nextMassIdx);
          }

          // [long-diagonal] link top and bottom faces diagonal
          if (z > 0 && x > 0 && y > 0) {
            nextMassIdx = dvec3(x - 1, y - 1, z - 1);
            AttachSpring(currentMassIdx, nextMassIdx);
          }

          // [long-diagonal] link top and bottom faces diagonal
          if (z < masses_per_slice - 1 && x < masses_per_row - 1 && y > 0) {
            nextMassIdx = dvec3(x + 1, y - 1, z + 1);
            AttachSpring(currentMassIdx, nextMassIdx);
          }

          // [long-diagonal] link top and bottom faces diagonal
          if (z < masses_per_slice - 1 && x > 0 && y > 0) {
            nextMassIdx = dvec3(x - 1, y - 1, z + 1);
            AttachSpring(currentMassIdx, nextMassIdx);
          }
        }
      }
    }
  }
}

// flatten the 3d mass grid into a 1d array
// update this function definition, it no longer
// uses the passed in m_grid
void Simulator::FlattenMassGrid(
    vector<vector<vector<shared_ptr<Mass>>>>& m_grid,
    shared_ptr<unordered_set<shared_ptr<Mass>>> output_array) {
  for (int z = 0; z < masses_per_slice; z++) {
    for (int y = 0; y < masses_per_col; y++) {
      for (int x = 0; x < masses_per_row; x++) {
        const auto m = this->m_grid[x][y][z];
        if (m != nullptr)
          output_array->insert(m);
      }
    }
  }
}

void Simulator::cleanup() {
  // delete physics_thread;
  // DumpSpringData();  // TODO 2: hack.  move to telemetry.  cleanup only
  // called by MainWindow, which should also change...
  // if (ArgParser::positionTraceFilepath.Get() != "")
  //  Telemetry::position_trace.close();
  // if (ArgParser::totalEnergyTraceFilepath.Get() != "")
  //  Telemetry::total_energy_trace.close();
}

void Simulator::DumpSpringData() {
  auto filename = "spring_data.csv";
  vector<double> data;
  for (auto spring : *springs) {
    auto m1 = spring->mass1;
    auto m2 = spring->mass2;
    data.push_back(m1->position.x);
    data.push_back(m1->position.y);
    data.push_back(m1->position.z);
    data.push_back(m2->position.x);
    data.push_back(m2->position.y);
    data.push_back(m2->position.z);
    data.push_back(spring->maxForce);
    data.push_back(bardiam);
  }

  vector<string> headers = {"mass1_position_x",
                            "mass1_position_y",
                            "mass1_position_z",
                            "mass2_position_x",
                            "mass2_position_y",
                            "mass2_position_z",
                            "spring_max_force",
                            "bar_diameter"};
  ofstream csvOut(filename, ios::out);

  if (csvOut.is_open()) {
    for (auto h : headers) {
      csvOut << h << ", ";
    }
    csvOut << endl;
    for (int i = 0; i < data.size(); i += headers.size()) {
      for (int j = 0; j < headers.size(); j++) {
        csvOut << data[i + j] << ", ";
      }
      csvOut << endl;
    }
  }

  cout << "Wrote spring data to " << filename << endl;

  csvOut.close();
}

bool started_thread = false;

void Simulator::setEnableUpdateLoop(bool status) {
  if (status) {
    spring_mass->enableUpdating();
    if (!started_thread) {
      physics_thread = new thread([&]() { spring_mass->PerformUpdate(); });
      physics_thread->detach();
      started_thread = true;
    }
  } else {
    spring_mass->disableUpdating();
  }
}

void Simulator::LogPerformance(double time_step,
                               int volume_width,
                               int volume_height,
                               int volume_depth,
                               int bars,
                               float ms_per_frame,
                               float fps) {
  ofstream csvOut("performance.csv", ios::out | ios::app);

  if (csvOut.is_open()) {
    csvOut << time_step << ", ";
    csvOut << volume_width << ", ";
    csvOut << volume_height << ", ";
    csvOut << volume_depth << ", ";
    csvOut << bars << ", ";
    csvOut << ms_per_frame << ", ";
    csvOut << fps << ", ";
    csvOut << endl;
  }

  csvOut.close();
}

bool Simulator::FileExists(string& name) {
  std::ifstream f(name.c_str());
  return f.good();
}

// Create the volumes
void Simulator::CreateVolumesFromDML(vector<DML::Volume> dml_volumes,
                                     string volume_to_fill_id) {
  for (auto dml_volume : dml_volumes) {
    volumes[dml_volume.GetID()] = make_shared<Volume>(dml_volume);
    if (dml_volume.GetID() == volume_to_fill_id) {
      lattice_volume = volumes[dml_volume.GetID()];
    }
  }
}

void Simulator::AssignForcesToVolumes() {
  for (auto lc : *(dml_config.GetLoadcases())) {
    for (const auto& prop : lc.Properties()) {
      auto vol = volumes.at(prop.volume->GetID());
      vol->AddForce(prop.type, prop.params);
    }
  }
}

void Simulator::AssignMassesToVolumes() {
  // Cover the rest of the volumes without a connection to loadcase.
  // Namely trace for now...
  // another quick hack to get centroids working...
  // FIXME -- order is important here, since the way
  // that the acceleration gets set in the AddMass method
  // this needs to be fixed asap
  // these masses have to be added before we perform the section below
  auto vol_trace = volumes["trace"];
  for (const auto& m : *masses) {
    if (Geometry::IsPointOutside(Point3(m->position.x, m->position.y, m->position.z),
                                 vol_trace->aabb_tree)) {
      continue;
    }
    vol_trace->AddMass(m);
  }

  for (auto lc : *(dml_config.GetLoadcases())) {
    for (auto p : lc.Properties()) {
      if (volumes[p.volume->GetID()]) {
        auto vol = volumes[p.volume->GetID()];
        if (vol->id == lattice_volume->id) {
          // don't bother computing intersection for the volume we fill the
          // lattice with since we know every mass is contained within.
          for (const auto& m : *masses) {
            vol->AddMass(m);
          }
          continue;
        }
        for (const auto& m : *masses) {
          if (Geometry::IsPointOutside(
                  Point3(m->position.x, m->position.y, m->position.z), vol->aabb_tree)) {
            continue;
          }
          vol->AddMass(m);
        }
      }
    }
  }
}

// TODO(jclay): remove me
// void Simulator::ApplyVolumeProperties() {
//   for (auto key : volumes) {
//     auto volume = key.second;
//     if (volume->loadcase_property == nullptr) {
//       continue;
//     }

//     // TODO: refactor these
//     auto loadcase_type = volume->loadcase_property->type;
//     // if (loadcase_type == "force") {
//       // auto force_per_mass = dvec3(0, 0, 0);
//       // force_per_mass += volume->loadcase_property->Magnitude();
//       // force_per_mass /= volume->masses_inside.size();
//       // for (auto m : volume->masses_inside) {
//         // m->externalForce += force_per_mass;
//         // m->underExternalForce = true;
//       // }
//     // } else if(loadcase_type == "fix") {
//     if(loadcase_type == "fix") {
//       // since we don't know which volume will be reached first,
//       // we remove any acceleration that may have been added
//       // and we also have a guard clause in accel below.
//       for (auto m : volume->masses_inside) {
//         m->fixed = true;
//         m->appliedAcceleration = dvec3(0, 0, 0);
//       }
//     }
//     // else if (loadcase_type == "accel") {
//       // for (auto m : *masses) {
//         // if (m->fixed)
//           // continue;
//         // m->appliedAcceleration = volume->loadcase_property->Magnitude();
//       // }
//     // }
//   }
// }

void Simulator::enableGPU() {
  // TODO(jclay) Remove me
  // GPUSpringMass * gpm = new GPUSpringMass();
  // gpm->Initialize(masses, springs,
  // dml_config.GetRule()->GetIntegration().method, time_step, damping, true);
  // spring_mass = make_unique<GPUSpringMass>(); spring_mass->Initialize(masses,
  // springs, dml_config.GetRule()->GetIntegration().method, time_step, damping,
  // true);
}

void Simulator::ConfigureFromDML(DML::Config dml_config) {
  // srand(time(NULL));

  volumeScalar = 1;

  this->dml_config = dml_config;
  auto rule = dml_config.GetRule();
  auto damping_amount = rule->GetDamping().amount;
  auto material = dml_config.GetMaterial();
  auto lattice = material->GetLattice();
  bardiam = lattice->bardiam;  // TODO(jclay) 2: used only for DumpSpringData
  springCrossSectionArea = glm::pi<double>() * glm::pow(lattice->bardiam / 2.0, 2);

  cubeX = lattice->size.x;
  cubeY = lattice->size.y;
  cubeZ = lattice->size.z;

  density = 4430.0;
  massOfEachMass = 0.0;  // lattice->vertexmass;
  // TODO: respect units
  modulusElasticity = material->elasticitymodulus * (1E9);  // convert to Pa from GPA

  auto volume_to_fill = lattice->volume->GetID();

  damping = 1.0 - (damping_amount / 100);

  CreateVolumesFromDML(*dml_config.GetVolumes(), volume_to_fill);
  cout << "Lattice is random: " << lattice->is_random << endl;

  if (lattice->is_random) {
    cout << "Building random lattice" << endl;
    Bbox bbox = Geometry::PolyhedronBBOX(lattice_volume->polyhedron);
    dvec3 min_bound(bbox.xmin(), bbox.ymin(), bbox.zmin());
    dvec3 max_bound(bbox.xmax(), bbox.ymax(), bbox.zmax());

    auto masses_per_row = (bbox.xmax() - bbox.xmin()) / cubeX;
    auto masses_per_col = (bbox.ymax() - bbox.ymin()) / cubeY;
    auto masses_per_slice = (bbox.zmax() - bbox.zmin()) / cubeZ;

    // We use the number of masses that would have been used by the grid to
    // determine the number of masses we are randomly generating.
    int num_masses = (masses_per_col + 1) * (masses_per_row + 1) * (masses_per_slice + 1);

    BuildRandomLattice(num_masses, min_bound, max_bound);
    CullLattice();
  } else {
    cout << "Building grid lattice" << endl;
    BuildLattice();
    AttachSprings();
    FlattenMassGrid(m_grid, tmp_masses);
    // CullLattice();

    // copy tmp springs to real springs for faster processing
    copy(tmp_springs->begin(), tmp_springs->end(), back_inserter(*springs));
    copy(tmp_masses->begin(), tmp_masses->end(), back_inserter(*masses));
  }

  mass_buffer.resize(masses->size());
  spring_buffer.resize(2 * springs->size());

  if (rule->GetIntegration().time_step > 0.0) {
    time_step = rule->GetIntegration().time_step;
  } else {
    time_step = 1e-6;
  }

  // time_step = 0.0000005;
  // time_step = 0.999 / (2 * 3.14159 * glm::sqrt(max_stiffness /
  // massOfEachMass));
  AssignForcesToVolumes();
  AssignMassesToVolumes();

  auto rule_output_dir = rule->GetTelemetry().output_dir;

  if (rule_output_dir.empty()) {
    cout << "Output dir is empty, using a unique output dir: " << endl;
    output_dir = Utils::GetUniqueOutputDir();
  } else {
    output_dir = boost::filesystem::path{rule_output_dir};
  }

  cout << "Output dir: " << output_dir.string() << endl;

  // Setup telemetry
  Telemetry::PositionTrace::instance().data_source = &Analysis::GetVolumeTrace;
  Telemetry::PositionTrace::instance().telemetry_dir =
      Simulator::instance().output_dir / "telemetry";
  Telemetry::PositionTrace::instance().open("position_trace.csv");

  Telemetry::TotalEnergyTrace::instance().telemetry_dir =
      Simulator::instance().output_dir / "telemetry";
  Telemetry::TotalEnergyTrace::instance().open("total_energy.csv");

  Telemetry::ConnectedSprings::instance().telemetry_dir =
      Simulator::instance().output_dir / "telemetry";
  Telemetry::ConnectedSprings::instance().open("connected_springs.csv");

  vector<shared_ptr<Volume>> vols_vec;

  for (const auto& vol : volumes) {
    vols_vec.push_back(vol.second);
  }

  for (const auto m : *masses) {
    m->originalPosition = m->position;
  }

  vector<vec3> original_positions;
  for (const auto m : *masses) {
    original_positions.push_back(m->originalPosition);
  }

  auto it = std::unique(original_positions.begin(), original_positions.end());
  bool wasUnique = (it == original_positions.end());
  if (wasUnique) {
    cout << "All masses were unique" << endl;
  } else {
    cout << "Duplicate masses found" << endl;
    exit(0);
  }

  {
    Bbox bbox = Geometry::PolyhedronBBOX(lattice_volume->polyhedron);
    auto masses_per_row = (bbox.xmax() - bbox.xmin()) / cubeX;
    auto masses_per_col = (bbox.ymax() - bbox.ymin()) / cubeY;
    auto masses_per_slice = (bbox.zmax() - bbox.zmin()) / cubeZ;

    auto num_masses =
        (masses_per_col + 1) * (masses_per_row + 1) * (masses_per_slice + 1);

    cout << "Calculated number of masses: " << num_masses << endl;

    auto min_len = numeric_limits<double>::max();
    auto max_len = numeric_limits<double>::min();
    auto min_stiffness = numeric_limits<double>::max();
    auto max_stiffness = numeric_limits<double>::min();
    auto min_connections = numeric_limits<double>::max();
    auto max_connections = numeric_limits<double>::min();
    auto total_connections = 0.0;
    auto total_stiffness = 0.0;

    for (const auto s : *springs) {
      if (s->restLength > max_len) {
        max_len = s->restLength;
      }
      if (s->restLength < min_len) {
        min_len = s->restLength;
      }
      if (s->stiffness > max_stiffness) {
        max_stiffness = s->stiffness;
      }
      if (s->stiffness < min_stiffness) {
        min_stiffness = s->stiffness;
      }
      total_stiffness += s->stiffness;
    }

    for (const auto m : *masses) {
      if (m->connected_springs.size() > max_connections) {
        max_connections = m->connected_springs.size();
      }

      if (m->connected_springs.size() < min_connections) {
        min_connections = m->connected_springs.size();
      }

      total_connections += m->connected_springs.size();
    }

    cout << "Max Spring Connections: " << max_connections << endl;
    cout << "Min Spring Connections: " << min_connections << endl;
    cout << "Average Spring Connections: " << total_connections / masses->size() << endl;

    cout << "Max Spring Length: " << max_len << endl;
    cout << "Min Spring Length: " << min_len << endl;

    cout << "Max Spring Stiffness: " << max_stiffness << endl;
    cout << "Min Spring Stiffness: " << min_stiffness << endl;
    cout << "Average Spring Stiffness: " << total_stiffness / springs->size() << endl;
  }

  cout << masses->size() << " masses" << endl;

  // set masses based on density of ti64
  const double volume_meters = volumes["cantilever"]->scale.x *
                               volumes["cantilever"]->scale.y *
                               volumes["cantilever"]->scale.z;

  // Start method for applying uniform volume across all masses
  // This method equally distributes the total mass across each mass in the
  // volume.

  // cout << "Volume meters: ";
  // cout << volume_meters << endl;
  // const double mass_kg = (density * volume_meters) /
  // static_cast<double>(masses->size()); const double mass_kg = 1000 /
  // static_cast<double>(springs->size());

  // for (const auto s : *springs) {
  //   m->mass = mass_kg * static_cast<double>(m->connected_springs.size());
  // }
  // End method for applying uniform volume across all masses

  // const double force_per_mass = volumes["cantilever"]->scale.z * 200.0;

  // Attempt to calculate the force based on the displacement formula:
  // https://en.wikipedia.org/wiki/Deflection_(engineering)#End-loaded_cantilever_beams
  // note that this value is in meters but we cannot actually expect that the
  // values will actually be realized. this is expected to represent some value
  // that scales proportionally to change in width, length, thickness, etc.

  const double target_displacement = 1e-6;
  const double scale_x = volumes["cantilever"]->scale.x;
  const double scale_z = volumes["cantilever"]->scale.z;
  const double scale_y = volumes["cantilever"]->scale.y;
  // const double I = (scale_z * glm::pow(scale_y, 3)) / 12.0;
  // const double force = (target_displacement * 3 * I * modulusElasticity) /
  // (scale_x, 3);
  // const double force = 100;

  // We overide the more advanced dyanmic forces for now
  // in order to rule out some issues it may have been causing in
  // performance tests

  // restore force to 0.002 in y-direction for testing;

  dvec3 force = dvec3(0, 0, 0);
  for (auto f : volumes["loadpoint"]->forces) {
    force += f->Next(0);
  }

  const dvec3 force_per_mass =
      force / static_cast<double>(volumes["loadpoint"]->masses_inside.size());

  for (const auto mass : volumes["loadpoint"]->masses_inside) {
    mass->externalForce = force_per_mass;
  }

  // total mass for debugging
  double total_mass = 0.0;
  for (const auto m : *masses) {
    total_mass += m->mass;
  }
  // end total mass for debugging

  cout << "initial centroid" << Analysis::GetVolumeTrace().y << endl;

  cout << "Cuda Device ID: " << ArgParser::gpuDeviceId.Get() << endl;

#ifdef CUDA
  if (rule->gpu_acceleration) {
    gpu_acceleration = true;
    spring_mass = make_unique<cuda::GPUSpringMass>();
    cuda::Utils::set_gpu_device(gpu_device_id);
  } else {
    spring_mass = make_unique<CPUSpringMass>();
  }
#else
  spring_mass = make_unique<CPUSpringMass>();
#endif

  // Telemetry::ConnectedSprings::instance().update();

  spring_mass->Initialize(
      masses, springs, rule->GetIntegration().method, time_step, damping, !hide_window);

  if (rule->gpu_acceleration) {
    spring_mass->SetTracedMasses(volumes["trace"]->masses_inside);
  }

  // TODO(jclay): cleanup
  // CUDAEnable
  // CPUSpringMass::Initialize(std::make_unique<vector<shared_ptr<Volume>>>(vols_vec),
  // time_step,
  //                           damping, rule->GetIntegration().method);

  // TODO: dml specification for topology optimization
  // auto drop_springs_update_interval_ns = 1 * 1e6;  // 1e6 ns in a ms
  // auto num_springs_to_remove = static_cast<int>(.03 * springs->size());
  // double stop_at_pct_springs_remaining = .75;
  // CPUSpringMass::AddTopologyOptimizer(
  //     std::make_shared<TopOpt::DropSprings>(TopOpt::DropSprings(
  //         num_springs_to_remove, stop_at_pct_springs_remaining,
  //         masses, springs)), drop_springs_update_interval_ns);
}
