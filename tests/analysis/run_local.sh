#!/usr/bin/env bash

# Run OpenDML tests locally
set -e
set -u

. "$(dirname "$(readlink -f "$0")")"/test_lib.sh

setup_dml_test_script $*

gen_simulation_tests | xargs -P${NUM_CORES:-1} -I% sh -c '%'
if [ ! "${ANALYZE_DATA:-1}" = 0 ] ; then echo analyze_data ; fi
