#pragma once

#include <glm/vec3.hpp>
#include <iostream>
#include <vector>
#include "Spring.h"
#include "Vertex.h"

using glm::dvec3;

class Spring;

class Mass {
 public:
  Mass(double massAmount, dvec3 position);
  ~Mass();

  int id = 0;
  bool fixed = false;
  bool traced;
  bool updating = false;
  double mass = 0.0;
  bool removed = false;
  dvec3 appliedAcceleration = dvec3(0, 0, 0);
  dvec3 currentVelocity = dvec3(0, 0, 0);
  dvec3 externalForce = dvec3(0, 0, 0);
  dvec3 force = dvec3(0, 0, 0);
  dvec3 customColor = dvec3(0, 0, 0);
  dvec3 originalPosition;
  dvec3 position;
  dvec3 lastPosition;
  int update_count = 0;
  std::vector<std::shared_ptr<Spring>> connected_springs;

  Vertex GetVertex();
  void RemoveSpring(std::shared_ptr<Spring> spring);
  dvec3 GetColor();
  void UpdateSprings(double modulusElasticity, double springCrossSectionArea);
  inline bool UnderExternalForce() const { return dvec3(0, 0, 0) != externalForce; }
};
