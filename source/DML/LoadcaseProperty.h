#pragma once
#include "LoadcaseParams.h"
#include "Volume.h"

#include <iostream>
#include <memory>
#include "glm/vec3.hpp"

namespace DML {
struct LoadcaseProperty {
  LoadcaseParams::Types type;
  std::shared_ptr<DML::Volume> volume;
  std::shared_ptr<LoadcaseParams::Params> params;
};
}  // namespace DML
