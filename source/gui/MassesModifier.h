#pragma once

#include "Simulator.h"
#include "Vertex.h"

#include <Qt3DCore/QEntity>
#include <QtCore/QObject>
#include <memory>
#include <vector>

class MassModifier : public QObject {
  Q_OBJECT

 public:
  MassModifier(Qt3DCore::QEntity* rootEntity, Simulator* sim);
  ~MassModifier();
 public slots:
  void SyncMasses();
  void setEnabled(bool enabled);
  void setForcesEnabled(bool enabled);
  bool isEnabled();

 private:
  void updateMassBuffer();
  bool show_forces = false;
  Qt3DCore::QEntity* forces_root;
  Qt3DCore::QEntity* root_entity;
  Qt3DCore::QEntity* mass_root;
  Simulator* sim;
  vector<Vertex> mass_buffer;
  vector<Vertex> force_buffer;
};
