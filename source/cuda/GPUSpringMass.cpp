#include "GPUSpringMass.h"
#include <thrust/host_vector.h>
#include <algorithm>
#include "Telemetry.h"

using namespace std;
using namespace glm;
using namespace cuda;

void GPUSpringMass::Initialize(shared_ptr<vector<shared_ptr<Mass>>> masses,
                               shared_ptr<vector<shared_ptr<Spring>>> springs,
                               string integration_method,
                               double time_step,
                               double damping,
                               bool is_rendering) {
  this->masses = masses;
  this->springs = springs;
  this->damping = damping;
  this->time_step = time_step;
  this->time_step_ns = time_step * 1e9;
  this->is_rendering = is_rendering;

  // Copy masses to host cuMasses;
  // We must sync to device here since there are dependencies
  // between each step.
  Utils::masses_to_cuMasses(this->masses);
  Utils::copy_data_to_device();
  Utils::springs_to_cuSprings(this->masses, this->springs);
  Utils::copy_data_to_device();

  Utils::set_connected_springs();
  Utils::copy_data_to_device();
  Utils::copy_data_to_host();

  // Set integration method
  if (integration_method == "euler") {
    this->integration_method = make_unique<Euler>(time_step, damping, this);
    Utils::set_gpu_integration_method(IntegrationMethods::Euler);
    cout << "Using Euler integration method" << endl;
  } else if (integration_method == "verlet") {
    this->integration_method = make_unique<Verlet>(time_step, damping, this);
    Utils::set_gpu_integration_method(IntegrationMethods::Verlet);
    cout << "Using Verlet integration method" << endl;
  } else if (integration_method == "RK4") {
    this->integration_method = make_unique<RK4>(time_step, damping, this);
    Utils::set_gpu_integration_method(IntegrationMethods::RK4);
    cout << "Using RK4 integration method" << endl;
  } else {
    auto msg = "Integration method " + integration_method + "does not exist";
    throw invalid_argument(msg);
  }

  this->integration_method->initialize();

  cout << "GPU Acceleration Successfully Enabled" << endl;
}

void GPUSpringMass::UpdateTrace() {
  Utils::device_update_trace();
}

void GPUSpringMass::PostUpdate() {
  auto tr = Utils::get_trace_results();
  cout << "tr size: " << tr.size();
  Telemetry::PositionTrace::instance().replay(tr);
  cout << "Writing output" << endl;
  Telemetry::PositionTrace::instance().write();
  // Telemetry::TotalEnergyTrace::instance().write();
  disableUpdating();
  is_done = true;
}

//
// This is a hacked in workaround to alleviate some performance issues on GPU
// during the analysis phase Instead of copying the data back to the CPU on
// every iteration so that we can record the trace and later write it to a file,
// I will instead cache it on the GPU side so that we can simply transfer in
// bulk once simulation is over and then write to disk.
void GPUSpringMass::SetTracedMasses(std::vector<std::shared_ptr<Mass>> trace_masses) {
  vector<int> indices_to_trace;
  for (const auto& t_m : trace_masses) {
    int pos = static_cast<int>(std::distance(
        this->masses->begin(), find(this->masses->begin(), this->masses->end(), t_m)));

    indices_to_trace.push_back(pos);
  }
  Utils::set_masses_to_trace(indices_to_trace);
}

// This can likely go away, but keeping it for API compat for now.
void GPUSpringMass::Step() {
  this->integration_method->step();
  simulation_time += time_step_ns;

#ifndef PERFORMANCE_MODE
  if (Simulator::instance().scenario == SCENARIO::BEAM_TEST && this->released) {
    UpdateTrace();
  }

  if (this->released || is_rendering) {
    Utils::render_update(this->masses);
  }
#endif
}

void GPUSpringMass::release() {
  cout << "GPUSpringMass: Releasing Forces" << endl;
  Utils::release_forces();
}
