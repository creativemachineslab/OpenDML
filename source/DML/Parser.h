#pragma once

#include <memory>
#include "Config.h"
#include "LoadcaseProperty.h"

namespace DML {
namespace Parser {
void ApplyIndentLevel(int level);
void PrintAttribute(pugi::xml_attribute attr, int level);
size_t NumberChildren(pugi::xml_node node);
void ValidateDML(pugi::xml_node root);
void PrintDMLDebug(std::initializer_list<char const*> expectedNodes, pugi::xml_node root);
std::vector<double> VectorFromString(std::string str, size_t expectedLength);
glm::vec3 vec3FromString(std::string str, size_t expectedLength);
glm::dvec3 dvec3FromString(std::string str, size_t expectedLength);
bool ParseVolume(DML::Volume* vol,
                 pugi::xml_object_range<pugi::xml_attribute_iterator> attrs);
void ParseLoadcase(DML::Loadcase* loadcase, pugi::xml_node loadcaseNode);
std::unique_ptr<DML::LoadcaseProperty> ParseLoadCasePropertyGeneric(
    pugi::xml_node child,
    std::string loadcase_id);
void ParseRules(DML::Rule* rule, pugi::xml_node xml_node);
DML::Config LoadDML(std::string dml_path);

unsigned long long GetTimeUnitMultiplier(pugi::xml_node node);

template <class T>
T GetAttr(pugi::xml_attribute attr);

template <class T>
T GetAttr(pugi::xml_node node, std::string attr_name, bool required = false) {
  auto attr = node.attribute(attr_name.c_str());
  // TODO: do all attrs absolutely need to be specified?
  if (required && attr.empty()) {
    throw std::invalid_argument("Missing attribute: " + attr_name);
  }
  return GetAttr<T>(attr);
}

std::shared_ptr<Volume> ParseVolumeAttr(pugi::xml_node node, std::string loadcase_id);
}  // namespace Parser
}  // namespace DML
