#include <algorithm>
#include <boost/filesystem.hpp>

#include "gtest/gtest.h"

#include "Simulator.h"
#include "Volume.h"

using namespace std;
using namespace boost::filesystem;

namespace VolumeTest {

template <typename T>
class VolTestABC : public ::testing::Test {
public:
  static void Init(path dml_path) {
    DML::Config config = DML::Parser::LoadDML(dml_path.string());
    Simulator::instance().ConfigureFromDML(config);
  }
  static void TearDownTestCase() {
    delete sim;
  }
  static Simulator * sim;
};
template <typename T> Simulator * VolTestABC<T>::sim(NULL);

class VolumeTest : public VolTestABC<VolumeTest> {
public:
  static void SetUpTestCase() {
    Init(current_path() / "unit" / "dml_valid" / "cantilever.dml");
  }
};

class VolumeTestSTL : public VolTestABC<VolumeTestSTL> {
public:
  static void SetUpTestCase() {
    Init(current_path() / "unit" / "dml_valid" / "femur.dml");
  }
};

class VolumeTestMoveTo : public VolTestABC<VolumeTestSTL> {
public:
  static void SetUpTestCase() {
    Init(current_path() / "unit" / "dml_valid" / "move_to.dml");
  }
};


TEST_F(VolumeTest, test_load) {
  auto loadpoint_masses = sim->volumes["loadpoint"]->masses_inside;
  auto total_load_forces_x = 0.0;
  auto total_load_forces_y = 0.0;
  auto total_load_forces_z = 0.0;
  auto under_external_force_count = 0;

  for (auto m : loadpoint_masses) {
    total_load_forces_x += m->externalForce.x;
    total_load_forces_y += m->externalForce.y;
    total_load_forces_z += m->externalForce.z;
    if (m->UnderExternalForce())
      under_external_force_count++;
  }

  ASSERT_DOUBLE_EQ(total_load_forces_x, 0.0);
  ASSERT_DOUBLE_EQ(total_load_forces_y, -50.0);
  ASSERT_DOUBLE_EQ(total_load_forces_z, 0.0);
  EXPECT_EQ(under_external_force_count, 25);
}

TEST_F(VolumeTest, test_anchor) {
  auto anchored_masses = sim->volumes["anchor"]->masses_inside;
  auto anchored_mass_count = 0;
  for (auto m : anchored_masses) {
    if (m->fixed) {
      anchored_mass_count++;
    }
  }
  EXPECT_EQ(anchored_mass_count, 75);
}

TEST_F(VolumeTest, test_accel) {
  auto accel_masses = sim->volumes["cantilever"]->masses_inside;
  auto accel_masses_count = 0;
  for (auto m : accel_masses) {
    if (m->appliedAcceleration.x != 0 || m->appliedAcceleration.z != 0) {
      continue;
    }

    if (m->fixed) {
      continue;
    }

    if (m->appliedAcceleration.y < -9.0 && m->appliedAcceleration.y > -10.0) {
      accel_masses_count++;
    }
  }

  // all masses in cantilever minus anchored masses
  EXPECT_EQ(accel_masses_count, (1025-75));
}


TEST_F(VolumeTestSTL, test_load) {
  auto loadpoint_masses = sim->volumes["loadpoint"]->masses_inside;
  auto total_load_forces_x = 0.0;
  auto total_load_forces_y = 0.0;
  auto total_load_forces_z = 0.0;
  auto under_external_force_count = 0;

  for (auto m : loadpoint_masses) {
    total_load_forces_x += m->externalForce.x;
    total_load_forces_y += m->externalForce.y;
    total_load_forces_z += m->externalForce.z;
    if (m->UnderExternalForce())
      under_external_force_count++;
  }

  EXPECT_DOUBLE_EQ(total_load_forces_x, 0.0);
  EXPECT_NEAR(total_load_forces_y, -10.0, 0.001);
  EXPECT_DOUBLE_EQ(total_load_forces_z, 0.0);
  EXPECT_EQ(under_external_force_count, 139);
}

TEST_F(VolumeTestSTL, test_anchor) {
  auto anchored_masses = sim->volumes["anchor"]->masses_inside;
  auto anchored_mass_count = 0;
  for (auto m : anchored_masses) {
    if (m->fixed) {
      anchored_mass_count++;
    }
  }
  EXPECT_EQ(anchored_mass_count, 66);
}

TEST_F(VolumeTestSTL, test_accel) {
  auto accel_masses = sim->volumes["cantilever"]->masses_inside;
  auto accel_masses_count = 0;
  for (auto m : accel_masses) {
    if (m->appliedAcceleration.x != 0 || m->appliedAcceleration.z != 0) {
      continue;
    }

    if (m->fixed) {
      continue;
    }

    if (m->appliedAcceleration.y < -9.0 && m->appliedAcceleration.y > -10.0) {
      accel_masses_count++;
    }
  }

  // all masses in cantilever minus anchored masses
  EXPECT_EQ(accel_masses_count, 1843);
}

TEST_F(VolumeTestMoveTo, test_move_to_origin) {
  auto bbox = Geometry::PolyhedronBBOX(
      sim->volumes.at("moveorigin")->polyhedron);
  EXPECT_EQ(bbox.xmin(), 0);
  EXPECT_EQ(bbox.ymin(), 0);
  EXPECT_EQ(bbox.zmin(), 0);
}
TEST_F(VolumeTestMoveTo, test_move_to_centroid) {
  auto bbox = Geometry::PolyhedronBBOX(
      sim->volumes.at("movecentroid")->polyhedron);
  EXPECT_EQ(-1.0*bbox.xmin(), bbox.xmax());
  EXPECT_EQ(-1.0*bbox.ymin(), bbox.ymax());
  EXPECT_EQ(-1.0*bbox.zmin(), bbox.zmax());
}
TEST_F(VolumeTestMoveTo, test_move_to_none) {
  auto bbox = Geometry::PolyhedronBBOX(
      sim->volumes.at("movenone")->polyhedron);
  EXPECT_EQ(bbox.xmin(), 10);
  EXPECT_EQ(bbox.xmax(), 11);
  EXPECT_EQ(bbox.ymin(), 5);
  EXPECT_EQ(bbox.ymax(), 6);
  EXPECT_EQ(bbox.zmin(), 2);
  EXPECT_EQ(bbox.zmax(), 3);
}
TEST_F(VolumeTestMoveTo, test_move_to_undefined) {
  auto bbox = Geometry::PolyhedronBBOX(
      sim->volumes.at("moveundefined")->polyhedron);
  EXPECT_EQ(bbox.xmin(), 10);
  EXPECT_EQ(bbox.xmax(), 11);
  EXPECT_EQ(bbox.ymin(), 5);
  EXPECT_EQ(bbox.ymax(), 6);
  EXPECT_EQ(bbox.zmin(), 2);
  EXPECT_EQ(bbox.zmax(), 3);
}

}
