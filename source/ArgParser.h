#pragma once

#include <iostream>
#include "args.hxx"

// TODO(jclay): This can most likely be removed.
// namespace ArgParser {
// namespace detail {
// extern args::ArgumentParser parser;
// extern args::HelpFlag help;
//}  // namespace detail
//
// extern args::Positional<std::string> dmlFilepath;
// extern args::ValueFlag<std::string> positionTraceFilepath;
// extern args::ValueFlag<std::string> totalEnergyTraceFilepath;
// extern args::Flag applyForces;
// extern args::Flag hideWindow;
//
// void parse(int argc, char **argv);
//}  // namespace ArgParser

namespace ArgParser {
namespace detail {
static args::ArgumentParser parser("OpenDML");
static args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
}  // namespace detail

static args::Positional<std::string> dmlFilepath(detail::parser,
                                                 "filepath",
                                                 "Path to DML file");
static args::ValueFlag<std::string> positionTraceFilepath(
    detail::parser,
    "filepath",
    "Record to CSV position of trace node over time",
    {"fp-trace-position"});
static args::ValueFlag<std::string> totalEnergyTraceFilepath(
    detail::parser,
    "filepath",
    "Record to CSV total energy over time",
    {"fp-trace-total-energy"});

static args::Flag useGPU(detail::parser,
                         "boolean",
                         "Use the CUDA implementation"
                         "  Requires the binary to be built with CUDA and a "
                         "  CUDA capable GPU.",
                         {"use-gpu"});
static args::ValueFlag<int> gpuDeviceId(detail::parser,
                                        "integer",
                                        "Which CUDA device to select (defaults to 0)",
                                        {"deviceId"});
static args::Flag applyForces(detail::parser,
                              "boolean",
                              "Run the simulation and apply forces right away."
                              "  Equivalent to pressing spacebar at start.",
                              {"apply-forces"});
static args::Flag testPerformance(detail::parser,
                                  "boolean",
                                  "Run the performance tests and write results"
                                  "  to stdout",
                                  {"test-performance"});
static args::Flag testEnergy(detail::parser,
                             "boolean",
                             "Run the energy tests and write results"
                             "  to stdout",
                             {"test-energy"});
static args::Flag testBeam(detail::parser,
                           "boolean",
                           "Run the beam tests and write results"
                           "  to stdout",
                           {"test-beam"});
static args::Flag hideWindow(
    detail::parser,
    "boolean",
    "Run the simulation but don't show a window."
    "  Useful if you want to collect simulator data but do not want to see"
    " the visualization.  Use with --timeout-ms or --timeout-ts."
    " Increases performance.",
    {"hide-window"});

static void parse(int argc, char** argv) {
  try {
    detail::parser.ParseCLI(argc, argv);
  } catch (args::Help) {
    std::cout << detail::parser;
    exit(0);
  } catch (args::Error e) {
    std::cerr << e.what() << std::endl;
    std::cerr << detail::parser;
    exit(1);
  }
}
}  // namespace ArgParser
