#pragma once
#include "Simulator.h"
#include "Vertex.h"

#include "MassesModifier.h"
#include "SpringsModifier.h"

#include <Qt3DCore/qentity.h>
#include <Qt3DCore/qtransform.h>
#include <Qt3DExtras/qgoochmaterial.h>
#include <Qt3DExtras/qpervertexcolormaterial.h>
#include <Qt3DExtras/QConeMesh>
#include <Qt3DExtras/QCuboidMesh>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QPhongAlphaMaterial>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QPlaneMesh>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QTorusMesh>
#include <QtCore/QObject>
#include <vector>

class SceneModifier : public QObject {
  Q_OBJECT

 public:
  explicit SceneModifier(Qt3DCore::QEntity* rootEntity, Simulator* sim);
  ~SceneModifier();
  void AddBoundingBoxes();
  Simulator* sim;
 public slots:

  void enableMasses(bool enabled);
  void enableSprings(bool enabled);
  void enableBBox(bool enabled);
  void enableForceVectors(bool enabled);
  void render();

 private:
  bool line_to_cross_added = false;
  MassModifier* masses_modifier;
  SpringModifier* springs_modifier;
  Qt3DCore::QEntity* m_rootEntity;
  Qt3DCore::QEntity* bbox_root;
  Qt3DCore::QEntity* mass_root;
  Qt3DCore::QEntity* spring_root;
  vector<Qt3DCore::QEntity*> mass_entities;
  vector<Vertex> force_buffer;
};
