Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.19999999999999998, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.20099999999999998, 0.021
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.20099999999999998, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.20099999999999998, 0.021
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.2_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.2
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.2005
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.2005
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.2005
trace zMin: -0.0005
trace zMax: 0.0205
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_y\RK4\0.2_1
All masses were unique
Calculated number of masses: 8405
Max Spring Connections: 20
Min Spring Connections: 11
Average Spring Connections: 19.9365
Max Spring Length: 0.0175675
Min Spring Length: 0.00332599
Max Spring Stiffness: 2.68727e+07
Min Spring Stiffness: 5.0877e+06
Average Spring Stiffness: 1.40265e+07
8405 masses
initial centroid0.0991185
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
3753
3777
3797
3813
3830
3831
3853
3856
3869
3870
3878
3886
3898
3909
3921
3924
3934
3954
3961
3962
3984
4004
4007
4009
4012
4018
4031
4044
4061
4069
4108
4114
4121
4141
4160
4163
4177
4222
4227
4255
4258
4315
4318
4355
4372
4448
4491
4495
4496
4510
4548
4612
4616
4618
4640
4655
4668
4783
4800
4803
4805
4855
4874
4878
4886
4904
4915
4963
5019
5031
5034
5087
5094
5165
5176
5199
5216
5219
5276
5304
5365
5387
5424
5581
5685
5691
5813
5876
6005
6060
6114
6221
6300
6301
6342
6383
6414
6563
6598
6644
6678
6730
6889
6890
6957
7145
7198
7285
7353
7485
7495
7505
7603
7676
7707
7766
7855
7868
7941
7958
7983
8049
8052
8088
8137
8162
8206
8234
8253
8261
8265
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.0991185
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 1385.56
Avg Time Per Step: 0.00138556
Masses per Second: 6.06614e+06
Springs per Second: 6.04687e+07
