#version 330 core

in vec3 aPos;
in vec3 aNormal;
in vec3 aColor;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec3 FragPos;
out vec3 Normal;
out vec3 Color;

void main() {
    // Apply all matrix transformations to vert
    gl_PointSize = 2.0f;

	FragPos = vec3(model * vec4(aPos, 1.0));
	Normal = aNormal;
	Color = aColor;
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}