#pragma once
#include <iostream>
#include <unordered_map>

#include "Volume.h"

namespace DML {
class RuleProperty {
 public:
  RuleProperty();
  ~RuleProperty();

  void Type(std::string name);
  void SetAttribute(std::string key, std::string value);

  std::string Type();
  std::string GetAttribute(std::string key);

 private:
  std::string type;  // lattice, load, mechanics, development, trace, stop
  std::unordered_map<std::string, std::string> attributes;
};
}  // namespace DML
