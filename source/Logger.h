#pragma once
#include "spdlog/spdlog.h"

namespace Logger {
std::shared_ptr<spdlog::logger> getLogger(const std::string& loggerName);
}
