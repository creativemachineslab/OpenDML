#pragma once

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace Utils {
using boost::uuids::uuid;
using namespace boost::filesystem;
using boost::lexical_cast;
using boost::uuids::random_generator;
using boost::uuids::uuid;
using namespace std;

inline std::string MakeUUID() {
  return lexical_cast<std::string>((random_generator())());
}

inline path GetUniqueOutputDir() {
  return "output" / MakeUUID();
}
}  // namespace Utils
