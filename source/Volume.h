#pragma once

#include <glm/glm.hpp>
#include <memory>
#include "DML/LoadcaseProperty.h"
#include "DML/Volume.h"
#include "Forces.h"
#include "Geometry.h"
#include "Mass.h"

using glm::dvec3;
using glm::vec3;

enum class VolumeType { Cube, STL };

class Volume {
 public:
  Volume();
  Volume(DML::Volume& volume);
  ~Volume();

  void Build();
  void AddMass(shared_ptr<Mass> mass);
  void AddForce(DML::LoadcaseParams::Types type,
                shared_ptr<DML::LoadcaseParams::Params> params);

  template <class T, class U>
  void AddForce(shared_ptr<DML::LoadcaseParams::Params> prop);

  dvec3 scale;
  vec3 translate;
  vec3 color;
  bool fixed = false;
  dvec3 appliedAcceleration = dvec3(0, 0, 0);
  std::string id;
  std::string stl_path;
  VolumeType type;
  vector<shared_ptr<Mass>> masses_inside;
  vector<shared_ptr<Forces::ForceIterator>> forces;
  Polyhedron polyhedron;
  Bbox bbox;
  shared_ptr<AABB_tree> aabb_tree;
  shared_ptr<DML::Volume> dml_volume;
};
