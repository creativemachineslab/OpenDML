// System includes
#include <assert.h>
#include <stdio.h>

// local includes
#include "Mass.h"
#include "Spring.h"

// helper functions and utilities to work with CUDA
#include <cuda.h>
#include <cuda_gl_interop.h>
#include <helper_cuda.h>

// Forward declare
void ApplyMassForces();
void UpdateMassPosition();
void UpdateSpringPosition();

//__shared__ Mass * d_mass_grid[];
struct cudaGraphicsResource *mass_vbo_resource;
struct cudaGraphicsResource *spring_vbo_resource;
__device__
float maxSpringForce;
float time_step;

__global__

void ApplyMassForces_K(Mass *masses,
                       Spring *springs,
                       int num_masses,
                       int volume_w,
                       int volume_h,
                       int volume_d,
                       float time_step) {
  // unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
  // unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
  // unsigned int z = blockIdx.z*blockDim.z + threadIdx.z;
  // u_int i = z*volume_w*volume_h + y*volume_w + x;

  int i = (blockDim.x * blockIdx.x + threadIdx.x);
  if (i < num_masses) {
    Mass *this_mass      = &masses[i];
    Mass *connected_mass = NULL;
    if (!this_mass->fixed) {
      this_mass->force = dvec3(0, -9.8, 0) * this_mass->mass;
      this_mass->force += this_mass->externalForce;

      for (int springCount = 0; springCount < this_mass->attachedSpringCount;
           springCount++) {
        auto springIdx = this_mass->connectedSpringsIndices[springCount];
        Spring *s      = &springs[springIdx];
        // differentiate current mass from connected
        if (s->mass1_idx == i) {
          connected_mass = &masses[s->mass2_idx];
        } else {
          connected_mass = &masses[s->mass1_idx];
        }

        dvec3 distance     = connected_mass->position - this_mass->position;
        dvec3 direction    = glm::normalize(distance);
        double diff_length = glm::length(distance) - s->restLength;
        double scalarForce = (s->stiffness * diff_length);
        dvec3 forceVec     = (scalarForce * direction) * (0.5);

        this_mass->force += forceVec;

        if (abs(scalarForce) > s->maxScalarForce) {
          s->maxScalarForce = abs(scalarForce);
          if (s->maxScalarForce > maxSpringForce) {
            maxSpringForce = abs(scalarForce);
          }
        }

        float scalarVal = 0.0;
        if (maxSpringForce == 0.0) {
          scalarVal = 0.0;
        } else {
          scalarVal = s->maxScalarForce / maxSpringForce;
        }

        // float scalarVal = 1.0 / (s->updateCount * 0.01);
        auto red   = (scalarVal);
        auto green = scalarVal;
        auto blue  = (scalarVal);
        s->color   = glm::dvec3(red, green, blue);
      }
    }
  }
}

__global__

void UpdateMassPositions_K(GLfloat *pos,
                           Mass *mass_grid,
                           int num_masses,
                           int volume_w,
                           int volume_h,
                           int volume_d,
                           double time_step) {
  int i = (blockDim.x * blockIdx.x + threadIdx.x);
  if (i < num_masses) {
    Mass *mass = &mass_grid[i];

    // Integrate forces
    mass->currentVelocity += (mass->force / mass->mass) * time_step;
    mass->force = dvec3(0, 0, 0);
    mass->position += (mass->currentVelocity) * (time_step);

    i = i * 8;

    pos[i++] = mass->position.x;
    pos[i++] = mass->position.y;
    pos[i++] = mass->position.z;
    pos[i++] = 1.0f;

    // From Mass->GetColor()
    if (mass->underExternalForce) {
      pos[i++] = 0.0f;
      pos[i++] = 1.0f;
      pos[i++] = 0.0f;
      pos[i++] = 1.0f;

    } else if (mass->fixed) {
      pos[i++] = 0.0f;
      pos[i++] = 0.0f;
      pos[i++] = 1.0f;
      pos[i++] = 1.0f;
    } else {
      pos[i++] = 1.0f;
      pos[i++] = 1.0f;
      pos[i++] = 1.0f;
      pos[i++] = 1.0f;
    }
  }
}

__global__

void UpdateSpringPositions_K(GLfloat *pos,
                             Mass *mass_grid,
                             Spring *springs,
                             int num_springs) {
  int i = (blockDim.x * blockIdx.x + threadIdx.x);
  if (i < num_springs) {
    Spring *s = &springs[i];
    Mass *m1  = &mass_grid[springs[i].mass1_idx];
    Mass *m2  = &mass_grid[springs[i].mass2_idx];

    // components in each vector, number of vectors, double since there are
    // colors
    i = i * 4 * 2 * 2;

    pos[i++] = m1->position.x;
    pos[i++] = m1->position.y;
    pos[i++] = m1->position.z;
    pos[i++] = 1.0f;

    pos[i++] = s->color.r;
    pos[i++] = s->color.g;
    pos[i++] = s->color.b;
    pos[i++] = 1.0f;

    pos[i++] = m2->position.x;
    pos[i++] = m2->position.y;
    pos[i++] = m2->position.z;
    pos[i++] = 1.0f;

    pos[i++] = s->color.r;
    pos[i++] = s->color.g;
    pos[i++] = s->color.b;
    pos[i++] = 1.0f;
  }
}

//__global__ void
// ApplyExternalForce_K(Mass * mass_grid) {
//	auto x = threadIdx.x + blockDim.x * blockIdx.x;
//	auto y = threadIdx.y + blockDim.y * blockIdx.y;
//	auto z = threadIdx.z + blockDim.z * blockIdx.z;
//
//	Mass * mass = &mass_grid[x + y + z];
//	if (!mass->fixed && mass->underExternalForce) {
//		mass->force += mass->externalForce;
//	}
//}

int volume_width;
int volume_height;
int volume_depth;
int num_masses;
int num_springs;
Mass *d_mass_grid = NULL;
Mass *h_masses    = NULL;
Spring *d_springs = NULL;
Spring *h_springs = NULL;
size_t grid_size;
size_t springs_size;
int mass_threads;
int mass_grid;
int volume_scalar; // used to determine grid size for now

// =====================
// External Functions
// ~ These are called from other programs.
// =====================
extern "C" void CUDA_InitializeData(Mass masses[],
                                    Spring *springs[],
                                    int spring_count,
                                    int _volume_width,
                                    int _volume_height,
                                    int _volume_depth,
                                    int _volume_scalar,
                                    float _time_step) {
  cudaError_t err = cudaSuccess;
  time_step       = _time_step;
  volume_width    = _volume_width;
  volume_depth    = _volume_depth;
  volume_height   = _volume_height;
  volume_scalar   = _volume_scalar;

  num_masses  = (volume_width * volume_height * volume_depth);
  num_springs = spring_count;

  springs_size = num_springs * sizeof(Spring);
  grid_size    = num_masses * sizeof(Mass);

  h_masses  = (Mass *)malloc(grid_size);
  h_springs = (Spring *)malloc(springs_size);

  // make sure allocations succeeded
  if (h_masses == NULL) {
    fprintf(stderr, "failed to allocate host vectors!\n");
    // exit(EXIT_FAILURE);
  }

  if (h_springs == NULL) {
    fprintf(stderr, "failed to allocate springs\n");
  }

  d_mass_grid = NULL;
  d_springs   = NULL;
  err         = cudaMalloc(&d_mass_grid, grid_size);

  // make sure device allocation worked
  if (err != cudaSuccess) {
    fprintf(stderr, "Failed to allocate device vector (error code %s) \n",
            cudaGetErrorString(err));
    // exit(EXIT_FAILURE);
  }

  d_springs = NULL;
  err       = cudaMalloc(&d_springs, springs_size);

  // make sure device allocation worked
  if (err != cudaSuccess) {
    fprintf(stderr, "Failed to allocate springs on device (error code %s) \n",
            cudaGetErrorString(err));
    // exit(EXIT_FAILURE);
  }

  // copy mass_grid over to host
  for (int i    = 0; i < num_masses; i++) {
    h_masses[i] = masses[i];
  }

  // copy springs over to host
  for (int i     = 0; i < num_springs; i++) {
    h_springs[i] = *(springs[i]);
  }

  err = cudaMemcpy(d_mass_grid, h_masses, grid_size, cudaMemcpyHostToDevice);
  err = cudaMemcpy(d_springs, h_springs, springs_size, cudaMemcpyHostToDevice);

  if (err != cudaSuccess) {
    fprintf(stderr, "Failed to copy to devicev (error code %s) \n",
            cudaGetErrorString(err));
    // exit(EXIT_FAILURE);
  }

  // if larger than num of max threads...
  // this should prob be per device in the future
  if (num_masses > 1024) {
    mass_threads = 1024;
    mass_grid    = ceil(float(num_masses) / 1024);
  } else {
    mass_threads = num_masses;
    mass_grid    = 1;
  }

  // checkCudaErrors(cudaMemcpy(h_springs, d_springs, springs_size,
  // cudaMemcpyDeviceToHost));

  // for (int i = 0; i < num_springs; i++) {
  //	auto springOrig = springs[i];
  //	Spring * springNew = &h_springs[i];
  //	auto m1OrigPos = springOrig->m1->position;
  //	auto m1NewPos = h_masses[springNew->mass1_idx].position;
  //	auto m2OrigPos = springOrig->m2->position;
  //	auto m2NewPos = h_masses[springNew->mass2_idx].position;
  //	//Mass * m2 = &h_masses[h_springs[i].mass2_idx];
  //	//auto vec1 = m1->position - m2->position;
  //	//auto vec2 = h_springs[i].m1->position - h_springs[i].m2->position;
  //	if (m1OrigPos != m1NewPos || m2OrigPos != m2NewPos) {
  //		cout << "vectors differ";
  //	}
  //}
  // free(h_masses);
  // h_masses = NULL;
}

extern "C" void CUDA_InitializeMassBuffer(GLuint *vbo) {
  checkCudaErrors(cudaGraphicsGLRegisterBuffer(
                                               &mass_vbo_resource, *vbo,
                                               cudaGraphicsMapFlagsWriteDiscard));
}

extern "C" void CUDA_InitializeSpringBuffer(GLuint *vbo) {
  checkCudaErrors(cudaGraphicsGLRegisterBuffer(
                                               &spring_vbo_resource, *vbo,
                                               cudaGraphicsMapFlagsWriteDiscard));
}

extern "C" void CUDA_PerformUpdate() {
  ApplyMassForces();

  cudaDeviceSynchronize();

  UpdateMassPosition();

  UpdateSpringPosition();
}

// =====================
// END External
// =====================

void ApplyMassForces() {
  ApplyMassForces_K<<<mass_grid, mass_threads>>>(
                                                 d_mass_grid, d_springs, num_masses, volume_width,
                                                 volume_height,
                                                 volume_depth, time_step);
}

void UpdateMassPosition() {
  // map OpenGL buffer object for writing from CUDA
  GLfloat *buffer_data_ptr;
  checkCudaErrors(cudaGraphicsMapResources(1, &mass_vbo_resource, 0));

  size_t num_bytes;
  checkCudaErrors(cudaGraphicsResourceGetMappedPointer(
                                                       (void **)&buffer_data_ptr, &num_bytes,
                                                       mass_vbo_resource));

  // dim3 block(2, 2, 2);
  // dim3 grid(ceil(float(volume_width) / block.x), ceil(float(volume_height) /
  // block.y), ceil(float(volume_depth) / block.z));
  UpdateMassPositions_K<<<mass_grid, mass_threads>>>(
                                                     buffer_data_ptr, d_mass_grid, num_masses,
                                                     volume_width, volume_height,
                                                     volume_depth, time_step);
  // cudaDeviceSynchronize();

  checkCudaErrors(cudaGraphicsUnmapResources(1, &mass_vbo_resource, 0));
}

void UpdateSpringPosition() {
  // map OpenGL buffer object for writing from CUDA
  GLfloat *buffer_data_ptr;
  checkCudaErrors(cudaGraphicsMapResources(1, &spring_vbo_resource, 0));

  size_t num_bytes;
  checkCudaErrors(cudaGraphicsResourceGetMappedPointer(
                                                       (void **)&buffer_data_ptr, &num_bytes,
                                                       spring_vbo_resource));

  int threadsPerBlock = 512;
  int blocksPerGrid   = ceil(float(num_springs) / threadsPerBlock);
  // this doesn't give us enough, so round up
  // could be more optimal
  UpdateSpringPositions_K<<<blocksPerGrid, threadsPerBlock>>>(
                                                              buffer_data_ptr, d_mass_grid,
                                                              d_springs, num_springs);
  // cudaDeviceSynchronize();

  checkCudaErrors(cudaGraphicsUnmapResources(1, &spring_vbo_resource, 0));
}

// extern "C"
// void ApplySpringForceGPU() {
//	//int threadsPerBlock = 256;
//	//int blocksPerGrid = num_springs/threadsPerBlock+1;
//	// this doesn't give us enough, so round up
//	// could be more optimal
//	ApplySpringForce_K<<<1, 256>>>(d_mass_grid, d_springs, num_springs,
// 0.001); 	cudaDeviceSynchronize();
//	checkCudaErrors(cudaMemcpy(h_springs, d_springs, springs_size,
// cudaMemcpyDeviceToHost)); 	checkCudaErrors(cudaMemcpy(h_masses,
// d_mass_grid, grid_size, cudaMemcpyDeviceToHost));
//
//	for (int i = 0; i < num_springs; i++) {
//		Spring * s = &h_springs[i];
//		Mass * m1 = &h_masses[s->mass1_idx];
//		Mass * m2 = &h_masses[s->mass2_idx];
//		//if (s->color.r == 1.0) {
//		//	cout << "correct" << endl;
//		//}
//		//else if (s->color.r > 1.0) {
//		//	cout << "invalid: " << s->color.r << endl;
//		//}
//		//else {
//		//	cout << "idx: " << i << endl;
//		//}
//		//cout << s->color.r << endl;
//		//if (m1->updateCount > 0) {
//		//	cout << "update count" << endl;
//		//}
//		if (m1->springUpdatesRemaining > m1->attachedSpringCount) {
//			cout << "mis-match" << endl;
//		}
//		if (m2->springUpdatesRemaining > m2->attachedSpringCount) {
//			cout << "mis-match" << endl;
//		}
//	}
//}

// extern "C"
// void TestUpdateCounts() {
//	for (int i = 0; i < 100; i++) {
//		ApplyMassForcesGPU();
//		cudaDeviceSynchronize();
//	}
//
//	checkCudaErrors(cudaMemcpy(h_springs, d_springs, springs_size,
// cudaMemcpyDeviceToHost)); 	checkCudaErrors(cudaMemcpy(h_masses,
// d_mass_grid, grid_size, cudaMemcpyDeviceToHost));
//
//	printf("Springs\n");
//	for (int i = 0; i < num_springs; i++) {
//		Spring * s = &h_springs[i];
//		printf("%d\n", s->updateCount);
//	}
//
//	printf("Masses\n");
//	for (int i = 0; i < num_masses; i++) {
//		Mass * m = &h_masses[i];
//		printf("%d\n", m->updateCount);
//		//printf("%f\n", m->position.x);
//	}
//}

// TODO, free the device memory on app shutdown
// err = cudaFree(d_mass_grid);

//// can better parallelize this by passing in an array of masses
// extern "C" bool applyGravityGPU(Mass * _m, Main * _sim) {
//	Mass * m;
//	Main * sim;
//	// using cuda Unified Memory
//	cudaMallocManaged(&m, sizeof(m));
//	cudaMallocManaged(&sim, sizeof(sim));
//
//	// copy arg into the newly allocated memory
//	*m = *_m;
//	*sim = *_sim;
//
//	dim3 grid(1, 1, 1);
//	dim3 threads(1, 1, 1);
//	performApplyGravity <<< grid, threads >>>(m, sim);
//	checkCudaErrors(cudaDeviceSynchronize());
//
//	// after GPU comp, copy back into
//	*_m = *m;
//
//	cudaFree(m);
//	cudaFree(sim);
//
//	return EXIT_SUCCESS;
//}
//
