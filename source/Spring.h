#pragma once

#include <glm/common.hpp>
#include <glm/geometric.hpp>
#include <glm/vec3.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include "Mass.h"

class Mass;

class Spring {
 public:
  Spring(unsigned int mass1_idx,
         unsigned int mass2_idx,
         double restLength,
         double stiffness);
  Spring(std::shared_ptr<Mass> mass1,
         std::shared_ptr<Mass> mass2,
         double restLength,
         double stiffness);
  ~Spring();
  double stiffness;
  double mass;
  unsigned int mass1_idx;
  unsigned int mass2_idx;
  std::shared_ptr<Mass> mass1;
  std::shared_ptr<Mass> mass2;
  double restLength;
  double diffLength;
  bool springFailed;
  double maxScalarForce;  // tmp state tracking now
  double maxForce = 0;    // tmp state tracking for parameter dump
  int updateCount;
  glm::dvec3 color;
  double GetRestLength();

  enum class States { DEFAULT, EXTENDED, COMPRESSED };

  void SetColorByState(States state);
  void SetColor(glm::dvec3 color);
};
