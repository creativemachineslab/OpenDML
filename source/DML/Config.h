#pragma once
#include "Loadcase.h"
#include "Material.h"
#include "Rule.h"

namespace DML {
class Config {
 public:
  Config();
  ~Config();

  void SetRule(Rule rule);
  void SetMaterial(Material material);
  void AppendVolume(Volume volume);
  void AppendLoadcase(Loadcase loadcase);

  Rule* GetRule();
  Material* GetMaterial();
  std::vector<Volume>* GetVolumes();
  std::vector<Loadcase>* GetLoadcases();

 private:
  Rule rule;
  Material material;
  std::vector<Volume> volumes;
  std::vector<Loadcase> loadcases;
};
}  // namespace DML
