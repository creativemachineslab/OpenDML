from lxml import etree

import pathlib
import glob
import subprocess
import sys, os, time, datetime
import argparse

def run(command, _cwd):
    popen = subprocess.Popen(command, stdout=subprocess.PIPE, cwd=_cwd)
    return iter(popen.stdout.readline, b'')

parser = argparse.ArgumentParser(description='The name of the folder containing the tests to run.')
parser.add_argument('scenario', metavar='scenario', type=str, help='The folder containing the tests to run')

args = parser.parse_args()

this_dir, filename = os.path.split(sys.argv[0]) # directory our script is in
for var_type in ['compare_integrations']:
    dml_files = glob.glob(os.path.join(this_dir, 'scenarios', args.scenario, 'dml_files', var_type, '**/*/*.dml'), recursive=True)
    print(os.path.join(this_dir, 'scenarios', args.scenario, 'dml_files', var_type, '**/*/*.dml'))
    dml_files = [os.path.relpath(f, this_dir) for f in dml_files]
    print(dml_files)
    # opendml_exe = "%s/OpenDML_Build_Work/release/opendml.exe" % os.environ['OneDrive']
    opendml_exe = "../../build/Release/opendml.exe"
    (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(opendml_exe)
    print("OpenDML.exe last modified: %s" % time.ctime(mtime))
    time.sleep(3)
    for d in dml_files:
        xml = etree.parse(os.path.join(this_dir,d))
        telemetry = xml.xpath('/dml/rules/telemetry')[0]
        output_dir = os.path.join(this_dir, telemetry.attrib["output_dir"])
        pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)
        log_file = os.path.join(output_dir, "stdout.txt")
        f = open(log_file,"bw+")
        print(output_dir)
        print(opendml_exe)
        for line in run([opendml_exe, d, "--hide-window"], this_dir):
            f.write(line)
            print(line.decode('utf-8'), end='')
        f.close()