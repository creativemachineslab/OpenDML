Beam Test Scenario Enabled
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.02, 0.16
 translate: 0, 0, 0
 rot: 0, 0, 0
 color: 0,0,0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.021, 0.161
 translate: -0.0025, -0.0005, -0.0005
 color: 255, 0, 0
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.021, 0.161
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.021, 0.161
 translate: 0.1975, -0.0005, -0.0005
 color: 0, 255, 0
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0.0, 1.0, 0.0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   random: true
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
 gpu_acceleration: true
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: scenarios\random_gpu_rk4\output\varying_z\RK4\0.16_1
  integration
   method: RK4
   time_step: 1e-06
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
is_random 1
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.02
cantilever zMin: 0
cantilever zMax: 0.16
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.0205
anchor zMin: -0.0005
anchor zMax: 0.1605
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.0205
loadpoint zMin: -0.0005
loadpoint zMax: 0.1605
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.0205
trace zMin: -0.0005
trace zMax: 0.1605
Lattice is random: 1
Building random lattice
Output dir: scenarios\random_gpu_rk4\output\varying_z\RK4\0.16_1
All masses were unique
Calculated number of masses: 6765
Max Spring Connections: 20
Min Spring Connections: 10
Average Spring Connections: 19.9264
Max Spring Length: 0.0167788
Min Spring Length: 0.00337928
Max Spring Stiffness: 2.64489e+07
Min Spring Stiffness: 5.32684e+06
Average Spring Stiffness: 1.40281e+07
6765 masses
initial centroid0.0102713
Cuda Device ID: 0
GPU Device was set to: 0
CUDA device name: GeForce GTX 1070
CUDA device major: 6
CUDA device minor: 1
Using RK4 integration method
GPU Acceleration Successfully Enabled
Setting masses_to_trace
3347
3382
3398
3400
3425
3432
3437
3451
3452
3453
3461
3463
3468
3476
3479
3501
3514
3529
3549
3558
3563
3575
3584
3585
3600
3612
3621
3625
3626
3660
3691
3697
3716
3723
3728
3772
3774
3793
3796
3849
3852
3864
3875
3897
3901
3911
3937
3969
3975
4009
4025
4118
4138
4143
4144
4148
4179
4233
4303
4356
4364
4372
4373
4433
4561
4575
4581
4680
4802
4824
4825
5017
5048
5204
5240
5317
5353
5486
5648
5659
5683
5779
5787
5877
5911
6011
6038
6108
6163
6194
6264
6277
6283
6317
6353
6366
6414
6431
6483
6520
6525
6575
GPUSpringMass: Releasing Forces
Sum forces y: 0
Release damping at: 500ms
Release point position: 0.0102713
tr size: 500000position_traces_size 500001
Writing output
Elapsed Seconds: 1085.63
Avg Time Per Step: 0.00108563
Masses per Second: 6.23143e+06
Springs per Second: 6.20849e+07
