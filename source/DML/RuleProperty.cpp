#include "RuleProperty.h"

using namespace std;

namespace DML {
RuleProperty::RuleProperty() {}

RuleProperty::~RuleProperty() {}

void RuleProperty::Type(string name) {
  this->type = name;
}

void RuleProperty::SetAttribute(string key, string value) {
  attributes[key] = value;
}

string RuleProperty::Type() {
  return this->type;
}

string RuleProperty::GetAttribute(string key) {
  return this->attributes.at(key);
}
}  // namespace DML
