Hiding window!
XML Loading result: No error
Validating XML...
XML validated successfully...
volume
 id: cantilever
 primitive: cube
 scale: 0.2, 0.3, 0.02
 translate: 0, 0, 0
 rot: 0, 0, 0
 
volume
 id: anchor
 primitive: cube
 scale: 0.005, 0.301, 0.021
 translate: -0.0025, -0.0005, -0.0005
 
volume
 id: loadpoint
 primitive: cube
 scale: 0.005, 0.301, 0.021
 translate: 0.1975, -0.0005, -0.0005
 
volume
 id: trace
 primitive: cube
 scale: 0.005, 0.301, 0.021
 translate: 0.1975, -0.0005, -0.0005
 
loadcase
 id: L1
  fix
   volume: anchor
  constant
   volume: loadpoint
   magnitude: 0, -200, 0
  accel
   volume: cantilever
   magnitude: 0, 0, 0
 
material
 id: steel-lattice
  elasticity
   modulus: 113.8
   unit: GPa
  lattice
   cell: cubic
   vertexmass: 0.00034859
   size: 0.005, 0.005, 0.005
   bardiam: 0.001
   volume: cantilever
   offset: 0, 0, 0
 
rules
 vol: cantilever
  damping
   amount: 0.01
  material
   matid: steel-lattice
  output
   destination: window
   size: 1920, 1080
  load
   load-id: L1
  telemetry
   output_dir: output/varying_y_47cb3a5e/euler/0.3_1/
  integration
   method: euler
  stop
   simulation_time: 500
   wall_clock_time: 0
   time_unit: ms
 
1
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.3
cantilever zMin: 0
cantilever zMax: 0.02
1
anchor xMin: -0.0025
anchor xMax: 0.0025
anchor yMin: -0.0005
anchor yMax: 0.3005
anchor zMin: -0.0005
anchor zMax: 0.0205
1
loadpoint xMin: 0.1975
loadpoint xMax: 0.2025
loadpoint yMin: -0.0005
loadpoint yMax: 0.3005
loadpoint zMin: -0.0005
loadpoint zMax: 0.0205
1
trace xMin: 0.1975
trace xMax: 0.2025
trace yMin: -0.0005
trace yMax: 0.3005
trace zMin: -0.0005
trace zMax: 0.0205
cantilever xMin: 0
cantilever xMax: 0.2
cantilever yMin: 0
cantilever yMax: 0.3
cantilever zMin: 0
cantilever zMax: 0.02
Output dir: output/varying_y_47cb3a5e/euler/0.3_1/
All masses were unique
Number of masses is correct.
Debug Output
==========
mass_xmax	0.2
mass_xmin	0
mass_ymax	0.3
mass_ymin	0
mass_zmax	0.02
mass_zmin	0
max_spring_stiffness	1.78757e+07
min_spring_stiffness	1.03205e+07
total_volume_mass	3.31219
total_force	1920.38
load_volume_count	305
anchor_volume_count	305
traced_count	305
cantilever_count	12505
total_masses	12505
total_spring_count	136104
==========
Updates enabled
Movement below epsilon
Std Dev Trace: 1.04607e-15
Release damping at: 1000ms
Release point position: -2.73285e-05
num_same_springs: 0
100 / 500
200 / 500
300 / 500
400 / 500
