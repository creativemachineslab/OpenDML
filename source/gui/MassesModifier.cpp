#include <Qt3DCore/qtransform.h>
#include <Qt3DExtras/qpervertexcolormaterial.h>
#include <Qt3DRender/qeffect.h>
#include <Qt3DRender/qpointsize.h>
#include <Qt3DRender/qrenderpass.h>
#include <Qt3DRender/qtechnique.h>

#include "LinesGeometry.h"
#include "MassesModifier.h"
#include "PointGeometry.h"

using namespace std;
using namespace Qt3DRender;

MassModifier::MassModifier(Qt3DCore::QEntity* rootEntity, Simulator* sim)
    : root_entity(rootEntity), sim(sim) {
  mass_root = new Qt3DCore::QEntity(rootEntity);
  forces_root = new Qt3DCore::QEntity(rootEntity);

  auto mass_entity = new Qt3DCore::QEntity(mass_root);
  auto point = new Point();
  auto point_transform = new Qt3DCore::QTransform();
  auto point_material = new Qt3DExtras::QPerVertexColorMaterial();

  // hacky way to set point size...
  // the only way I can find without creating our own shaders
  auto effect = point_material->effect();
  for (auto t : effect->techniques()) {
    for (auto rp : t->renderPasses()) {
      auto pointSize = new QPointSize();
      pointSize->setSizeMode(QPointSize::SizeMode::Fixed);
      pointSize->setValue(5.0f);
      rp->addRenderState(pointSize);
    }
  }

  mass_entity->addComponent(point);
  mass_entity->addComponent(point_material);
  mass_entity->addComponent(point_transform);

  auto vector_entity = new Qt3DCore::QEntity(forces_root);
  auto line = new Lines();
  auto line_transform = new Qt3DCore::QTransform();
  auto line_material = new Qt3DExtras::QPerVertexColorMaterial();

  vector_entity->addComponent(line);
  vector_entity->addComponent(line_transform);
  vector_entity->addComponent(line_material);

  for (int i = 0; i < 2; i++) {
    auto centroid = Analysis::GetVolumeTrace();
    Vertex vert1;
    Vertex vert2;
    vert1.color = vec3(0, 0, 1);
    vert2.color = vec3(0, 0, 1);
    vert1.position = centroid * dvec3(0, 1, 1);
    vert2.position = centroid + dvec3(0.02, 0, 0);
    // *glm::normalize(m->externalForce);
    force_buffer.push_back(vert1);
    force_buffer.push_back(vert2);
  }
}

void MassModifier::updateMassBuffer() {
  if (!mass_mutex().try_lock()) {
    return;
  }
  if (sim->masses->size() != mass_buffer.size()) {
    mass_buffer.resize(sim->masses->size());
  }
  auto m_idx = 0;
  for (const auto m : *sim->masses) {
    // TODO move GetVertex implementation to here...
    (mass_buffer)[m_idx] = m->GetVertex();

    // if (show_forces) {
    //   if (m->UnderExternalForce()) {
    //     Vertex vert1;
    //     Vertex vert2;
    //     vert1.color = m->GetColor();
    //     vert2.color = vert1.color;
    //     vert1.position = m->position;
    //     vert2.position =
    //         vert1.position + 0.002 * glm::normalize(m->externalForce);
    //     force_buffer.push_back(vert1);
    //     force_buffer.push_back(vert2);
    //   }
    // }

    m_idx++;
  }

  mass_mutex().unlock();
}

MassModifier::~MassModifier() {}

// void MassModifier::AddMasses() {}

void MassModifier::setEnabled(bool enabled) {
  mass_root->setEnabled(enabled);
}

void MassModifier::setForcesEnabled(bool enabled) {
  show_forces = enabled;
  forces_root->setEnabled(enabled);
}

bool MassModifier::isEnabled() {
  return mass_root->isEnabled();
}

void MassModifier::SyncMasses() {
  updateMassBuffer();
  auto mass_entities = mass_root->findChildren<Point*>();

  for (auto me : mass_entities) {
    me->setVertices(mass_buffer);
  }
  auto spring_entities = forces_root->findChildren<Lines*>();

  for (auto se : spring_entities) {
    se->setVertices(force_buffer);
  }

  {
    auto centroid = Analysis::GetVolumeTrace();
    Vertex vert1;
    Vertex vert2;
    vert1.color = vec3(1, 0, 0);
    vert2.color = vec3(1, 0, 0);
    vert1.position = centroid * dvec3(0, 1, 1);
    vert2.position = centroid + dvec3(0.02, 0, 0);
    // *glm::normalize(m->externalForce);
    force_buffer[2] = vert1;
    force_buffer[3] = vert2;
  }
}
