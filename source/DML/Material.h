#pragma once
#include "Lattice.h"

namespace DML {
class Material {
 public:
  Material();
  ~Material();
  Lattice* GetLattice();
  void SetLattice(Lattice);
  double elasticitymodulus;

 private:
  Lattice lattice;
};
}  // namespace DML
