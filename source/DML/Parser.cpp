#include "Parser.h"
#include "../Logger.h"
#include "Lattice.h"
#include "Loadcase.h"
#include "Material.h"
#include "Rule.h"
#include "Volume.h"

#include <boost/filesystem.hpp>
#include <iostream>
#include <pugixml.hpp>
#include <regex>
#include <unordered_map>
#include <vector>

using namespace std;
namespace fs = boost::filesystem;

namespace DML {
unordered_map<string, Volume> volumes;
unordered_map<string, Loadcase> loadcases;

namespace Parser {
auto logger = Logger::getLogger("DMLParser");
fs::path dml_file;
fs::path canonical_dml_dir;

void ApplyIndentLevel(int level) {
  for (int i = 0; i < level; i++) {
    cout << " ";
  }
}

// level specifies the indent level
// number of spaces it is prefixed with
void PrintAttribute(pugi::xml_attribute attr, int level) {
  Parser::ApplyIndentLevel(level);
  cout << attr.name() << ": " << attr.value() << endl;
}

// seemigly the best / only way to do this
// https://stackoverflow.com/a/14331639
size_t NumberChildren(pugi::xml_node node) {
  size_t count = distance(node.children().begin(), node.children().end());
  return count;
}

// a bare-bones validation
void ValidateDML(pugi::xml_node root) {
  auto requiredNodes = {"volume", "loadcase", "material", "rules"};
  cout << "Validating XML..." << endl;

  for (string req : requiredNodes) {
    auto this_child = root.child(req.c_str());
    if (this_child == NULL) {
      cout << "The " << req << " node was missing." << endl;
      cin.get();
      exit(0);
    }
  }

  cout << "XML validated successfully..." << endl;
}

void PrintDMLDebug(vector<string> expectedNodes, pugi::xml_node root) {
  for (string node_name : expectedNodes) {
    for (pugi::xml_node node : root.children(node_name.c_str())) {
      cout << node.name() << endl;

      for (pugi::xml_attribute attr : node.attributes()) {
        PrintAttribute(attr, 1);
      }

      for (pugi::xml_node child : node.children()) {
        ApplyIndentLevel(2);
        cout << child.name() << endl;
        for (pugi::xml_attribute attr : child.attributes()) {
          PrintAttribute(attr, 3);
        }
      }

      cout << " " << endl;
    }
  }
}

// todo: move me to utility file
vector<double> VectorFromString(string str, size_t expectedLength) {
  vector<double> parsed;
  auto originalStr = str;
  regex re("-?\\d+[.]*\\d*");
  smatch matches;

  if (expectedLength > 3) {
    string msg = "Only lengths up to ";  //+ string(3) + " are supported";
    logger->error(msg);
    exit(0);
  }

  while (regex_search(str, matches, re)) {
    for (auto match : matches) {
      double m;
      try {
        m = stod(match.str());
      } catch (const std::invalid_argument& ia) {
        string msg = "Couldn't parse " + match.str() + " to double.";
        logger->error(msg);
        exit(0);  // fail early and often? tmp, but works for now.
      }

      parsed.push_back(m);
    }
    str = matches.suffix();
  }

  if (parsed.size() > expectedLength) {
    string msg = "There were ";  //+parsed.size() + "parsed ints"
    //+ expectedLength + "were expected";
    logger->error(msg);
    exit(0);
  }

  return parsed;
}

// expected length doesn't really work since we are
// using hardcoded vec3 for now
glm::vec3 vec3FromString(string str, size_t expectedLength) {
  auto parsed = VectorFromString(str, expectedLength);
  return glm::vec3(parsed[0], parsed[1], parsed[2]);
}

glm::dvec3 dvec3FromString(string str, size_t expectedLength) {
  auto parsed = VectorFromString(str, expectedLength);
  return glm::dvec3(parsed[0], parsed[1], parsed[2]);
}

bool ParseVolume(DML::Volume* vol,
                 pugi::xml_object_range<pugi::xml_attribute_iterator> attrs) {
  for (pugi::xml_attribute attr : attrs) {
    auto name = string(attr.name());
    auto val = string(attr.value());

    if (name == "id")
      vol->setID(val);
    else if (name == "primitive") {
      // checkPrimitive(val);
      vol->setPrimitive(val);
    } else if (name == "stl_path") {
      fs::path relative_stl_path = val;
      // look for the Stl file by treating
      // the folder containing the dml file
      // as the root
      fs::path stl_path = (canonical_dml_dir / relative_stl_path).make_preferred();
      if (!fs::exists(stl_path)) {
        throw runtime_error("The STL file was not found at " + stl_path.string());
      }
      vol->setSTLPath(stl_path.string());
    } else if (name == "scale") {
      vol->setScale(vec3FromString(val, 3));
    } else if (name == "translate") {
      vol->setTranslate(vec3FromString(val, 3));
    } else if (name == "color") {
      vol->setColor(vec3FromString(val, 3));
    } else if (name == "move_to") {
      vol->setMoveTo(val);
    }
  }

  return true;
}

bool ParseMaterial(DML::Material* material, pugi::xml_node xml_node) {
  for (pugi::xml_node child : xml_node.children()) {
    auto nodeName = string(child.name());

    if (nodeName == "elasticity") {
      for (pugi::xml_attribute attr : child.attributes()) {
        if (string(attr.name()) == "modulus") {
          material->elasticitymodulus = stod(string(attr.value()));
        }
      }
    } else if (nodeName == "lattice") {
      DML::Lattice lattice;
      auto set_random = GetAttr<bool>(child, "random", false);
      cout << "is_random " << set_random << endl;
      lattice.is_random = set_random;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "volume") {
          Volume* vol;
          try {
            vol = &volumes.at(value);
          } catch (out_of_range) {
            throw runtime_error("Unrecognized volume id specified in lattice: " + value);
          }
          lattice.volume = vol;
        } else if (name == "cell") {
          lattice.cell = value;
        } else if (name == "size") {
          lattice.size = vec3FromString(value, 3);
        } else if (name == "bardiam") {
          lattice.bardiam = stod(value);
        } else if (name == "offset") {
          lattice.offset = vec3FromString(value, 3);
        } else if (name == "vertexmass") {
          lattice.vertexmass = stod(value);
        }
      }

      material->SetLattice(lattice);
    }
  }

  return true;
}

unsigned long long GetTimeUnitMultiplier(pugi::xml_node node) {
  auto unit_id = GetAttr<string>(node, "time_unit", true);
  if (unit_id == "s") {
    return 1e9;
  } else if (unit_id == "ms") {
    return 1e6;
  } else if (unit_id == "us") {
    return 1e3;
  } else if (unit_id == "ns") {
    return 1;
  } else {
    throw runtime_error("Unrecognized time_unit: " + unit_id);
  }
}

void ParseLoadcase(DML::Loadcase* loadcase, pugi::xml_node loadcaseNode) {
  for (pugi::xml_attribute attr : loadcaseNode.attributes()) {
    auto name = string(attr.name());
    auto val = string(attr.value());

    if (name == "id")
      loadcase->setID(val);
  }

  auto loadcase_id = string(loadcase->GetID());

  for (pugi::xml_node child : loadcaseNode.children()) {
    auto vol = ParseVolumeAttr(child, loadcase_id);
    std::shared_ptr<LoadcaseParams::Params> params;
    LoadcaseParams::Types lctype;

    auto type = string(child.name());
    if (type == "pulse") {
      lctype = LoadcaseParams::Types::Pulse;
      LoadcaseParams::Pulse tmp;
      tmp.on_ns = GetAttr<unsigned long long>(child, "on_time", true) *
                  GetTimeUnitMultiplier(child);
      tmp.off_ns = GetAttr<unsigned long long>(child, "off_time", true) *
                   GetTimeUnitMultiplier(child);
      tmp.on_magnitude = GetAttr<glm::dvec3>(child, "on_magnitude", true);
      tmp.off_magnitude = GetAttr<glm::dvec3>(child, "off_magnitude", true);
      tmp.repeat = GetAttr<bool>(child, "repeat", true);
      params = std::make_shared<LoadcaseParams::Pulse>(tmp);
    } else if (type == "shock") {
      lctype = LoadcaseParams::Types::Shock;
      LoadcaseParams::Shock tmp;
      tmp.magnitude = GetAttr<glm::dvec3>(child, "magnitude", true);
      tmp.ns =
          GetAttr<unsigned long long>(child, "time", true) * GetTimeUnitMultiplier(child);
      params = std::make_shared<LoadcaseParams::Shock>(tmp);
    } else if (type == "constant") {
      lctype = LoadcaseParams::Types::Constant;
      LoadcaseParams::Constant tmp;
      tmp.magnitude = GetAttr<glm::dvec3>(child, "magnitude", true);
      params = std::make_shared<LoadcaseParams::Constant>(tmp);
    } else if (type == "fix") {
      lctype = LoadcaseParams::Types::Fix;
      params = std::make_shared<LoadcaseParams::Fix>();
    } else if (type == "accel") {
      lctype = LoadcaseParams::Types::Accel;
      LoadcaseParams::Accel tmp;
      tmp.magnitude = GetAttr<glm::dvec3>(child, "magnitude", true);
      params = std::make_shared<LoadcaseParams::Accel>(tmp);
    } else {
      throw std::invalid_argument("Unrecognized loadcase property: " + type);
    }
    LoadcaseProperty prop = {lctype, vol, params};
    loadcase->AddProperty(prop);
  }
}

template <>
glm::dvec3 GetAttr<glm::dvec3>(pugi::xml_attribute attr) {
  return dvec3FromString(attr.as_string(), 3);
}

template <>
string GetAttr(pugi::xml_attribute attr) {
  return attr.as_string();
}

template <>
double GetAttr(pugi::xml_attribute attr) {
  return attr.as_double();
}

template <>
unsigned long long GetAttr(pugi::xml_attribute attr) {
  return attr.as_ullong();
}

template <>
int GetAttr(pugi::xml_attribute attr) {
  return attr.as_int();
}

template <>
bool GetAttr(pugi::xml_attribute attr) {
  return attr.as_bool();
}

shared_ptr<Volume> ParseVolumeAttr(pugi::xml_node node, string loadcase_id) {
  auto vol_id = GetAttr<string>(node, "volume", true);
  try {
    return std::make_shared<Volume>(volumes.at(vol_id));
  } catch (const out_of_range) {
    string msg =
        "Loadcase ID: " + loadcase_id + " Couldn't find volume with id: " + vol_id;
    logger->error(msg);
    exit(0);
  }
}

void ParseRules(DML::Rule* rule, pugi::xml_node xml_node) {
  bool found_stop = false;
  auto use_gpu_accel = GetAttr<bool>(xml_node, "gpu_acceleration", false);
  rule->gpu_acceleration = use_gpu_accel;

  for (pugi::xml_node child : xml_node.children()) {
    DML::RuleProperty prop;
    auto nodeName = string(child.name());

    if (nodeName == "output") {
      DML::RuleOutput output;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "destination") {
          output.destination = value;
        } else if (name == "size") {
          // specified as WxH
          // this will return [w, h]
          auto size = VectorFromString(value, 2);
          output.windowWidth = size[0];
          output.windowHeight = size[1];
        }
      }
      rule->SetOutput(output);
    }

    if (nodeName == "load") {
      DML::RuleLoad load;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "load-id") {
          auto loadcase = &loadcases.at(value);
          load.loadcase = loadcase;
        }
      }
      rule->SetLoad(load);
    }

    if (nodeName == "mechanics") {
      DML::RuleMechanics mechanics;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "integration") {
          mechanics.integration = value;
        }
      }
      rule->SetMechanics(mechanics);
    }

    if (nodeName == "development") {
      DML::RuleDevelopment development;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "rule") {
          development.rule = value;
        }
      }
      rule->SetDevelopment(development);
    }

    if (nodeName == "trace") {
      DML::RuleTrace trace;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "vol") {
          auto vol = &volumes.at(value);
          trace.volume = vol;
        } else if (name == "value") {
          trace.value = value;
        } else if (name == "file") {
          trace.file = value;
        }
      }
      rule->SetTrace(trace);
    }

    if (nodeName == "damping") {
      DML::RuleDamping damping;
      for (pugi::xml_attribute attr : child.attributes()) {
        auto name = string(attr.name());
        auto value = string(attr.value());

        if (name == "type") {
          damping.type = value;
        } else if (name == "amount") {
          auto val = VectorFromString(value, 1);
          damping.amount = val[0];
        }
      }
      rule->SetDamping(damping);
    }

    if (nodeName == "telemetry") {
      DML::RuleTelemetry telemetry;
      telemetry.output_dir = GetAttr<string>(child, "output_dir", true);
      rule->SetTelemetry(telemetry);
    }

    if (nodeName == "integration") {
      DML::RuleIntegration integration;
      integration.method = GetAttr<string>(child, "method", true);
      integration.time_step = GetAttr<double>(child, "time_step", true);
      rule->SetIntegration(integration);
    }

    if (nodeName == "stop") {
      found_stop = true;
      DML::RuleStop stop;
      auto time_unit = GetTimeUnitMultiplier(child);
      stop.simulation_ns =
          GetAttr<unsigned long long>(child, "simulation_time", true) * time_unit;
      stop.wall_clock_ns =
          GetAttr<unsigned long long>(child, "wall_clock_time", true) * time_unit;
      rule->SetStop(stop);
    }
  }
  if (!found_stop) {
    throw runtime_error("Could not find <stop ...> rule and it is required");
  }
}

DML::Config LoadDML(string _dml_path) {
  dml_file = _dml_path;
  canonical_dml_dir = canonical(dml_file.parent_path()).make_preferred();
  if (!fs::exists(dml_file)) {
    throw runtime_error("The DML file was not found at " +
                        (canonical_dml_dir / dml_file).make_preferred().string());
  }
  DML::Config config;
  auto expectedNodes = vector<string>{"volume"s, "loadcase"s, "material"s, "rules"s};
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(dml_file.string().c_str());
  cout << "XML Loading result: " << result.description() << endl;
  auto dml_root = doc.child("dml");

  ValidateDML(dml_root);
  PrintDMLDebug(expectedNodes, dml_root);

  for (pugi::xml_node xml_vol : dml_root.children("volume")) {
    DML::Volume vol;
    auto attrs = xml_vol.attributes();
    ParseVolume(&vol, attrs);
    config.AppendVolume(vol);
    volumes[vol.GetID()] = vol;
  }

  for (pugi::xml_node xml_material : dml_root.children("material")) {
    DML::Material material;
    ParseMaterial(&material, xml_material);
    config.SetMaterial(material);
  }

  for (pugi::xml_node xml_load : dml_root.children("loadcase")) {
    DML::Loadcase loadcase;
    ParseLoadcase(&loadcase, xml_load);
    config.AppendLoadcase(loadcase);
    loadcases[loadcase.GetID()] = loadcase;
  }

  for (pugi::xml_node xml_rule : dml_root.children("rules")) {
    DML::Rule rule;
    ParseRules(&rule, xml_rule);
    config.SetRule(rule);
  }
  return config;
}

}  // namespace Parser
}  // namespace DML
