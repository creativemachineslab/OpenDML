# This script runs simulator tests on the TACC supercomputer.
# It expects to run from repo root


cwd="$(dirname "$(dirname "$(dirname "$(readlink -f "$0")")")")"
cd $cwd

# set up some configuration vars to find custom cmake and libs
. ./scripts/tacc.sh
tacc_load_profile

set -u
set -e

mkdir -p $WORK/jobfiles
# create the job file
export LAUNCHER_JOB_FILE="$WORK/jobfiles/`date '+%Y%m%d-%H%M%S'`"
./scripts/build  -DUSE_QT=0  # disable qt.  biuld configuration caches this flag >/dev/null
. ./tests/analysis/test_lib.sh

srun -J OpenDML-localtest -p gpu -t 10:00:00 -D $WORK/OpenDML -n 1 -N 1 bash -c 'ANALYZE_DATA=0 ./tests/analysis/run_local.sh'
setup_dml_test_script $*
analyze_data

# setup_dml_test_script $*
# gen_simulation_tests | xargs -P20 -I% sh -c 'srun -J OpenDML-tests -p gpu -t 00:00:30 -D $WORK/OpenDML -n 1 -N 1 %'




#echo '#!/bin/bash' > $LAUNCHER_JOB_FILE
#gen_simulation_tests >> $LAUNCHER_JOB_FILE
#ls -l $LAUNCHER_JOB_FILE
#
#
##cd $WORK/jobfiles
#numjobs=`wc -l $LAUNCHER_JOB_FILE | cut -f 1 -d\  `
#numjobs=$(( $numjobs - 1 ))
#numcores=$(( $numjobs * 4 ))
#sbatch -J OpenDML-tests -p gpu -t 00:01:00 -D `pwd` -n 1 -N 1 $LAUNCHER_JOB_FILE
