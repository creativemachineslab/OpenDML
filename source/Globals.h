#pragma once
#include <mutex>

static auto& mass_mutex() {
  static std::mutex i;
  return i;
}

static auto& springs_mutex() {
  static std::mutex i;
  return i;
}
