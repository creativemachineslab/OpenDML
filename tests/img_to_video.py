#!/usr/local/bin/python3

import cv2
import argparse
import os
import pprint
import re

pp = pprint.PrettyPrinter(indent=4)

# https://arcpy.wordpress.com/2012/05/11/sorting-alphanumeric-strings-in-python/
def sorted_nicely( l ):
    """ Sorts the given iterable in the way that is expected.
 
    Required arguments:
    l -- The iterable to be sorted.
 
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)


# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument('dir_path', help='directory containing the images')
ap.add_argument("-ext", "--extension", required=False, default='png', help="extension name. default is 'png'.")
ap.add_argument("-o", "--output", required=False, default='output.mp4', help="output video file")
args = vars(ap.parse_args())

# Arguments
dir_path = args['dir_path']
ext = args['extension']
output = args['output']
output = "{}.mp4".format(os.path.split(dir_path)[1]);

out_dir = dir_path

while (os.path.split(out_dir)[1] != 'charts'):
    out_dir = os.path.split(out_dir)[0]

# one more to remove the "charts dir"
out_dir = os.path.split(out_dir)[0]
out_dir = os.path.join(out_dir, "videos", output)


if not os.path.exists(os.path.dirname(out_dir)):
    try:
        os.makedirs(os.path.dirname(out_dir))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

images = []
for f in os.listdir(dir_path):
    if f.endswith(ext):
        images.append(f)

images = sorted_nicely(images)
# Determine the width and height from the first image
image_path = os.path.join(dir_path, images[0])
frame = cv2.imread(image_path)
# cv2.imshow('video',frame)
height, width, channels = frame.shape

# Define the codec and create VideoWriter object
# fourcc = cv2.VideoWriter_fourcc(*'X264') # Be sure to use lower case
fourcc = 0X00000020 
out = cv2.VideoWriter(out_dir, fourcc, 20.0, (width, height))

for image in images:
    image_path = os.path.join(dir_path, image)
    frame = cv2.imread(image_path)

    out.write(frame) # Write out frame to video

    # cv2.imshow('video',frame)
    # if (cv2.waitKey(1) & 0xFF) == ord('q'): # Hit `q` to exit
    #     break

# Release everything if job is finished
out.release()
cv2.destroyAllWindows()

print("The video has been saved to {}".format(out_dir))