#include "gtest/gtest.h"
#include "DML/Parser.h"
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;
using namespace std;


TEST(dml_parser, dml_file_does_not_exist) {
  string dml_fp = "some_path/without.dml";
  ASSERT_THROW(DML::Parser::LoadDML(dml_fp), runtime_error);
}

TEST(dml_parser, dml_file_does_exist) {
  fs::path dml_path = fs::current_path();
  dml_path = dml_path / "unit" / "dml_valid" / "cantilever.dml";
  ASSERT_NO_THROW(DML::Parser::LoadDML(dml_path.string()));
}

TEST(dml_parser, stl_path_invalid) {
  fs::path dml_path = fs::current_path();
  dml_path = dml_path / "unit" / "dml_invalid" / "invalid_stl_path.dml";
  ASSERT_THROW(DML::Parser::LoadDML(dml_path.string()), runtime_error);
}

TEST(dml_parser, stl_path_valid) {
  fs::path dml_path = fs::current_path();
  dml_path = dml_path / "unit" / "dml" / "femur.dml";
  ASSERT_THROW(DML::Parser::LoadDML(dml_path.string()), runtime_error);
}
